﻿Imports System.Data.SqlClient

Friend Class ProovedoresAD


    Friend Function ConsultarProveedores() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select pro.id,pro.nombre,(select count(*)from productos  as prod where  prod.proovedor =  pro.id and prod.estatus = 1) as productos,pro.cuenta_bancaria,concat(pro.telefono,'/',pro.telefono2) as telefono,es.estado  
                                 from proovedor as pro,estadosmexico as es
                                 where pro.estatus  = 1 and  pro.estado = es.id_estado")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "productos"
            ds.Tables(0).Columns(3).ColumnName = "cuenta_bancaria"
            ds.Tables(0).Columns(4).ColumnName = "telefono"
            ds.Tables(0).Columns(5).ColumnName = "estado"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultarProveedores(v As String) As List(Of String)
        Dim informacionProovedor As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select a.nombre,a.titular,a.direccion,a.colonia,b.id_estado,a.municipio,a.CP,a.telefono,a.telefono2,a.direccion_web,a.correo_electronico,a.RFC,a.cuenta_bancaria
                                 from proovedor as A,EstadosMexico as B 
                                 where a.id = " + v + " and B.id_estado = A.estado and a.estatus = 1
                                ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionProovedor.Add(myReader("nombre").ToString())
                informacionProovedor.Add(myReader("titular").ToString())
                informacionProovedor.Add(myReader("direccion").ToString())
                informacionProovedor.Add(myReader("colonia").ToString())
                informacionProovedor.Add(myReader("id_estado").ToString())
                informacionProovedor.Add(myReader("municipio").ToString())
                informacionProovedor.Add(myReader("CP").ToString())
                informacionProovedor.Add(myReader("telefono").ToString())
                informacionProovedor.Add(myReader("telefono2").ToString())
                informacionProovedor.Add(myReader("direccion_web").ToString())
                informacionProovedor.Add(myReader("correo_electronico").ToString())
                informacionProovedor.Add(myReader("RFC").ToString())
                informacionProovedor.Add(myReader("cuenta_bancaria").ToString())


            End While


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacionProovedor
    End Function

    Friend Function ActualizarInformacionRegion(iD As String, codigoProvedor As String, nombreEmpreas As String, nombreTitular As String, direcionEmpresa As String, colonia As String, estadoEmpresa As String, empresaMunicipio As String, codigoPostal As String, numero1 As String, numero2 As String, direcionWeb As String, correoElectronico As String, rfc As String, cuentaBancaria As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = " UPDATE [dbo].[proovedor]
                                   SET [nombre] = '" + nombreEmpreas + "'
                                      ,[titular] = '" + nombreTitular + "'
                                      ,[direccion] =  '" + direcionEmpresa + "'
                                      ,[RFC] = '" + rfc + "'
                                      ,[colonia] = '" + colonia + "'
                                      ,[municipio] = '" + empresaMunicipio + "'
                                      ,[estado] = '" + estadoEmpresa + "'
                                      ,[direccion_web] = '" + direcionEmpresa + "'
                                      ,[correo_electronico] = '" + correoElectronico + "'
                                      ,[CP] = '" + codigoPostal + "'
                                      ,[cuenta_bancaria] = '" + cuentaBancaria + "'
                                      ,[telefono] = '" + numero1 + "'
                                      ,[telefono2] = '" + numero2 + "'
                                 WHERE  id= '" + iD + "' and estatus  =1
                                "


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function RegistrarProovedor(nombreEmpreas As String, nombreTitular As String, direcionEmpresa As String, colonia As String, estadoEmpresa As String, empresaMunicipio As String, codigoPostal As String, numero1 As String, numero2 As String, direcionWeb As String, correoElectronico As String, rfc As String, cuentaBancaria As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "INSERT INTO [dbo].[proovedor]
                                   ([nombre]
                                   ,[estatus]
                                   ,[titular]
                                   ,[direccion]
                                   ,[RFC]
                                   ,[colonia]
                                   ,[municipio]
                                   ,[estado]
                                   ,[direccion_web]
                                   ,[correo_electronico]
                                   ,[CP]
                                   ,[cuenta_bancaria]
                                   ,[telefono]
                                   ,[telefono2])
                             VALUES
                                   ('" + nombreEmpreas + "'
                                   ,1
                                   ,'" + nombreTitular + "'
                                   ,'" + direcionEmpresa + "'
                                   ,'" + rfc + "'
                                   ,'" + colonia + "'
                                   ,'" + empresaMunicipio + "'
                                   ," + estadoEmpresa + "
                                   ,'" + direcionEmpresa + "'
                                   ,'" + correoElectronico + "'
                                   ,'" + codigoPostal + "'
                                   ,'" + cuentaBancaria + "'
                                   ,'" + numero1 + "'
                                   ,'" + numero2 + "')"



        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function EliminarProovedor(v As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = " UPDATE [dbo].[proovedor]
                                   SET estatus = 2 
                                 WHERE  id= '" + v + "' and estatus  =1
                                "


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
End Class
