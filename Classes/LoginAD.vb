﻿Imports System.Data.SqlClient

Public Class LoginAD


    Public Function ConsultarExistencia(usuario As String) As Integer
        Dim Status As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select estatus from empleados where clave_trabajador ='" & usuario.Trim() & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                Status = myReader("estatus").ToString()
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return Status
    End Function


    Public Function ValidarCredenciales(usuario As String, pwd As String) As List(Of String)
        Dim validacion As New List(Of String)
        Dim userTemp As String = ""
        Dim pwdTemp As String = ""
        Dim puesto As String = ""
        Dim region As String = ""


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select clave_trabajador,pwd,puesto,region from empleadosactivos where clave_trabajador ='" & usuario.Trim() & "' and pwd = '" & pwd.Trim() & "' and estatus  = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                userTemp = myReader("clave_trabajador")
                pwdTemp = myReader("pwd")
                puesto = myReader("puesto")
                region = myReader("region")
            End While

            If userTemp.Equals(usuario) And pwdTemp.Equals(pwd) Then
                validacion.Add("TRUE")
                validacion.Add(puesto)
                validacion.Add(region)
            Else
                validacion.Add("FALSE")

            End If

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try



        Return validacion
    End Function

End Class
