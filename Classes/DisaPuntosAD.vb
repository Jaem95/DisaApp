﻿Imports System.Data.SqlClient
Public Class DisaPuntosAD
    Friend Function ConsultarDisaPuntos() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select puntos.id,prod.codigoDisa,prod.nombre,puntos.equivalente  
                                from productos as prod, disaPuntos as puntos 
                                where prod.codigo_barrras = puntos.codigoBarras and  prod.estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")

            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "equivalente"



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function CrearEquvialencia(selectedValue As String, equivalenciaPuntos As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = " INSERT INTO [dbo].[disaPuntos]
           ([codigoBarras]
           ,[equivalente])
     VALUES
           ('" + selectedValue + "'
           ,'" + equivalenciaPuntos + "')"


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function ConsultarEquivalenciaActual() As String
        Dim equivalencia As String = ""

        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT  [id]
                                  ,[disaPuntos]
                                  ,[montoCompra]
                              FROM [dbo].[EquivalenciaPuntoPrecio]")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                equivalencia = "Actualmente por cada  $" + myReader("montoCompra").ToString() + " de compra se dan:" + myReader("disaPuntos").ToString() + "DISA-Puntos"
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")
            equivalencia = "Erro en la consulta de equivalencia por compra"

        Finally
            connexio.Close()
        End Try



        Return equivalencia
    End Function

    Friend Function ConsultarProducto(idEquivalencia As String) As List(Of String)
        Dim codigoBarras As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select  codigoBarras,[equivalente] from [disaPuntos] where id = " + idEquivalencia + "")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                codigoBarras.Add(myReader("codigoBarras").ToString())
                codigoBarras.Add(myReader("equivalente").ToString())

            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return codigoBarras
    End Function

    Friend Function ActualizarEquivalencia(v As String, nuevaEquivelencia As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "UPDATE [dbo].[disaPuntos]
                                SET [equivalente] = '" + nuevaEquivelencia + "'
                                WHERE [id] = " + v + ""


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function EliminarEquivalencia(v As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "delete from [disaPuntos]  where ID = " + v + ""


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function EquivalenciaPrecio(disaPuntos As String, monto As String) As String
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "UPDATE [dbo].[EquivalenciaPuntoPrecio]
                               SET [disaPuntos] = '" + disaPuntos + "'
                                  ,[montoCompra] = '" + monto + "'"


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
End Class
