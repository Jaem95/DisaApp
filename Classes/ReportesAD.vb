﻿Imports System.Data.SqlClient

Friend Class ReportesAD
    Friend Function ReporteVendedor(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct
                                b.clave_trabajador, 
                                concat(a.nombre,' ',a.apellidos) as nombre,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador) as tiendas_asignadas,
                                (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador  and convert(date, a.fecha_registro, 103) > CONVERT ( date, @theDate,103 )) as tiendas_registradas,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from empleados a,empleadosactivos b,R" + region + "pedidos c where  a.clave_trabajador = b.clave_trabajador and b.puesto = 5  and c.vendedor = b.clave_trabajador
                                ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "ventas"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "tiendas_asignadas"
            ds.Tables(0).Columns(5).ColumnName = "tiendas_registradas"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteVendedorEspecifico_3(region As String, vendedor As String, selectedValue As String, Rango As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("")

        If Rango.Equals("Mayor") Then
            query = "                  	
		DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + selectedValue + ",dateadd(hour,-6,getdate())),103)
                                 
	 select top 1 sum (monto_cobrado) as monto,idPedido ,d.nombre,e.nombre  as nom,e.id from R" + region + "ProductosVenta a,R" + region + "pedidos b,rutaPOrCliente c,ruta d,cliente e
  where  a.idPedido  = b.id and c.idCliente = b.cliente  and c.idRuta =d.id  and d.tipoRuta like '%vendedor%' and e.id =b.cliente and e.id  =  c.idCliente  --and  d.empleado = b.vendedor
  and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) 
  and b.vendedor = '" + vendedor + "'
   group by  idPedido,d.nombre ,e.nombre ,e.id 
    order by monto desc"
        ElseIf Rango.Equals("Menor") Then
            query = "
                   	
		DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + selectedValue + ",dateadd(hour,-6,getdate())),103)
                                 
	 select top 1 sum (monto_cobrado) as monto,idPedido ,d.nombre,e.nombre as nom ,e.id from R" + region + "ProductosVenta a,R" + region + "pedidos b,rutaPOrCliente c,ruta d,cliente e
  where  a.idPedido  = b.id and c.idCliente = b.cliente  and c.idRuta =d.id  and d.tipoRuta like '%vendedor%' and e.id =b.cliente and e.id  =  c.idCliente  --and  d.empleado = b.vendedor
  and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) 
  and b.vendedor = '" + vendedor + "' 
   group by  idPedido,d.nombre ,e.nombre ,e.id 
    order by monto asc"
        End If


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("nom").ToString())
                informacion.Add(myReader("id").ToString())


            End While




        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Friend Function ReporteVendedorEspecifico_2(v As String, codigoTrabajadores As String, selectedValue As String, Rango As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("")
        If Rango.Equals("Mayor") Then
            query = "
                    DECLARE @theDate varchar(60)
                     SET @theDate = convert(varchar(10),dateadd(mm,-" + selectedValue + ",dateadd(hour,-6,getdate())),103)	
                     select  top 1  sum(monto_cobrado) as monto,b.cliente,e.nombre,e.id,c.nombre  as ruta
                     from R" + v + "ProductosVenta a,R" + v + "pedidos b, ruta c, rutaPorCliente d,cliente e 
                     where  b.id =a.idPedido  and  b.cliente = e.id and c.id  =d.idRuta and d.idCliente  = e.id   and b.cliente =  d.idCliente 
                     and c.tipoRuta like '%vendedor%' and b.vendedor = '" + codigoTrabajadores + "' 
                     and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) 
                     group by  idPedido,b.cliente,e.nombre,e.id,c.nombre
                     order by monto desc"
        ElseIf Rango.Equals("Menor") Then
            query = "
                    DECLARE @theDate varchar(60)
                     SET @theDate = convert(varchar(10),dateadd(mm,-" + selectedValue + ",dateadd(hour,-6,getdate())),103)	
                     select  top 1  sum(monto_cobrado) as monto,b.cliente,e.nombre,e.id,c.nombre  as ruta
                     from R" + v + "ProductosVenta a,R" + v + "pedidos b, ruta c, rutaPorCliente d,cliente e 
                     where  b.id =a.idPedido  and  b.cliente = e.id and c.id  =d.idRuta and d.idCliente  = e.id   and b.cliente =  d.idCliente 
                     and c.tipoRuta like '%vendedor%' and b.vendedor = '" + codigoTrabajadores + "' 
                     and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) 
                     group by  idPedido,b.cliente,e.nombre,e.id,c.nombre
                     order by monto asc"
        End If
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("cliente").ToString())
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("ruta").ToString())


            End While




        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Friend Function ReporteVendedorEspecifico(region As String, periodo As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct
                                b.clave_trabajador, 
                                concat(a.nombre,' ',a.apellidos) as nombre,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador) as tiendas_asignadas,
                                (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador  and convert(date, a.fecha_registro, 103) > CONVERT ( date, @theDate,103 )) as tiendas_registradas,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                ,
								case when  (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador) end as promedio_tienda,
								(select top 1 max( x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador and convert(date, x.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idPedido order  by x.idPedido  desc) as venta_maxima,
								(select top 1 max( x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador and convert(date, x.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idPedido order  by x.idPedido  asc) as venta_minima
                                from empleados a,empleadosactivos b,R" + region + "pedidos c where  a.clave_trabajador = b.clave_trabajador and b.puesto = 5  and c.vendedor = b.clave_trabajador and c.vendedor = '" + trabajador + "'
                                ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("ventas").ToString())
                informacion.Add(myReader("pedidos").ToString())
                informacion.Add(myReader("tiendas_asignadas").ToString())
                informacion.Add(myReader("tiendas_registradas").ToString())
                informacion.Add(myReader("promedio").ToString())
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("promedio_tienda").ToString())
                informacion.Add(myReader("venta_maxima").ToString())
                informacion.Add(myReader("venta_minima").ToString())

            End While




        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Friend Function ReporteVendedor(region As String, periodo As String, trabajador As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct
                                b.clave_trabajador, 
                                concat(a.nombre,' ',a.apellidos) as nombre,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador) as tiendas_asignadas,
                                (select count(*) from cliente a where a.nombreRepresentante = b.clave_trabajador  and convert(date, a.fecha_registro, 103) > CONVERT ( date, @theDate,103 )) as tiendas_registradas,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from empleados a,empleadosactivos b,R" + region + "pedidos c where  a.clave_trabajador = b.clave_trabajador and b.puesto = 5  and c.vendedor = b.clave_trabajador and c.vendedor = '" + trabajador + "'
                                ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "ventas"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "tiendas_asignadas"
            ds.Tables(0).Columns(5).ColumnName = "tiendas_registradas"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteRepartidores(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador, concat(a.nombre,' ',a.apellidos) as nombre, d.countD,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.tiempo) from R" + region + "Pedidos x  where  x.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio_tiempo,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select TOP 1 dev.tipoDevolucion from (select h.tipoDevolucion, count(h.tipoDevolucion) as 'ocurrencia' from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) group by h.tipoDevolucion) as dev order by dev.ocurrencia desc) as tipoDev,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos
                                from empleados a,empleadosactivos b,R3pedidos c, 
                                (select r.empleado as emp, Count(r.empleado) as countD from empleados as e, ruta as r, (select r.empleado as ed, p.id, p.status, rpc.idCliente 
                                from ruta as r,R" + region + "Pedidos as p, rutaPorCliente as rpc where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta) and(r.idLocalidad=" + region + ") and (p.cliente=rpc.idCliente) and (p.status=2) and (convert(date, p.fecha, 103) > CONVERT ( date, @theDate,103 ))) as nest where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" + region + ") and (r.tipoRuta='repartidor') and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado) group by r.empleado) as d
                                where  a.clave_trabajador = b.clave_trabajador and(b.clave_trabajador=d.emp)")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "countD"
            ds.Tables(0).Columns(3).ColumnName = "promedio_tiempo"
            ds.Tables(0).Columns(4).ColumnName = "devoluciones"
            ds.Tables(0).Columns(5).ColumnName = "tipoDev"
            ds.Tables(0).Columns(6).ColumnName = "ventas"
            ds.Tables(0).Columns(7).ColumnName = "pedidos"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteRepartidor(region As String, periodo As String, trabajador As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador, concat(a.nombre,' ',a.apellidos) as nombre, d.countD,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.tiempo) from R" + region + "Pedidos x  where  x.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio_tiempo,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select TOP 1 dev.tipoDevolucion from (select h.tipoDevolucion, count(h.tipoDevolucion) as 'ocurrencia' from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) group by h.tipoDevolucion) as dev order by dev.ocurrencia desc) as tipoDev,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos
                                from empleados a,empleadosactivos b,R3pedidos c, 
                                (select r.empleado as emp, Count(r.empleado) as countD from empleados as e, ruta as r, (select r.empleado as ed, p.id, p.status, rpc.idCliente 
                                from ruta as r,R" + region + "Pedidos as p, rutaPorCliente as rpc where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta) and(r.idLocalidad=" + region + ") and (p.cliente=rpc.idCliente) and (p.status=2) and (convert(date, p.fecha, 103) > CONVERT ( date, @theDate,103 ))) as nest where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" + region + ") and (r.tipoRuta='repartidor') and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado) group by r.empleado) as d
                                where  a.clave_trabajador = b.clave_trabajador and(b.clave_trabajador=d.emp)
                                and (b.clave_trabajador='" + trabajador + "')")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "countD"
            ds.Tables(0).Columns(3).ColumnName = "promedio_tiempo"
            ds.Tables(0).Columns(4).ColumnName = "devoluciones"
            ds.Tables(0).Columns(5).ColumnName = "tipoDev"
            ds.Tables(0).Columns(6).ColumnName = "ventas"
            ds.Tables(0).Columns(7).ColumnName = "pedidos"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteRepartidorEspecifico(region As String, periodo As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador, concat(a.nombre,' ',a.apellidos) as nombre, d.countD,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.tiempo) from R" + region + "Pedidos x  where  x.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio_tiempo,
                                (select max(x.tiempo) from R" + region + "Pedidos x  where  x.vendedor = b.clave_trabajador) as max_tiempo,
                                (select min(x.tiempo) from R" + region + "Pedidos x  where  x.vendedor = b.clave_trabajador) as min_tiempo,
                                case when (select count(*) from R3pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R3ProductosVenta x ,R3pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from R3pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio_venta,
								(select max( x.monto_cobrado) from R3ProductosVenta x ,R3pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador and convert(date, x.fecha, 103) > CONVERT ( date, @theDate,103 )) as venta_maxima,
								(select min( x.monto_cobrado) from R3ProductosVenta x ,R3pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador and convert(date, x.fecha, 103) > CONVERT ( date, @theDate,103 )) as venta_minima,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select TOP 1 dev.tipoDevolucion from (select h.tipoDevolucion, count(h.tipoDevolucion) as 'ocurrencia' from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) group by h.tipoDevolucion) as dev order by dev.ocurrencia desc) as tipoDev,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos
                                from empleados a,empleadosactivos b,R3pedidos c, 
                                (select r.empleado as emp, Count(r.empleado) as countD from empleados as e, ruta as r, (select r.empleado as ed, p.id, p.status, rpc.idCliente 
                                from ruta as r,R" + region + "Pedidos as p, rutaPorCliente as rpc where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta) and(r.idLocalidad=" + region + ") and (p.cliente=rpc.idCliente) and (p.status=2) and (convert(date, p.fecha, 103) > CONVERT ( date, @theDate,103 ))) as nest where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" + region + ") and (r.tipoRuta='repartidor') and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado) group by r.empleado) as d
                                where  a.clave_trabajador = b.clave_trabajador and(b.clave_trabajador=d.emp)
                                and (b.clave_trabajador='" + trabajador + "')")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("clave_trabajador").ToString())
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("countD").ToString())
                informacion.Add(myReader("promedio_tiempo").ToString())
                informacion.Add(myReader("max_tiempo").ToString())
                informacion.Add(myReader("min_tiempo").ToString())
                informacion.Add(myReader("promedio_venta").ToString())
                informacion.Add(myReader("venta_maxima").ToString())
                informacion.Add(myReader("venta_minima").ToString())
                informacion.Add(myReader("devoluciones").ToString())
                informacion.Add(myReader("tipoDev").ToString())
                informacion.Add(myReader("ventas").ToString())
                informacion.Add(myReader("pedidos").ToString())
            End While




        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    'Bodegueros Shit

    Friend Function ReporteBodegueros(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador, concat(a.nombre,' ',a.apellidos) as nombre,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select TOP 1 dev.tipoDevolucion from (select h.tipoDevolucion, count(h.tipoDevolucion) as 'ocurrencia' from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) group by h.tipoDevolucion) as dev order by dev.ocurrencia desc) as tipoDev,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas
                                from empleados a,empleadosactivos b,R" + region + "pedidos c
                                where  a.clave_trabajador = b.clave_trabajador and b.puesto=3")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "devoluciones"
            ds.Tables(0).Columns(3).ColumnName = "tipoDev"
            ds.Tables(0).Columns(4).ColumnName = "ventas"
            ds.Tables(0).Columns(5).ColumnName = "pedidos"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteBodeguero(region As String, periodo As String, trabajador As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador, concat(a.nombre,' ',a.apellidos) as nombre,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select TOP 1 dev.tipoDevolucion from (select h.tipoDevolucion, count(h.tipoDevolucion) as 'ocurrencia' from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) group by h.tipoDevolucion) as dev order by dev.ocurrencia desc) as tipoDev,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas
                                from empleados a,empleadosactivos b,R" + region + "pedidos c
                                where  a.clave_trabajador = b.clave_trabajador and b.puesto=3
                                and (b.clave_trabajador='" + trabajador + "')")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "devoluciones"
            ds.Tables(0).Columns(3).ColumnName = "tipoDev"
            ds.Tables(0).Columns(4).ColumnName = "ventas"
            ds.Tables(0).Columns(5).ColumnName = "pedidos"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
    Friend Function ReporteBodegueroEspecifico(region As String, periodo As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador, concat(a.nombre,' ',a.apellidos) as nombre,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select TOP 1 dev.tipoDevolucion from (select h.tipoDevolucion, count(h.tipoDevolucion) as 'ocurrencia' from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, a.fecha, 103) > CONVERT ( date, @theDate,103 ) group by h.tipoDevolucion) as dev order by dev.ocurrencia desc) as tipoDev,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas
                                from empleados a,empleadosactivos b,R" + region + "pedidos c
                                where  a.clave_trabajador = b.clave_trabajador and b.puesto=3
                                and (b.clave_trabajador='" + trabajador + "')")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("clave_trabajador").ToString())
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("devoluciones").ToString())
                informacion.Add(myReader("tipoDev").ToString())
                informacion.Add(myReader("ventas").ToString())
                informacion.Add(myReader("pedidos").ToString())
            End While




        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Friend Function ReporteBodegueroEspecifico_2(v As String, codigoTrabajadores As String, selectedValue As String, Rango As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("")
        If Rango.Equals("Mayor") Then
            query = "DECLARE @theDate varchar(60)
                        SET @theDate = convert(varchar(10),dateadd(mm,-" + selectedValue + ",dateadd(hour,-6,getdate())),103)
                        select top 1 count(r.nombre) as conteo,r.nombre 
                        from r" + v + "pedidos as rp, ruta as r 
                        where rp.vendedor = r.empleado 
                        and convert(date, rp.fecha, 103) > CONVERT ( date, @theDate,103 ) 
                        group by r.nombre 
                        order by conteo desc"
        ElseIf Rango.Equals("Menor") Then
            query = "DECLARE @theDate varchar(60)
                        SET @theDate = convert(varchar(10),dateadd(mm,-" + selectedValue + ",dateadd(hour,-6,getdate())),103)
                        select top 1 count(r.nombre) as conteo,r.nombre 
                        from r" + v + "pedidos as rp, ruta as r 
                        where rp.vendedor = r.empleado 
                        and convert(date, rp.fecha, 103) > CONVERT ( date, @theDate,103 ) 
                        group by r.nombre 
                        order by conteo asc"
        End If
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("conteo").ToString())
                informacion.Add(myReader("nombre").ToString())

            End While




        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Friend Function RecorProductos(region As String, condition As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa, p.nombre as producto, inv.cantidad, cp.nombre as categoria, prov.nombre as proveedor
                                from R" + region + "Inventario as inv, productos as p, proovedor as prov, categoriasProductos as cp
                                where inv.idProductoCB=p.codigo_barrras
                                and p.proovedor=prov.id
                                and p.categoria=cp.id
                                order by " + condition)
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "producto"
            ds.Tables(0).Columns(2).ColumnName = "cantidad"
            ds.Tables(0).Columns(3).ColumnName = "categoria"
            ds.Tables(0).Columns(4).ColumnName = "proveedor"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function RecorProductosInd(region As String, codigo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa, p.nombre as producto, inv.cantidad, cp.nombre as categoria, prov.nombre as proveedor
                                from R" + region + "Inventario as inv, productos as p, proovedor as prov, categoriasProductos as cp
                                where inv.idProductoCB=p.codigo_barrras
                                and p.proovedor=prov.id
                                and p.categoria=cp.id
                                and p.codigoDisa='" + codigo + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "producto"
            ds.Tables(0).Columns(2).ColumnName = "cantidad"
            ds.Tables(0).Columns(3).ColumnName = "categoria"
            ds.Tables(0).Columns(4).ColumnName = "proveedor"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function HistorialPedidos(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select pp.idPedido, ped.fecha, ped.vendedor,
                                prod.codigoDisa,CONCAT(prod.nombre,' ',prod.descripcion) as producto,pp.precioUnitario,pp.cantidadProductos,pp.precioUnitario*pp.cantidadProductos as importe
                                from R" + region + "Pedidos as ped, R" + region + "ProductosPedido as pp, productos as prod
                                where ped.id=pp.idPedido and pp.idProductoCB=prod.codigo_barrras
                                and convert(date, ped.fecha, 103) > CONVERT ( date, @theDate,103 )")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "idPedido"
            ds.Tables(0).Columns(1).ColumnName = "fecha"
            ds.Tables(0).Columns(2).ColumnName = "vendedor"
            ds.Tables(0).Columns(3).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(4).ColumnName = "producto"
            ds.Tables(0).Columns(5).ColumnName = "precioUnitario"
            ds.Tables(0).Columns(6).ColumnName = "cantidadProductos"
            ds.Tables(0).Columns(7).ColumnName = "importe"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function HistorialPedidosFolio(region As String, periodo As String, codigo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select pp.idPedido, ped.fecha, ped.vendedor,
                                prod.codigoDisa,CONCAT(prod.nombre,' ',prod.descripcion) as producto,pp.precioUnitario,pp.cantidadProductos,pp.precioUnitario*pp.cantidadProductos as importe
                                from R" + region + "Pedidos as ped, R" + region + "ProductosPedido as pp, productos as prod
                                where ped.id=pp.idPedido and pp.idProductoCB=prod.codigo_barrras
                                and convert(date, ped.fecha, 103) > CONVERT ( date, @theDate,103 )
                                and ped.id='" + codigo + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "idPedido"
            ds.Tables(0).Columns(1).ColumnName = "fecha"
            ds.Tables(0).Columns(2).ColumnName = "vendedor"
            ds.Tables(0).Columns(3).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(4).ColumnName = "producto"
            ds.Tables(0).Columns(5).ColumnName = "precioUnitario"
            ds.Tables(0).Columns(6).ColumnName = "cantidadProductos"
            ds.Tables(0).Columns(7).ColumnName = "importe"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteClientes(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select c.id,c.nombre as tienda, r.nombre as ruta,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.cliente = c.id) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                case when (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.cliente = c.id) / (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.cliente = c.id and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones
                                from ruta as r, cliente as c, rutaPorCliente as rpc
                                where rpc.idRuta=r.id and rpc.idCliente=c.id")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_cliente"
            ds.Tables(0).Columns(1).ColumnName = "tienda"
            ds.Tables(0).Columns(2).ColumnName = "ruta"
            ds.Tables(0).Columns(3).ColumnName = "ventas"
            ds.Tables(0).Columns(4).ColumnName = "pedidos"
            ds.Tables(0).Columns(5).ColumnName = "promedio"
            ds.Tables(0).Columns(6).ColumnName = "devoluciones"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteClientesButton(region As String, periodo As String, codigo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select c.id,c.nombre as tienda, r.nombre as ruta,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.cliente = c.id) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                case when (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.cliente = c.id) / (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.cliente = c.id and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones
                                from ruta as r, cliente as c, rutaPorCliente as rpc
                                where rpc.idRuta=r.id and rpc.idCliente=c.id
                                and c.id='" + codigo + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_cliente"
            ds.Tables(0).Columns(1).ColumnName = "tienda"
            ds.Tables(0).Columns(2).ColumnName = "ruta"
            ds.Tables(0).Columns(3).ColumnName = "ventas"
            ds.Tables(0).Columns(4).ColumnName = "pedidos"
            ds.Tables(0).Columns(5).ColumnName = "promedio"
            ds.Tables(0).Columns(6).ColumnName = "devoluciones"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteClienteEspecifico(region As String, periodo As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select c.id,c.nombre as cliente,c.direccion, CONCAT(e.nombre,' ',e.apellidos) as representante,r.nombre as ruta,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.cliente = c.id) as ventas,
                                (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                case when (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.cliente = c.id) / (select count(*) from R" + region + "pedidos a where a.cliente = c.id and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.cliente = c.id and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                c.disaPuntos as puntos
                                from ruta as r, cliente as c, rutaPorCliente as rpc, empleados as e
                                where rpc.idRuta=r.id and rpc.idCliente=c.id and e.clave_trabajador=c.nombreRepresentante
                                and c.id='" + trabajador + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("id").ToString())
                informacion.Add(myReader("cliente").ToString())
                informacion.Add(myReader("direccion").ToString())
                informacion.Add(myReader("representante").ToString())
                informacion.Add(myReader("ruta").ToString())
                informacion.Add(myReader("ventas").ToString())
                informacion.Add(myReader("pedidos").ToString())
                informacion.Add(myReader("promedio").ToString())
                informacion.Add(myReader("devoluciones").ToString())
                informacion.Add(myReader("puntos").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function
    Friend Function ReporteClienteGastadosEspecifico(region As String, periodo As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select p.cliente, SUM(dp.equivalente*pp.cantidadProductos) as puntos
                                from R" + region + "ProductosPedido as pp, disaPuntos as dp, R" + region + "Pedidos as p
                                where pp.idProductoCB=dp.codigoBarras and pp.tipoPromo='9994999'
                                and pp.idPedido=p.id and p.cliente='" + trabajador + "'
                                and convert(date, p.fecha, 103) > CONVERT ( date, @theDate,103 )
                                group by p.cliente")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("puntos").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function
    Friend Function ReporteProductosAdmin(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct prov.nombre as fabricante, prod.codigoDisa, prod.nombre as producto,
                                (select count(x.idProductoCB) from R" + region + "ProductosPedido as x, R" + region + "Pedidos as y 
                                where x.idProductoCB=prod.codigo_barrras
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as pedidos,
                                (select sum(x.cantidadProductos) as suma from R" + region + "ProductosPedido as x, R" + region + "Pedidos as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as cantidad,
                                (select sum(x.cantidadProductos*x.precioUnitario) as suma from R" + region + "ProductosPedido as x, R" + region + "Pedidos as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as importe,
                                case when (select count(*) from R" + region + "pedidos a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and x.idProductoCB = prod.codigo_barrras) / (select count(*) from R" + region + "pedidos a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from proovedor as prov, productos as prod")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "fabricante"
            ds.Tables(0).Columns(1).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(2).ColumnName = "producto"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "cantidad"
            ds.Tables(0).Columns(5).ColumnName = "importe"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteProductosAdminButton(region As String, periodo As String, codigo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct prov.nombre as fabricante, prod.codigoDisa, prod.nombre as producto,
                                (select count(x.idProductoCB) from R" + region + "ProductosPedido as x, R" + region + "Pedidos as y 
                                where x.idProductoCB=prod.codigo_barrras
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as pedidos,
                                (select sum(x.cantidadProductos) as suma from R" + region + "ProductosPedido as x, R" + region + "Pedidos as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as cantidad,
                                (select sum(x.cantidadProductos*x.precioUnitario) as suma from R" + region + "ProductosPedido as x, R" + region + "Pedidos as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as importe,
                                case when (select count(*) from R" + region + "pedidos a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and x.idProductoCB = prod.codigo_barrras) / (select count(*) from R" + region + "pedidos a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from proovedor as prov, productos as prod
                                where prod.codigoDisa='" + codigo + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "fabricante"
            ds.Tables(0).Columns(1).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(2).ColumnName = "producto"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "cantidad"
            ds.Tables(0).Columns(5).ColumnName = "importe"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ReporteProductosAdminEspecifico(region As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @month varchar(60)
                                SET @month = convert(varchar(10),dateadd(mm,-1,dateadd(hour,-6,getdate())),103)
                                DECLARE @thri varchar(60)
                                SET @thri = convert(varchar(10),dateadd(mm,-3,dateadd(hour,-6,getdate())),103)
                                DECLARE @sem varchar(60)
                                SET @sem = convert(varchar(10),dateadd(mm,-6,dateadd(hour,-6,getdate())),103)
                                DECLARE @year varchar(60)
                                SET @year = convert(varchar(10),dateadd(mm,-12,dateadd(hour,-6,getdate())),103)
                                select p.nombre,p.descripcion,p.presentacion,p.cantidadxcaja,
                                case when (select sum(y.canProduct) from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @month,103 )) = 0 or p.cantidadxcaja = 0 then '0' else (select sum(y.canProduct) as suma from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras and convert(date, y.fecha, 103) > CONVERT ( date, @month,103 ))/p.cantidadxcaja end as mes,
                                case when (select sum(y.canProduct) from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @thri,103 ) ) = 0 or p.cantidadxcaja = 0 then '0' else (select sum(y.canProduct) as suma from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras and convert(date, y.fecha, 103) > CONVERT ( date, @thri,103 ))/p.cantidadxcaja end as trim,
                                case when (select sum(y.canProduct) from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @sem,103 )) = 0 or p.cantidadxcaja = 0 then '0' else (select sum(y.canProduct) as suma from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras and convert(date, y.fecha, 103) > CONVERT ( date, @sem,103 ))/p.cantidadxcaja end as sem,
                                case when (select sum(y.canProduct) from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @year,103 )) = 0 or p.cantidadxcaja = 0 then '0' else (select sum(y.canProduct) as suma from R" + region + "ProductosVenta as y where y.idProductoCB=p.codigo_barrras and convert(date, y.fecha, 103) > CONVERT ( date, @year,103 ))/p.cantidadxcaja end as anual
                                from productos as p
                                where p.codigoDisa='" + trabajador + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("descripcion").ToString())
                informacion.Add(myReader("presentacion").ToString())
                informacion.Add(myReader("cantidadxcaja").ToString())
                informacion.Add(myReader("mes").ToString())
                informacion.Add(myReader("trim").ToString())
                informacion.Add(myReader("sem").ToString())
                informacion.Add(myReader("anual").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function
    Friend Function ReporteVentasRegionales(region As String, periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador,d.nombreGerente, b.region,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from empleados a,empleadosactivos b,R" + region + "pedidos c, region as d where  a.clave_trabajador = b.clave_trabajador and b.puesto = 5  and c.vendedor = b.clave_trabajador and d.id=b.region")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "nombreGerente"
            ds.Tables(0).Columns(2).ColumnName = "region"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "devoluciones"
            ds.Tables(0).Columns(5).ColumnName = "ventas"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
    Friend Function ReporteVentasRegionalesEspecificas(region As String, periodo As String, trabajador As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct b.clave_trabajador,d.nombreGerente, b.region,
                                (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as pedidos,
                                (select count(*) from R" + region + "pedidos a, R" + region + "Devoluciones h where a.vendedor = b.clave_trabajador and a.id=h.idPedido and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) as devoluciones,
                                (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) as ventas,
                                case when (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from R" + region + "ProductosVenta x ,R" + region + "pedidos y  where  y.id = x.idPedido and y.vendedor = b.clave_trabajador) / (select count(*) from R" + region + "pedidos a where a.vendedor = b.clave_trabajador and convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from empleados a,empleadosactivos b,R" + region + "pedidos c, region as d where  a.clave_trabajador = b.clave_trabajador and b.puesto = 5  and c.vendedor = b.clave_trabajador and d.id=b.region and b.clave_trabajador='" + trabajador + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("clave_trabajador").ToString())
                informacion.Add(myReader("nombreGerente").ToString())
                informacion.Add(myReader("region").ToString())
                informacion.Add(myReader("pedidos").ToString())
                informacion.Add(myReader("devoluciones").ToString())
                informacion.Add(myReader("ventas").ToString())
                informacion.Add(myReader("promedio").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function
    Friend Function ReporteProductosAdminGeneral(periodo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct prov.nombre as fabricante, prod.codigoDisa, prod.nombre as producto,
                                (select count(x.idProductoCB) from (select * from R3ProductosPedido union select * from R4ProductosPedido union select * from R5ProductosPedido union select * from R6ProductosPedido union select * from R9ProductosPedido) as x, (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) as y 
                                where x.idProductoCB=prod.codigo_barrras
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as pedidos,
                                (select sum(x.cantidadProductos) as suma from (select * from R3ProductosPedido union select * from R4ProductosPedido union select * from R5ProductosPedido union select * from R6ProductosPedido union select * from R9ProductosPedido) as x, (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as cantidad,
                                (select sum(x.cantidadProductos*x.precioUnitario) as suma from (select * from R3ProductosPedido union select * from R4ProductosPedido union select * from R5ProductosPedido union select * from R6ProductosPedido union select * from R9ProductosPedido) as x, (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as importe,
                                case when (select count(*) from (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from (select * from R3ProductosVenta union select * from R4ProductosVenta union select * from R5ProductosVenta union select * from R6ProductosVenta union select * from R9ProductosVenta) x ,(select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) y  where  y.id = x.idPedido and x.idProductoCB = prod.codigo_barrras) / (select count(*) from (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from proovedor as prov, productos as prod")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "fabricante"
            ds.Tables(0).Columns(1).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(2).ColumnName = "producto"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "cantidad"
            ds.Tables(0).Columns(5).ColumnName = "importe"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
    Friend Function ReporteProductosAdminButtonGeneral(periodo As String, codigo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("DECLARE @theDate varchar(60)
                                SET @theDate = convert(varchar(10),dateadd(mm,-" + periodo + ",dateadd(hour,-6,getdate())),103)
                                select distinct prov.nombre as fabricante, prod.codigoDisa, prod.nombre as producto,
                                (select count(x.idProductoCB) from (select * from R3ProductosPedido union select * from R4ProductosPedido union select * from R5ProductosPedido union select * from R6ProductosPedido union select * from R9ProductosPedido) as x, (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) as y 
                                where x.idProductoCB=prod.codigo_barrras
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as pedidos,
                                (select sum(x.cantidadProductos) as suma from (select * from R3ProductosPedido union select * from R4ProductosPedido union select * from R5ProductosPedido union select * from R6ProductosPedido union select * from R9ProductosPedido) as x, (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as cantidad,
                                (select sum(x.cantidadProductos*x.precioUnitario) as suma from (select * from R3ProductosPedido union select * from R4ProductosPedido union select * from R5ProductosPedido union select * from R6ProductosPedido union select * from R9ProductosPedido) as x, (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) as y 
                                where x.idProductoCB=prod.codigo_barrras 
                                and x.idPedido=y.id 
                                and convert(date, y.fecha, 103) > CONVERT ( date, @theDate,103 ) group by x.idProductoCB) as importe,
                                case when (select count(*) from (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) ) = 0 then '0' else (select sum(x.monto_cobrado) from (select * from R3ProductosVenta union select * from R4ProductosVenta union select * from R5ProductosVenta union select * from R6ProductosVenta union select * from R9ProductosVenta) x ,(select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) y  where  y.id = x.idPedido and x.idProductoCB = prod.codigo_barrras) / (select count(*) from (select * from R3pedidos union select * from R4pedidos union select * from R5pedidos union select * from R6pedidos union select * from R9pedidos) a where convert(date, fecha, 103) > CONVERT ( date, @theDate,103 ) )  end as promedio
                                from proovedor as prov, productos as prod
                                where prod.codigoDisa='" + codigo + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "fabricante"
            ds.Tables(0).Columns(1).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(2).ColumnName = "producto"
            ds.Tables(0).Columns(3).ColumnName = "pedidos"
            ds.Tables(0).Columns(4).ColumnName = "cantidad"
            ds.Tables(0).Columns(5).ColumnName = "importe"
            ds.Tables(0).Columns(6).ColumnName = "promedio"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
    Friend Function ReporteCodigoDisa() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa, p.nombre, '1' as cantidad, dp.equivalente
                                from productos as p, disaPuntos as dp
                                where dp.codigoBarras=p.codigo_barrras
                                and p.estatus=1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "cantidad"
            ds.Tables(0).Columns(3).ColumnName = "equivalente"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
    Friend Function ReporteCodigoDisaButton(codigo As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa, p.nombre, '1' as cantidad, dp.equivalente
                                from productos as p, disaPuntos as dp
                                where dp.codigoBarras=p.codigo_barrras
                                and p.codigoDisa='" + codigo + "'
                                and p.estatus=1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "cantidad"
            ds.Tables(0).Columns(3).ColumnName = "equivalente"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
    Friend Function ReporteCodigoDisaEspecifico(codigo As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa, p.nombre, '1' as cantidad, dp.equivalente,p.descripcion,p.presentacion,p.cantidadxcaja,p.urlfoto
                                from productos as p, disaPuntos as dp
                                where dp.codigoBarras=p.codigo_barrras
                                and p.codigoDisa='" + codigo + "'
                                and p.estatus=1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try

            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("codigoDisa").ToString())
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("cantidad").ToString())
                informacion.Add(myReader("equivalente").ToString())
                informacion.Add(myReader("descripcion").ToString())
                informacion.Add(myReader("presentacion").ToString())
                informacion.Add(myReader("cantidadxcaja").ToString())
                informacion.Add(myReader("urlfoto").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function
End Class
