﻿Imports System.Data.SqlClient

Friend Class InventariosAD
    Friend Function ConsultarExistencia(codigoBarras As String, v As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        query = "select * from R" + v + "inventario where [idProductoCB] = " + codigoBarras + ""

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected < 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion


    End Function

    Friend Function ActualizarInventario(codigoBarras As String, v As String, cantidad As String, fecha As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        query = "update R" + v + "inventario set  cantidad = " + cantidad + ", caducidad = '" + fecha + "' where [idProductoCB] = " + codigoBarras + ""

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function InsertarNuevoProducto(codigoBarras As String, text1 As String, text2 As String, v1 As String, v2 As String, v3 As String, v4 As String, ubicacion As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        query = "INSERT INTO [dbo].[R" + ubicacion + "inventario]
                                   ([idProductoCB]
                                   ,[cantidad]
                                   ,[caducidad]
                                   ,[cantidadLimite]
                                   ,[cantidadMinima]
                                   ,[status]
                                   ,[categoria])
                             VALUES
                                   (" + codigoBarras + "
                                   ," + text1 + "
                                   ,'" + text2 + "'
                                   ," + v1 + "
                                   ," + v2 + "
                                   ," + v3 + "
                                   ," + v4 + ")"

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
End Class
