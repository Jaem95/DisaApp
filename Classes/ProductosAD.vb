﻿Imports System.Data.SqlClient
Public Class ProductosAD

    Public Function ConsultarProductos(condicion As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select prd.codigoDisa, prd.codigo_barrras, prd.nombre, prd.descripcion, prd.presentacion, prd.cantidadxcaja, cat.nombre as clas
                                from productos as prd , categoriasProductos as cat
                                where  prd.categoria = cat.id and ( prd.estatus = 1 and cat.estatus = 1) order by " & condicion & " ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "codigo_barrras"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "descripcion"
            ds.Tables(0).Columns(4).ColumnName = "presentacion"
            ds.Tables(0).Columns(5).ColumnName = "cantidadxcaja"
            ds.Tables(0).Columns(6).ColumnName = "clas"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function


    Public Function ConsultarProductoEspecifico(codigoDisa As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select prd.codigoDisa, prd.codigo_barrras, prd.nombre, prd.descripcion, prd.presentacion, prd.cantidadxcaja, cat.nombre as clas
                                from productos as prd , categoriasProductos as cat
                                where  prd.categoria = cat.id and ( prd.estatus = 1 and cat.estatus = 1)  and prd.codigoDisa = '" & codigoDisa & "'  ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "codigo_barrras"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "descripcion"
            ds.Tables(0).Columns(4).ColumnName = "presentacion"
            ds.Tables(0).Columns(5).ColumnName = "cantidadxcaja"
            ds.Tables(0).Columns(6).ColumnName = "clas"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function


    Public Function ConsultarProductoEnInventario(region As String, condicion As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("  select prod.codigoDisa,prod.codigo_barrras,prod.nombre,cp.nombre as clasificacion,datediff(month,sysdatetime(),convert(datetime,inv.caducidad)) as caducidad,inv.cantidadMinima 
                              from  R" & region & "inventario as inv,productos as prod,categoriasProductos as cp
                              where prod.categoria = cp.id   and inv.idProductoCB = prod.codigo_barrras order by " & condicion & "")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "codigo_barrras"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "clasificacion"
            ds.Tables(0).Columns(4).ColumnName = "caducidad"
            ds.Tables(0).Columns(5).ColumnName = "cantidadLimite"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function


    Public Function ConsultarProductoEnInventarioEspecifico(region As String, codigoBarras As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("  select prod.codigoDisa,prod.codigo_barrras,prod.nombre,cp.nombre as clasificacion,datediff(month,sysdatetime(),convert(datetime,inv.caducidad)) as caducidad,inv.cantidadLimite 
                              from  R" & region & "inventario as inv,productos as prod,categoriasProductos as cp
                              where prod.categoria = cp.id   and inv.idProductoCB = prod.codigo_barrras and inv.idProductoCB = " & codigoBarras & " ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "codigo_barrras"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "clasificacion"
            ds.Tables(0).Columns(4).ColumnName = "caducidad"
            ds.Tables(0).Columns(5).ColumnName = "cantidadLimite"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Public Function ActualizarCaducidadCantidadLimite(codigoBarras As String, cantidadLimite As String, caducidadLimite As String, region As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update R" & region & "inventario  set  cantidadMinima = '" & cantidadLimite & "' ,caducidad ='" & caducidadLimite & "'  where idProductoCB = '" & codigoBarras & "'  and status = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function


End Class
