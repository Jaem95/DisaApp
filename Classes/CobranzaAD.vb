﻿Imports System.Data.SqlClient

Friend Class CobranzaAD
    Friend Function InsertarFicha(fileName As String, text1 As String, text2 As String, txtDiferencia As String, text3 As String, v As String, idTran As String, ubicacion As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("
                                INSERT INTO [dbo].[tbl_CobranzaFichas]
                                           ([foto_ficha]
                                           ,[cantidad_ingresada]
                                           ,[cantidad_esperada]
                                           ,[diferencia]
                                           ,[repartidor]
                                           ,[fecha]
                                           ,[estatus])
                                     VALUES
                                           ('" + fileName + "'
                                           ," + text1 + "
                                           ," + text2 + "
                                           ," + txtDiferencia + "
                                           ,'" + text3 + "'
                                           ,'" + v + "'
                                           ,1 ); 
                                            
                                           update R" + ubicacion + "ProductosVenta set status = 2 where id = " + idTran + "
                                           
                                           ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function InsertarPago(text1 As String, text2 As String, text3 As String, text4 As String, v As String, idTran As String, ubicacion As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("INSERT INTO [dbo].[tbl_CobranzaCaja]
                                           ([cantidad_ingresada]
                                           ,[cantidad_esperada]
                                           ,[diferencia]
                                           ,[repartidor]
                                           ,[fecha]
                                           ,[estatus])
                                     VALUES
                                           (" + text1 + "
                                           ," + text2 + "
                                           ," + text3 + "
                                           ,'" + text4 + "'
                                           ,'" + v + "'
                                           ,1);
                                           
                                             update R" + ubicacion + "ProductosVenta set status = 2 where id = " + idTran + "
                                           ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
End Class
