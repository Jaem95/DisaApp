﻿Imports System.Data.SqlClient

Public Class ClientesAD
    Friend Function ConsultarClientes() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
      ,[nombre]
      ,[nombreRepresentante]
      ,[localidad]
      ,[direccion]
      ,concat(cast(latitud as decimal(19,3)) ,'/'
      ,cast(longitud as decimal(19,3))) as ubicacion
      ,[disaPuntos]
      ,[telefono]
      ,[celular]
      ,[rfc]
      ,[correo]
      ,[empleadoAlta]
  FROM [dbo].[cliente] where estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "nombreRepresentante"
            ds.Tables(0).Columns(4).ColumnName = "direccion"
            ds.Tables(0).Columns(4).ColumnName = "ubicacion"
            ds.Tables(0).Columns(4).ColumnName = "disaPuntos"
            ds.Tables(0).Columns(4).ColumnName = "telefono"
            ds.Tables(0).Columns(4).ColumnName = "celular"
            ds.Tables(0).Columns(4).ColumnName = "rfc"
            ds.Tables(0).Columns(4).ColumnName = "correo"
            ds.Tables(0).Columns(4).ColumnName = "empleadoAlta"



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultarClientesLista(codigoCliente As String) As List(Of String)
        Dim informacionCliente As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()

        Dim query As String = ("
SELECT a.nombre,a.nombreRepresentante,concat(c.nombre,' ',c.apellidos) as nombreTrabajador ,a.direccion,a.telefono,a.celular,a.rfc,a.correo,longitud,latitud
                              FROM [dbo].[cliente] a ,empleados c where a.estatus = 1 and c.clave_trabajador = a.nombreRepresentante and a.id= '" + codigoCliente.Trim + "'")

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionCliente.Add(myReader("nombre").ToString())
                informacionCliente.Add(myReader("nombreRepresentante").ToString())
                informacionCliente.Add(myReader("nombreTrabajador").ToString())

                informacionCliente.Add(myReader("direccion").ToString())
                informacionCliente.Add(myReader("telefono").ToString())
                informacionCliente.Add(myReader("celular").ToString())
                informacionCliente.Add(myReader("rfc").ToString())
                informacionCliente.Add(myReader("correo").ToString())
                informacionCliente.Add(myReader("longitud").ToString())
                informacionCliente.Add(myReader("latitud").ToString())

            End While


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacionCliente
    End Function

    Friend Function EliminarCliente(v As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("UPDATE [dbo].[cliente]
                                   SET [estatus] = 2
                                 WHERE  id = '" + v + "' and estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function



    Friend Function ActualizarInformacion(nombreTienda As String, responsableTienda As String, direccion As String, telefono As String, celular As String, correo As String, longitud As String, latitud As String, rfc As String, id As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("UPDATE [dbo].[cliente]
                                   SET [nombre] = '" + nombreTienda + "'
                                      ,[nombreRepresentante] = '" + responsableTienda + "'
                                      ,[direccion] = '" + direccion + "'
                                      ,[latitud] = '" + latitud + "'
                                      ,[longitud] = '" + longitud + "'
                                      ,[telefono] = '" + telefono + "'
                                      ,[celular] = '" + celular + "'
                                      ,[rfc] = '" + rfc + "'
                                      ,[correo] = '" + correo + "'
                                 WHERE  id = '" + id + "' and estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function ConsultarClientes(cliente As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
      ,[nombre]
      ,[nombreRepresentante]
      ,[localidad]
      ,[direccion]
      ,concat(cast(latitud as decimal(19,3)) ,'/'
      ,cast(longitud as decimal(19,3))) as ubicacion
      ,[disaPuntos]
      ,[telefono]
      ,[celular]
      ,[rfc]
      ,[correo]
      ,[empleadoAlta]
  FROM [dbo].[cliente] where estatus = 1 and id= '" + cliente.Trim + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "nombreRepresentante"
            ds.Tables(0).Columns(4).ColumnName = "direccion"
            ds.Tables(0).Columns(4).ColumnName = "ubicacion"
            ds.Tables(0).Columns(4).ColumnName = "disaPuntos"
            ds.Tables(0).Columns(4).ColumnName = "telefono"
            ds.Tables(0).Columns(4).ColumnName = "celular"
            ds.Tables(0).Columns(4).ColumnName = "rfc"
            ds.Tables(0).Columns(4).ColumnName = "correo"
            ds.Tables(0).Columns(4).ColumnName = "empleadoAlta"



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function
End Class
