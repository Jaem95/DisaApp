﻿Imports System.Data.SqlClient

Public Class PerfilAD

    Public Function actualizarInformacion(telefono As String, direccion As String, correo As String, RFC As String, hobbie As String, descripcion As String, numeroTrabajador As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update empleados  set telefono = '" & telefono & "' ,direccion ='" & direccion & "' ,correo = '" & correo & "',rfc = '" & RFC & "',hobbie = '" & hobbie & "' ,descripcion = '" & descripcion & "' where clave_trabajador = '" & numeroTrabajador & "' and estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Public Function ActualizarURLPerfil(usuario As String, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update empleados set urlfoto = '" & ruta & "' where clave_trabajador =  '" & usuario & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function



End Class
