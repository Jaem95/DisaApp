﻿Imports Microsoft.VisualBasic
Imports DISA
Imports System.Data
Imports System.Data.SqlClient

Public Class InventarioAD
    Public Function getProductsInStock(region As String, condition As String) As DataSet
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa,i.idProductoCB as codigoBarras,p.nombre,i.caducidad,i.cantidad
                                from R" & region & "Inventario as i, productos as p
                                where (i.idProductoCB=p.codigo_barrras) 
                                and (i.status=1) and (p.estatus=1)
                                order by " & condition)
        Dim connection As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connection

        Try
            connection.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "codigoBarras"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "caducidad"
            ds.Tables(0).Columns(4).ColumnName = "cantidad"

        Catch ex As Exception
            Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connection.Close()
        End Try

        Return ds
    End Function

    Public Function getSpecificProductInStock(region As String, codigoDisa As String) As DataSet
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.codigoDisa,i.idProductoCB as codigoBarras,p.nombre,i.caducidad,i.cantidad
                                from R" & region & "Inventario as i, productos as p
                                where (p.codigoDisa='" & codigoDisa & "')
                                and (i.idProductoCB=p.codigo_barrras) 
                                and (i.status=1) and (p.estatus=1)")
        Dim connection As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connection

        Try
            connection.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "codigoBarras"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "caducidad"
            ds.Tables(0).Columns(4).ColumnName = "cantidad"

        Catch ex As Exception
            Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connection.Close()
        End Try

        Return ds
    End Function
End Class
