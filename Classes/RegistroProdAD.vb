﻿Imports System.Data.SqlClient

Public Class RegistroProdAD
    Public Function RegistrarProducto(nombre As String, descripcion As String, presentacion As String, cantidadxcaja As String, codigo_barras_pieza As String, codigo_barras_caja As String, codigoDisa As String, proovedor As String, clasificacion As String, tipoPieza As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "DECLARE @ID_AUXILIAR INT
                                IF(SELECT COUNT(*) FROM  PRODUCTOS )= 0
	                                BEGIN
		                                SET @ID_AUXILIAR = 1
	                                END
                                ELSE
	                                BEGIN
		                                DECLARE @ID_MAX INT
		                                SET @ID_MAX=(SELECT MAX(id)
						                                FROM PRODUCTOS  )
		                                SET @ID_AUXILIAR = @ID_MAX + 1
		                                insert into  productos values(@ID_AUXILIAR, '" & nombre & "','" & descripcion & "','" & codigoDisa & "'," & clasificacion & ",'','" & codigo_barras_pieza & "','" & presentacion & "','" & cantidadxcaja & "',1,'" & codigo_barras_caja & "', " & proovedor & ", " & tipoPieza & ",'')
                                END
                                "
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion

    End Function


    Public Function SubirFoto(producto As String, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update productos set urlfoto = '" & ruta & "' where codigoDisa =  '" & producto & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

End Class
