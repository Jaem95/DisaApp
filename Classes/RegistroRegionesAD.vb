﻿Imports System.Data.SqlClient


Public Class RegistroRegionesAD
    Friend Function RegistrarRegion(txtRFC As String, txtNomrbreRegion As String, txtDireccion As String, txtColonia As String, txtMunicipio As String, txtEstado As String, txtCodigoPostal As String, txtNumeroTelefonico As String, txtCodigoGerente As String, txtNombreDelGerente As String, txtSuperficeBodega As String, txtCapacidadCajas As String) As Integer
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim idGenerado As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()

        Dim query As String = "DECLARE @ID_AUXILIAR INT
                                IF(SELECT COUNT(*) FROM  region )= 0
	                                BEGIN
		                                SET @ID_AUXILIAR = 1
	                                END
                                ELSE
	                                BEGIN
		                                DECLARE @ID_MAX INT
		                                SET @ID_MAX=(SELECT MAX(id)
						                                FROM region  where id < 1000001 )
		                                SET @ID_AUXILIAR = @ID_MAX + 1
		                                insert into  region values(@ID_AUXILIAR, '" & txtNomrbreRegion & "','" & txtEstado & "',1,'" & txtCodigoGerente & "','" & txtNombreDelGerente & "','" & txtRFC & "','" & txtDireccion & "','" & txtColonia & "','" & txtMunicipio & "', '" & txtCodigoPostal & "', '" & txtSuperficeBodega & "','" & txtNumeroTelefonico & "','" & txtCapacidadCajas & "','');
                                        select @ID_AUXILIAR;
                                END
                                "


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim reader As SqlDataReader = myCommand.ExecuteReader()
            If reader.HasRows Then
                Do While reader.Read()
                    idGenerado = reader.GetInt32(0)
                Loop
            Else
                Console.WriteLine("No rows found.")
            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return idGenerado

    End Function

    Public Function SubirFoto(id As Integer, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update region set urlfoto = '" & ruta & "' where id =  " & id & " and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function


    Public Function CrearTablas(id As String) As Boolean
        Dim validacion As Boolean = False

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        ' Dim query As String = ("create table R" & id & "inventario(
        '                            idProductoCB bigint,
        '                           cantidad int,
        '                          caducidad nvarchar(50),
        '                         cantidadLimite int,
        '                        cantidadMinima int,
        '                       status int,
        '
        '                           CONSTRAINT PK_inventario" & id & "_idProductoCB PRIMARY KEY (idProductoCB),
        '                          CONSTRAINT FK_inventario" & id & "_idProductoCB FOREIGN KEY (idProductoCB) REFERENCES productos(codigo_barrras)
        '                     )
        '
        '                       create table R" & id & "precio(
        '                          id int IDENTITY(1,1),
        '                         idProductoCB bigint,
        '                        precio int,
        '                       rango1 int,
        '                      rango2 int,
        '                     status int,
        '
        '                           CONSTRAINT PK_precio" & id & "_id PRIMARY KEY (id),
        '                          CONSTRAINT FK_precio" & id & "_idProductoCB FOREIGN KEY (idProductoCB) REFERENCES R" & id & "inventario(idProductoCB)
        '                     )
        '
        '                       create table R" & id & "pedidos(
        '                          id int IDENTITY(1,1),
        '                         fecha nvarchar(12),
        '                        cantidadTotal bigint,
        '                       cliente int,
        '                      vendedor nvarchar(50),
        '                     status int,
        '
        '                           CONSTRAINT PK_pedidos" & id & "_id PRIMARY KEY (id),
        '                          CONSTRAINT FK_pedidos" & id & "_vendedor FOREIGN KEY (vendedor) REFERENCES empleadosactivos(clave_trabajador),
        '                         CONSTRAINT FK_pedidos" & id & "_cliente FOREIGN KEY (cliente) REFERENCES cliente(id)
        '                    )
        '
        '                       create table R" & id & "ProductosPedido(
        '                          id int IDENTITY(1,1),
        '                         idPedido int,
        '                        idProductoCB bigint,
        '                       status int,
        '                      cantidadProductos int,
        '
        '                           CONSTRAINT PK_ProductosPedido" & id & "_id PRIMARY KEY (id),
        '                          CONSTRAINT FK_ProductosPedido" & id & "_idPedido FOREIGN KEY (idPedido) REFERENCES R" & id & "pedidos(id),
        '                         CONSTRAINT FK_ProductosPedido" & id & "_idProductoCB FOREIGN KEY (idProductoCB) REFERENCES productos(codigo_barrras)
        '                    )	
        '
        '                       create table R" & id & "ventas(
        '                          id int IDENTITY(1,1),
        '                         precio int,
        '                        cliente int,
        '                       vendedor nvarchar(50),
        '                      status int,
        '
        '                           CONSTRAINT PK_ventas" & id & "_id PRIMARY KEY (id),
        '                          CONSTRAINT FK_ventas" & id & "_vendedor FOREIGN KEY (vendedor) REFERENCES empleadosactivos(clave_trabajador),
        '                         CONSTRAINT FK_ventas" & id & "_cliente FOREIGN KEY (cliente) REFERENCES cliente(id)
        '                    )
        '
        '                       create table R" & id & "ProductosVenta(
        '                          id int IDENTITY(1,1),
        '                         idPedido int,
        '                        idProductoCB bigint,
        '                       status int,
        '
        '                           CONSTRAINT PK_ProductosVenta" & id & "_id PRIMARY KEY (id),
        '                          CONSTRAINT FK_ProductosVenta" & id & "_idPedido FOREIGN KEY (idPedido) REFERENCES R" & id & "pedidos(id),
        '                         CONSTRAINT FK_ProductosVenta" & id & "_idProductoCB FOREIGN KEY (idProductoCB) REFERENCES productos(codigo_barrras)
        '                    )	
        '                   create table R" & id & "Devoluciones
        '                  (
        '                  idDevolucion int IDENTITY(1,1),
        '                 idPedido int ,
        '                idProductoCB bigint,
        '               estatus int,
        '
        '                        CONSTRAINT PK_R" & id & "Devoluciones_id PRIMARY KEY (idDevolucion),
        '                       CONSTRAINT FK_R" & id & "Devoluciones_idPedido FOREIGN KEY (idPedido) REFERENCES R" & id & "pedidos(id),
        '                      CONSTRAINT FK_R" & id & "Devoluciones_idProductoCB FOREIGN KEY (idProductoCB) REFERENCES productos(codigo_barrras)
        '
        '                       )
        '
        '                       create table R" + id + "cajaMagica(
        '                                                      id int IDENTITY(1,1),
        '                                                     idPedido int,
        '                                                    idCajaMagica varchar(6),
        '                                                   codigoDisa varchar(15),
        '                                                  precioTotal float,
        '                                                 numProdTotal int,
        '                                                status int,
        '
        '                                                       CONSTRAINT PK_cajaMagicaR" + id + "_idCajaMagica PRIMARY KEY (idCajaMagica),
        '                                                      CONSTRAINT FK_cajaMagicaR" + id + "_idPedido FOREIGN KEY (idPedido) REFERENCES R" + id + "pedidos(id)
        '                                                 )
        '                                                
        '                       ALTER TABLE R" + id + "ProductosPedido  ADD idCajaMagica varchar(6) FOREIGN KEY REFERENCES R" + id + "cajaMagica(idCajaMagica)
        '")

        Dim query As String = "
CREATE TABLE [dbo].[R" + id + "cajaMagica](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPedido] [int] NULL,
	[idCajaMagica] [varchar](6) NOT NULL,
	[codigoDisa] [varchar](15) NULL,
	[precioTotal] [float] NULL,
	[numProdTotal] [int] NULL,
	[status] [int] NULL,
	[cantidadCaja] [int] NULL,
	[r1] [int] NULL,
	[r2] [int] NULL,
	[p1] [float] NULL,
	[p2] [float] NULL,
	[pCaja] [float] NULL,
 CONSTRAINT [PK_cajaMagicaR" + id + "_idCajaMagica] PRIMARY KEY CLUSTERED 
(
	[idCajaMagica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

CREATE TABLE [dbo].[R" + id + "Devoluciones](
	[idDevolucion] [int] IDENTITY(1,1) NOT NULL,
	[idPedido] [int] NULL,
	[idProductoCB] [bigint] NULL,
	[estatus] [int] NULL,
	[cantidadProductos] [bigint] NULL,
	[idCajaMagica] [varchar](6) NULL,
	[tipoDevolucion] [varchar](15) NULL,
 CONSTRAINT [PK_R" + id + "Devoluciones_id] PRIMARY KEY CLUSTERED 
(
	[idDevolucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[R" + id + "inventario](
	[idProductoCB] [bigint] NOT NULL,
	[cantidad] [int] NULL,
	[caducidad] [nvarchar](50) NULL,
	[cantidadLimite] [int] NULL,
	[cantidadMinima] [int] NULL,
	[status] [int] NULL,
	[categoria] [int] NULL,
 CONSTRAINT [PK_inventario" + id + "_idProductoCB] PRIMARY KEY CLUSTERED 
(
	[idProductoCB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[R" + id + "pedidos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[fecha] [nvarchar](12) NULL,
	[cantidadTotal] [float] NULL,
	[cliente] [int] NULL,
	[vendedor] [nvarchar](50) NULL,
	[status] [int] NULL,
	[tiempo] [float] NULL,
 CONSTRAINT [PK_pedidos" + id + "_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[R" + id + "precio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idProductoCB] [bigint] NULL,
	[precio] [float] NULL,
	[rango1] [int] NULL,
	[rango2] [int] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_precio" + id + "_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[R" + id + "ProductosPedido](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPedido] [int] NULL,
	[idProductoCB] [bigint] NULL,
	[status] [int] NULL,
	[cantidadProductos] [int] NULL,
	[precioUnitario] [float] NULL,
	[idCajaMagica] [varchar](6) NULL,
	[promo] [bigint] NULL,
	[tipoPromo] [bigint] NULL,
 CONSTRAINT [PK_ProductosPedido" + id + "_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[R" + id + "ProductosVenta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idPedido] [int] NULL,
	[idProductoCB] [bigint] NULL,
	[status] [int] NULL,
	[canProduct] [int] NULL,
	[fecha] [nvarchar](max) NULL,
	[monto_devolucion] [float] NULL,
	[monto_cambios] [int] NULL,
	[monto_cobrado] [float] NULL,
	[idCajaMagica] [varchar](6) NULL,
 CONSTRAINT [PK_ProductosVenta" + id + "_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
 )
CREATE TABLE [dbo].[R" + id + "ventas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[precio] [float] NULL,
	[cliente] [int] NULL,
	[vendedor] [nvarchar](50) NULL,
	[status] [int] NULL,
	[fecha] [nvarchar](max) NULL,
	[idPedido] [int] NULL,
	[tiempo] [float] NULL,
 CONSTRAINT [PK_ventas" + id + "_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


ALTER TABLE [dbo].[R" + id + "cajaMagica]  WITH CHECK ADD  CONSTRAINT [FK_cajaMagicaR" + id + "_idPedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[R" + id + "pedidos] ([id])

ALTER TABLE [dbo].[R" + id + "cajaMagica] CHECK CONSTRAINT [FK_cajaMagicaR" + id + "_idPedido]

ALTER TABLE [dbo].[R" + id + "Devoluciones]  WITH CHECK ADD FOREIGN KEY([idCajaMagica])
REFERENCES [dbo].[R" + id + "cajaMagica] ([idCajaMagica])

ALTER TABLE [dbo].[R" + id + "Devoluciones]  WITH CHECK ADD  CONSTRAINT [FK_R" + id + "Devoluciones_idPedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[R" + id + "pedidos] ([id])

ALTER TABLE [dbo].[R" + id + "Devoluciones] CHECK CONSTRAINT [FK_R" + id + "Devoluciones_idPedido]

ALTER TABLE [dbo].[R" + id + "Devoluciones]  WITH CHECK ADD  CONSTRAINT [FK_R" + id + "Devoluciones_idProductoCB] FOREIGN KEY([idProductoCB])
REFERENCES [dbo].[productos] ([codigo_barrras])

ALTER TABLE [dbo].[R" + id + "Devoluciones] CHECK CONSTRAINT [FK_R" + id + "Devoluciones_idProductoCB]

ALTER TABLE [dbo].[R" + id + "inventario]  WITH CHECK ADD  CONSTRAINT [FK_inventario" + id + "_idProductoCB] FOREIGN KEY([idProductoCB])
REFERENCES [dbo].[productos] ([codigo_barrras])

ALTER TABLE [dbo].[R" + id + "inventario] CHECK CONSTRAINT [FK_inventario" + id + "_idProductoCB]

ALTER TABLE [dbo].[R" + id + "inventario]  WITH CHECK ADD  CONSTRAINT [FK_R" + id + "inventario_categoria] FOREIGN KEY([categoria])
REFERENCES [dbo].[categoriasProductos] ([id])

ALTER TABLE [dbo].[R" + id + "inventario] CHECK CONSTRAINT [FK_R" + id + "inventario_categoria]

ALTER TABLE [dbo].[R" + id + "pedidos]  WITH CHECK ADD  CONSTRAINT [FK_pedidos" + id + "_cliente] FOREIGN KEY([cliente])
REFERENCES [dbo].[cliente] ([id])

ALTER TABLE [dbo].[R" + id + "pedidos] CHECK CONSTRAINT [FK_pedidos" + id + "_cliente]

ALTER TABLE [dbo].[R" + id + "pedidos]  WITH CHECK ADD  CONSTRAINT [FK_pedidos" + id + "_vendedor] FOREIGN KEY([vendedor])
REFERENCES [dbo].[empleadosactivos] ([clave_trabajador])

ALTER TABLE [dbo].[R" + id + "pedidos] CHECK CONSTRAINT [FK_pedidos" + id + "_vendedor]

ALTER TABLE [dbo].[R" + id + "precio]  WITH CHECK ADD  CONSTRAINT [FK_precio" + id + "_idProductoCB] FOREIGN KEY([idProductoCB])
REFERENCES [dbo].[R" + id + "inventario] ([idProductoCB])

ALTER TABLE [dbo].[R" + id + "precio] CHECK CONSTRAINT [FK_precio" + id + "_idProductoCB]

ALTER TABLE [dbo].[R" + id + "ProductosPedido]  WITH CHECK ADD FOREIGN KEY([idCajaMagica])
REFERENCES [dbo].[R" + id + "cajaMagica] ([idCajaMagica])

ALTER TABLE [dbo].[R" + id + "ProductosPedido]  WITH CHECK ADD  CONSTRAINT [FK_ProductosPedido" + id + "_idPedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[R" + id + "pedidos] ([id])

ALTER TABLE [dbo].[R" + id + "ProductosPedido] CHECK CONSTRAINT [FK_ProductosPedido" + id + "_idPedido]

ALTER TABLE [dbo].[R" + id + "ProductosPedido]  WITH CHECK ADD  CONSTRAINT [FK_ProductosPedido" + id + "_idProductoCB] FOREIGN KEY([idProductoCB])
REFERENCES [dbo].[productos] ([codigo_barrras])

ALTER TABLE [dbo].[R" + id + "ProductosPedido] CHECK CONSTRAINT [FK_ProductosPedido" + id + "_idProductoCB]

ALTER TABLE [dbo].[R" + id + "ProductosVenta]  WITH CHECK ADD FOREIGN KEY([idCajaMagica])
REFERENCES [dbo].[R" + id + "cajaMagica] ([idCajaMagica])

ALTER TABLE [dbo].[R" + id + "ProductosVenta]  WITH CHECK ADD  CONSTRAINT [FK_ProductosVenta" + id + "_idPedido] FOREIGN KEY([idPedido])
REFERENCES [dbo].[R" + id + "pedidos] ([id])

ALTER TABLE [dbo].[R" + id + "ProductosVenta] CHECK CONSTRAINT [FK_ProductosVenta" + id + "_idPedido]

ALTER TABLE [dbo].[R" + id + "ProductosVenta]  WITH CHECK ADD  CONSTRAINT [FK_ProductosVenta" + id + "_idProductoCB] FOREIGN KEY([idProductoCB])
REFERENCES [dbo].[productos] ([codigo_barrras])

ALTER TABLE [dbo].[R" + id + "ProductosVenta] CHECK CONSTRAINT [FK_ProductosVenta" + id + "_idProductoCB]

ALTER TABLE [dbo].[R" + id + "ventas]  WITH CHECK ADD FOREIGN KEY([idPedido])
REFERENCES [dbo].[R" + id + "pedidos] ([id])

ALTER TABLE [dbo].[R" + id + "ventas]  WITH CHECK ADD  CONSTRAINT [FK_ventas" + id + "_cliente] FOREIGN KEY([cliente])
REFERENCES [dbo].[cliente] ([id])

ALTER TABLE [dbo].[R" + id + "ventas] CHECK CONSTRAINT [FK_ventas" + id + "_cliente]

ALTER TABLE [dbo].[R" + id + "ventas]  WITH CHECK ADD  CONSTRAINT [FK_ventas" + id + "_vendedor] FOREIGN KEY([vendedor])
REFERENCES [dbo].[empleadosactivos] ([clave_trabajador])

ALTER TABLE [dbo].[R" + id + "ventas] CHECK CONSTRAINT [FK_ventas" + id + "_vendedor]

"
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.ExecuteNonQuery()
            validacion = True

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion


        Return validacion
    End Function

End Class
