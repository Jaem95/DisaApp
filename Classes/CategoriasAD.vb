﻿Imports System.Data.SqlClient
Public Class CategoriasAD

    Public Function ConsultarCategorias() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select cp.id,cp.nombre as categoria,cp.iniciales as letrasIniciales,(select count (*) from productos p where p.categoria = cp.id) as cantidadProductos,cp.descripcion  from categoriasProductos cp ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")

            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(0).ColumnName = "categoria"
            ds.Tables(0).Columns(1).ColumnName = "letrasIniciales"
            ds.Tables(0).Columns(2).ColumnName = "cantidadProductos"
            ds.Tables(0).Columns(3).ColumnName = "descripcion"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function


    Public Function ConsultarInformacion(categoria As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select  nombre,iniciales,descripcion,foto from categoriasProductos where id  = '" & categoria & "' and  estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacion.Add(myReader("nombre").ToString())
                informacion.Add(myReader("iniciales").ToString())
                informacion.Add(myReader("descripcion").ToString())
                informacion.Add(myReader("foto").ToString())
            End While


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Public Function ActualizarInfo(nombre As String, iniciales As String, desc As String, id As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update categoriasProductos  set nombre = '" & nombre & "' ,  iniciales = '" & iniciales & "' ,  descripcion = '" & desc & "' where  id = '" & id & "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function



    Public Function ActualizarURLCategoria(id As String, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update categoriasProductos set foto = '" & ruta & "' where id =  '" & id & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function



    Public Function RegistrarCategoria(nombre As String, iniciales As String, desc As String) As Integer
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim idGenerado As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()

        Dim query As String = "DECLARE @ID_AUXILIAR INT
                                IF(SELECT COUNT(*) FROM  categoriasProductos )= 0
	                                BEGIN
		                                SET @ID_AUXILIAR = 1
	                                END
                                ELSE
	                                BEGIN
		                                DECLARE @ID_MAX INT
		                                SET @ID_MAX=(SELECT MAX(id)
						                                FROM categoriasProductos  )
		                                SET @ID_AUXILIAR = @ID_MAX + 1
		                                insert into  categoriasProductos values(@ID_AUXILIAR, '" & nombre & "','" & iniciales & "','" & desc & "','',1);
                                        select @ID_AUXILIAR;
                                END
                                "


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim reader As SqlDataReader = myCommand.ExecuteReader()
            If reader.HasRows Then
                Do While reader.Read()
                    idGenerado = reader.GetInt32(0)
                Loop
            Else
                Console.WriteLine("No rows found.")
            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return idGenerado
    End Function






End Class
