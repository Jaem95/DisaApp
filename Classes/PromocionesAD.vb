﻿Imports System.Data.SqlClient
Public Class PromocionesAD

    Public Function ConsultarPromocionesPrecios() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select promo.idPromocion, prd.codigoDisa,prd.nombre,prd.codigo_barrras as CB ,
                                (SELECT CASE 
                                            WHEN promoD.localidad = 1000001 
                                               THEN  'General'
                                               ELSE 'Regional' 
                                       END   FROM promocionDescuento promoD where  promo.idPromocion = promoD.idPromocion ) as LA
                                ,promo.fecha_inicio,promo.fecha_fin 
                                from  promocionDescuento promo, productos prd 
                                where promo.idProducto = prd.codigo_barrras  and promo.estatus =1  and prd.estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")

            ds.Tables(0).Columns(0).ColumnName = "idPromocion"
            ds.Tables(0).Columns(1).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "codigo_barrras"
            ds.Tables(0).Columns(4).ColumnName = "LA"
            ds.Tables(0).Columns(5).ColumnName = "fecha_inicio"
            ds.Tables(0).Columns(6).ColumnName = "fecha_fin"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultarPromocionesProductos() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select promo.idPromocion, prd.codigoDisa,prd.nombre,prd.codigo_barrras as CB ,
                                (SELECT CASE 
                                            WHEN promoD.localidad = 1000001 
                                               THEN  'General'
                                               ELSE 'Regional' 
                                       END   FROM promocionRegalo promoD where  promo.idPromocion = promoD.idPromocion ) as LA
                                ,promo.fecha_inicio,promo.fecha_fin 
                                from  promocionRegalo promo, productos prd 
                                where promo.idProducto = prd.codigo_barrras  and promo.estatus =1 and prd.estatus = 1  ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")

            ds.Tables(0).Columns(0).ColumnName = "idPromocion"
            ds.Tables(0).Columns(1).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "codigo_barrras"
            ds.Tables(0).Columns(4).ColumnName = "LA"
            ds.Tables(0).Columns(5).ColumnName = "fecha_inicio"
            ds.Tables(0).Columns(6).ColumnName = "fecha_fin"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds

    End Function

    Friend Function ConsultarProductos() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("

                                  SELECT
                                      [nombre]
                                      ,[codigo_barrras]


                                  FROM [dbo].[productos]
                                  where estatus = 1 


                               ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "Productos")


            ds.Tables(0).Columns(0).ColumnName = "nombre"
            ds.Tables(0).Columns(1).ColumnName = "codigo_barrras"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Public Function ConsultarInformacionPromo(idPromocion As String, TipoPromocion As String) As List(Of String)
        Dim informacion As New List(Of String)
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ""

        If TipoPromocion.Equals("Descuento") Then
            query = "select promo.idPromocion, prd.codigoDisa,prd.nombre,prd.codigo_barrras as CB ,promo.descuento,
                    (SELECT CASE 
                                WHEN promoD.localidad = 1000001 
                                   THEN  'General'
                                   ELSE 'Regional' 
                           END   FROM promocionDescuento promoD where  promo.idPromocion = promoD.idPromocion ) as LA
                    ,promo.fecha_inicio,promo.fecha_fin , re.estado
                    from  promocionDescuento promo, productos prd ,EstadosMexico re
                    where promo.idProducto = prd.codigo_barrras and promo.estatus =1 and promo.idPromocion = " & idPromocion & "  and promo.localidad = re.id_estado"
        ElseIf TipoPromocion.Equals("Producto") Then
            query = "select promo.idPromocion, prd.codigoDisa,prd.nombre,prd.codigo_barrras as CB ,promo.idProductoRegalo,promo.compraMinima,promo.cantidadRegalo,
                    (SELECT CASE 
                                WHEN promoD.localidad = 1000001 
                                   THEN  'General'
                                   ELSE 'Regional' 
                           END   FROM promocionRegalo promoD where  promo.idPromocion = promoD.idPromocion ) as LA
                    ,promo.fecha_inicio,promo.fecha_fin ,promo.localidad
                    from  promocionRegalo promo, productos prd 
					 where promo.idProducto = prd.codigo_barrras and promo.estatus =1  and promo.idPromocion = " & idPromocion & ""


        End If

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio
        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                If TipoPromocion.Equals("Descuento") Then
                    informacion.Add(myReader("LA").ToString())
                    informacion.Add(myReader("fecha_inicio").ToString())
                    informacion.Add(myReader("fecha_fin").ToString())
                    informacion.Add(myReader("descuento").ToString)
                    informacion.Add(myReader("estado").ToString())
                ElseIf TipoPromocion.Equals("Producto") Then
                    informacion.Add(myReader("LA").ToString())
                    informacion.Add(myReader("fecha_inicio").ToString())
                    informacion.Add(myReader("fecha_fin").ToString())
                    informacion.Add(myReader("idProductoRegalo").ToString())
                    informacion.Add(myReader("compraMinima").ToString())
                    informacion.Add(myReader("cantidadRegalo").ToString())
                    informacion.Add(myReader("localidad").ToString())

                End If

            End While


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return informacion
    End Function

    Friend Function ActualizarPromoRegalo(v As String, cbProductoRegalo As String, localidad As String, text As String, compraMinima As String, cantidadRegal As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "UPDATE [dbo].[promocionRegalo]
                                   SET [idProductoRegalo] = '" + cbProductoRegalo + "'
                                      ,[localidad] = '" + localidad + "'
                                      ,[fecha_fin] = '" + text + "'
                                      ,[compraMinima] = '" + compraMinima + "'
                                      ,[cantidadRegalo] ='" + cantidadRegal + "'
                                 WHERE  [idPromocion] = " + v + "
"

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Public Function ActualizarPromo(idPromo As String, codigoBarras As String, nuevaFechaFinal As String, montoPromo As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "  update promocionDescuento  set descuento = '" & montoPromo & "', fecha_fin= '" & nuevaFechaFinal & "' where idProducto = '" & codigoBarras & "' and idPromocion =  " & idPromo & ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Public Function ActualizarPromo(idPromo As String, codigoBarras As String, nuevaFechaFinal As String, montoPromo As String, region As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "  update promocionDescuento  set descuento = '" & montoPromo & "', fecha_fin= '" & nuevaFechaFinal & "' where idProducto = '" & codigoBarras & "' and idPromocion =  " & idPromo & ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function EliminaPromocionProducto(v As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = " UPDATE [dbo].[promocionRegalo]
                                   SET [estatus] = 2 
                                 WHERE  [idPromocion] = '" + v + "' and estatus  =1
                                "


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function CrearPromocion(cbProducto As String, descuento As String, localidad As String, fechaInicialPromo As String, fechaFinPromo As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "INSERT INTO [dbo].[promocionDescuento]
           ([idProducto]
           ,[descuento]
           ,[localidad]
           ,[fecha_inicio]
           ,[fecha_fin]
           ,[estatus])
     VALUES
           ('" + cbProducto + "'
           ,'" + descuento + "'
           ,'" + localidad + "'
           ,'" + fechaInicialPromo + "'
           ,'" + fechaFinPromo + "'
           ,1)
"

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function EliminarPromocionPrecio(v As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = " UPDATE [dbo].[promocionDescuento]
                                   SET [estatus] = 2 
                                 WHERE  [idPromocion]= '" + v + "' and estatus  =1
                                "


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function CrearPromocionRegalo(cbProductoCompra As String, cbProductoRegalo As String, region As String, fechaInicialPromo As String, fechaFinPromo As String, compraMinima As String, cantidadRegalo As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "INSERT INTO [dbo].[promocionRegalo]
                                               ([idProducto]
                                               ,[idProductoRegalo]
                                               ,[localidad]
                                               ,[fecha_inicio]
                                               ,[fecha_fin]
                                               ,[estatus]
                                               ,[cxR1]
                                               ,[cxR2]
                                               ,[cxR3]
                                               ,[compraMinima]
                                               ,[cantidadRegalo])
                                         VALUES
                                               ('" + cbProductoCompra + "'
                                               ,'" + cbProductoRegalo + "'
                                               ,'" + region + "'
                                               ,'" + fechaInicialPromo + "'
                                               ,'" + fechaFinPromo + "'
                                              ,1
                                              ,1
                                              ,1
                                              ,1
                                               ,'" + compraMinima + "'
                                               ,'" + cantidadRegalo + "')
                                    "

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()


        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
End Class
