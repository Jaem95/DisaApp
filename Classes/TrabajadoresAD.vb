﻿Imports System.Data.SqlClient

Public Class TrabajadoresAD
    Friend Function RegistrarTrababajador(nombreEmpleado As String, apelllidoEmpleado As String, fechasNacimiento As String, curp As String, rfc As String, direccion As String, estadoCivil As String, estado As String, ultimosEstudios As String, numeroTelefonico As String, numeroCelular As String, correoElectronico As String, region As String, puesto As String, sexo As String) As String
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim clave As String = ""
        Dim query As String = (" BEGIN 
                                        INSERT INTO [dbo].[empleados]
                                               ([nombre]
                                               ,[apellidos]
                                               ,[fecha_nacimiento]
                                               ,[sexo]
                                               ,[clave_trabajador]
                                               ,[telefono]
                                               ,[direccion]
                                               ,[correo]
                                               ,[estado_civil]
                                               ,[estudios]
                                               ,[rfc]
                                               ,[curp]
                                               ,[hobbie]
                                               ,[descripcion]
                                               ,[foto]
                                               ,[estatus]
                                               ,[urlfoto]
                                                 ,[fecha_contratacion]
                                               ,[estado]
                                               ,[celular])
                                         VALUES
                                               ('" + nombreEmpleado + "'
                                               ,'" + apelllidoEmpleado + "'
                                               ,'" + fechasNacimiento + "'
                                               ,'" + sexo + "'
                                               ,'" + GenerarClave() + "'
                                               ,'" + numeroTelefonico + "'
                                               ,'" + direccion + "'
                                               ,'" + correoElectronico + "'
                                               ,'" + estadoCivil + "'
                                               ,'" + ultimosEstudios + "'
                                               ,'" + rfc + "'
                                               ,'" + curp + "'
                                               ,''
                                               ,''
                                               ,''
                                               ,1
                                               ,''
                                               ,CONVERT(VARCHAR(10),GETDATE(),103)
                                                 ," + estado + "
                                                 ,'" + numeroCelular + "');
                                               
                                               select max (clave_trabajador) from empleados;
                                               
                                               END")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim reader As SqlDataReader = myCommand.ExecuteReader()

            If reader.HasRows Then
                Do While reader.Read()
                    clave = reader.GetString(0)
                Loop
            Else
                Console.WriteLine("No rows found.")
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return clave
    End Function

    Friend Function ConsultarTrabajadorEspecifico2(codigoTrabajador As String) As List(Of String)
        Dim informacionEmpleado As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT nombre
                                      ,apellidos
                                      ,fecha_nacimiento
                                      ,sexo
                                      ,telefono
                                      ,direccion
                                      ,correo
                                      ,estado_civil
                                      ,estudios
                                      ,rfc
                                      ,curp
                                      ,estado
                                      ,celular
									  ,act.region
									  ,act.puesto
                              FROM [dbo].[empleados],empleadosactivos as act where act.clave_trabajador = empleados.clave_trabajador and  empleados.clave_trabajador = '" + codigoTrabajador + "' and empleados.estatus =1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionEmpleado.Add(myReader("nombre").ToString())
                informacionEmpleado.Add(myReader("apellidos").ToString())
                informacionEmpleado.Add(myReader("fecha_nacimiento").ToString())
                informacionEmpleado.Add(myReader("sexo").ToString())
                informacionEmpleado.Add(myReader("telefono").ToString())
                informacionEmpleado.Add(myReader("direccion").ToString())
                informacionEmpleado.Add(myReader("estado_civil").ToString())
                informacionEmpleado.Add(myReader("estudios").ToString())
                informacionEmpleado.Add(myReader("curp").ToString())
                informacionEmpleado.Add(myReader("estado").ToString())
                informacionEmpleado.Add(myReader("rfc").ToString())
                informacionEmpleado.Add(myReader("celular").ToString())
                informacionEmpleado.Add(myReader("correo").ToString())
                informacionEmpleado.Add(myReader("region").ToString())
                informacionEmpleado.Add(myReader("puesto").ToString())



            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionEmpleado
    End Function

    Friend Function ModificarEmpleadoActivo(claveEmpleado As String, selectedItem As String, selectedValue As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim clave As String = ""
        Dim query As String = (" UPDATE [dbo].[empleadosactivos]
                                   SET [puesto] = '" & selectedItem.ToUpper & "'
                                      ,[region] = '" & selectedValue & "'
                             WHERE [clave_trabajador] = '" + claveEmpleado + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function EliminarTrabajador(codigoTrabajador As String, razon As String, usuario As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim clave As String = ""
        Dim query As String = ("INSERT INTO [dbo].[renuncias]
                               ([id_trabajador]
                               ,[razon]
                               ,[fecha]
                               ,[usuario_dio_baja]
                               ,[estatus])
                         VALUES
                               ('" + codigoTrabajador + "','" + razon + "',CONVERT(VARCHAR(10),GETDATE(),103),'" + usuario + "',1);
                                update    empleadosactivos set estatus = 2 where  clave_trabajador = '" + codigoTrabajador + "';
                                update    empleados        set estatus = 2 where  clave_trabajador =  '" + codigoTrabajador + "' and estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function ModificarEmpleado(claveEmpleado As String, nombreEmpleado As String, apelllidoEmpleado As String, fechasNacimiento As String, curp As String, rfc As String, direccion As String, estadoCivil As String, estado As String, ultimosEstudios As String, numeroTelefonico As String, numeroCelular As String, correoElectronico As String, region As String, puesto As String, sexo As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim clave As String = ""
        Dim query As String = (" UPDATE [dbo].[empleados]
                               SET [nombre] = '" + nombreEmpleado + "'
                                  ,[apellidos] =  '" + apelllidoEmpleado + "'
                                  ,[fecha_nacimiento] =  '" + fechasNacimiento + "'
                                  ,[sexo] =  '" + sexo + "'
                                  ,[telefono] = '" + numeroTelefonico + "'
                                  ,[direccion] =  '" + direccion + "'
                                  ,[correo] =  '" + correoElectronico + "'
                                  ,[estado_civil] =  '" + estadoCivil + "'
                                  ,[estudios] =  '" + ultimosEstudios + "'
                                  ,[rfc] =  '" + rfc + "'
                                  ,[curp] =  '" + curp + "'
                                
                                 
                                 
                                 
                                 
                                 
                                  ,[celular] =  '" + numeroCelular + "'
                                  ,[estado] =  '" + estado + "'
                             WHERE [clave_trabajador] = '" + claveEmpleado + "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Friend Function ConsultarTrabajadorEspecifico(codigoTrabajador As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("
  select emp1.clave_trabajador,emp2.pwd,Concat(emp1.nombre,'',emp1.apellidos) as nombre,emp2.puesto,emp1.fecha_contratacion from empleados  as emp1 , empleadosactivos as emp2 where emp1.clave_trabajador = emp2.clave_trabajador   and emp1.clave_trabajador = '" + codigoTrabajador + "'  and emp1.estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "pwd"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "puesto"
            ds.Tables(0).Columns(4).ColumnName = "fecha_contratacion"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultarEmpleados() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select emp1.clave_trabajador,emp2.pwd,Concat(emp1.nombre,' ',emp1.apellidos) as nombre,pues.tipo as puesto,emp1.fecha_contratacion from empleados  as emp1 , empleadosactivos as emp2,puesto as pues where emp1.clave_trabajador = emp2.clave_trabajador  and emp2.puesto = pues.id and emp1.estatus = 1
    ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "clave_trabajador"
            ds.Tables(0).Columns(1).ColumnName = "pwd"
            ds.Tables(0).Columns(2).ColumnName = "nombre"
            ds.Tables(0).Columns(3).ColumnName = "puesto"
            ds.Tables(0).Columns(4).ColumnName = "fecha_contratacion"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Private Function GenerarClave() As String
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim clave_trajador As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select max(clave_trabajador)  from empleados")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        Dim clave As String = ""
        myCommand.Connection = connexio


        Try
            connexio.Open()
            Dim reader As SqlDataReader = myCommand.ExecuteReader()

            If reader.HasRows Then
                Do While reader.Read()
                    clave = reader.GetString(0)
                Loop
            Else
                Console.WriteLine("No rows found.")
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return IDGenerado(clave)
    End Function

    Friend Function ActivarClientes(idGenerado As String, pwd As String, toUpper As String, region As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("INSERT INTO [dbo].[empleadosactivos]
           ([clave_trabajador]
           ,[pwd]
           ,[puesto]
           ,[region]
           ,[estatus])
     VALUES
           ('" + idGenerado + "'
           ,'" + pwd + "'
           ,'" + toUpper + "'
           ,'" + region + "'
           ,1)")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Private Function IDGenerado(clave As String) As String
        Dim numero As Integer = clave.Remove(0, 1)
        numero = numero + 1

        Dim longitudConsecutivo As Integer = 1
        Dim longitudNumero As Integer = numero.ToString.Length()
        Dim numeroCeros As Integer = clave.Length - longitudConsecutivo - longitudNumero

        Dim decimalLength As Integer = numero.ToString("D").Length + numeroCeros


        Return "A" & numero.ToString("D" + decimalLength.ToString())


    End Function

    Friend Function SubirFoto(idGenerado As String, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update empleados set urlfoto = '" & ruta & "' where clave_trabajador =  '" & idGenerado & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
End Class
