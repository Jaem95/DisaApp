﻿Imports Microsoft.VisualBasic
Imports DISA
Imports System.Data
Imports System.Data.SqlClient

Public Class DevolucionesAD
    Public Function getDevoluciones(region As String, condition As String) As DataSet
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select *
                                from R" & region & "Devoluciones
                                where estatus!=5
                                order by " & condition)
        Dim connection As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connection

        Try
            connection.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "idDevolucion"
            ds.Tables(0).Columns(1).ColumnName = "idPedido"
            ds.Tables(0).Columns(2).ColumnName = "idProductoCB"
            ds.Tables(0).Columns(3).ColumnName = "cantidadProductos"

        Catch ex As Exception
            Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connection.Close()
        End Try

        Return ds
    End Function
End Class
