﻿
Imports System.Data
Imports System.Data.SqlClient

Public Class PedidosPendientesAD

    Public Function obtenerRepartidores(region As String, condition As String) As DataSet
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select r.empleado as codigoDisa, e.nombre as nombre, r.nombre as ruta, nest1.countP as pending, nest2.countD as delivered
                                from empleados as e, ruta as r, (select r.empleado as emp, Count(r.empleado) as countP
                                from empleados as e, ruta as r, 
                                (select r.empleado as ed, p.id, p.status, rpc.idCliente
                                from ruta as r, R" & region & "Pedidos as p, rutaPorCliente as rpc
                                where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta)
                                and(r.idLocalidad=" & region & ") and (p.cliente=rpc.idCliente) and (p.status=1)) as nest
                                where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" & region & ") and (r.tipoRuta='repartidor')
                                and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado)
                                group by r.empleado) as nest1, (select r.empleado as emp, Count(r.empleado) as countD
                                from empleados as e, ruta as r, 
                                (select r.empleado as ed, p.id, p.status, rpc.idCliente
                                from ruta as r, R" & region & "Pedidos as p, rutaPorCliente as rpc
                                where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta)
                                and(r.idLocalidad=" & region & ") and (p.cliente=rpc.idCliente) and (p.status=2)) as nest
                                where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" & region & ") and (r.tipoRuta='repartidor')
                                and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado)
                                group by r.empleado) as nest2
                                where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" & region & ") and (r.tipoRuta='repartidor')
                                and (r.empleado=e.clave_trabajador) and (nest1.emp=r.empleado) and (nest2.emp=r.empleado)
                                order by " & condition & "")
        Dim connection As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connection

        Try
            connection.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "ruta"
            ds.Tables(0).Columns(3).ColumnName = "pending"
            ds.Tables(0).Columns(4).ColumnName = "delivered"

        Catch ex As Exception
            Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connection.Close()
        End Try

        Return ds
    End Function

    Public Function ConsultarProductoEspecifico(region As String, codigoDisa As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select r.empleado as codigoDisa, e.nombre as nombre, r.nombre as ruta, nest1.countP as pending, nest2.countD as delivered
                                from empleados as e, ruta as r, (select r.empleado as emp, Count(r.empleado) as countP
                                from empleados as e, ruta as r, 
                                (select r.empleado as ed, p.id, p.status, rpc.idCliente
                                from ruta as r, R" & region & "Pedidos as p, rutaPorCliente as rpc
                                where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta)
                                and(r.idLocalidad=" & region & ") and (p.cliente=rpc.idCliente) and (p.status=1)) as nest
                                where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" & region & ") and (r.tipoRuta='repartidor')
                                and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado)
                                group by r.empleado) as nest1, (select r.empleado as emp, Count(r.empleado) as countD
                                from empleados as e, ruta as r, 
                                (select r.empleado as ed, p.id, p.status, rpc.idCliente
                                from ruta as r, R" & region & "Pedidos as p, rutaPorCliente as rpc
                                where (r.tipoRuta='repartidor') and (r.id=rpc.idRuta)
                                and(r.idLocalidad=" & region & ") and (p.cliente=rpc.idCliente) and (p.status=2)) as nest
                                where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" & region & ") and (r.tipoRuta='repartidor')
                                and (r.empleado=e.clave_trabajador) and (nest.ed=r.empleado)
                                group by r.empleado) as nest2
                                where (e.estatus=1 and r.estatus=1) and (r.idLocalidad=" & region & ") and (r.tipoRuta='repartidor')
                                and (r.empleado=e.clave_trabajador) and (nest1.emp=r.empleado) and (nest2.emp=r.empleado)
                                and (r.empleado='" & codigoDisa & "')")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "codigoDisa"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "ruta"
            ds.Tables(0).Columns(3).ColumnName = "pending"
            ds.Tables(0).Columns(4).ColumnName = "delivered"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Public Function getPendingRequests(region As String) As String
        Dim informacionUsuario As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select Count(*) as countPendientes from R" & region & "Pedidos where status=1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionUsuario = myReader("countPendientes").ToString()
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionUsuario
    End Function

    Public Function getDeliveredRequesst(region As String) As String
        Dim informacionUsuario As String = ""
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select Count(*) as countPendientes from R" & region & "Pedidos where status=2")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionUsuario = myReader("countPendientes").ToString()
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionUsuario
    End Function

    Public Function getRequestsRepartidor(repartidor As String, region As String) As List(Of List(Of String))
        Dim repartidorRequests As New List(Of List(Of String))
        Dim infoRepartidor As List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.id, r.empleado, e.nombre as nombre, e.apellidos, r.nombre as ruta
                                from R" & region & "Pedidos as p, ruta as r, rutaPorCliente as rpc, empleados e
                                where (r.empleado='" & repartidor & "') and (rpc.idRuta=r.id)
                                and (rpc.idCliente=p.cliente) and (r.empleado=e.clave_trabajador)
                                and (e.estatus=1 and r.estatus=1 and rpc.estatus=1 and p.status=1)")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                infoRepartidor = New List(Of String)
                infoRepartidor.Add(myReader("empleado").ToString)
                infoRepartidor.Add(myReader("id").ToString)
                infoRepartidor.Add(myReader("ruta").ToString)
                infoRepartidor.Add(myReader("nombre") + " " + myReader("apellidos"))
                repartidorRequests.Add(infoRepartidor)
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return repartidorRequests
    End Function

    Public Function getRequestProducts(requestCode As String) As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select p.nombre as productos
                                from productos as p, R3Pedidos as ped, R3ProductosPedido as prod
                                where (p.estatus=1) and (prod.idProductoCB=p.codigo_barrras)
                                and (prod.status=1) and (ped.id=prod.idPedido)
                                and (ped.id=" & requestCode & ")")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "productos"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Public Function checkForManager(usuario As String, pwd As String) As Boolean
        Dim validacion As New List(Of String)
        Dim userTemp As String = ""
        Dim pwdTemp As String = ""
        Dim puesto As String = ""
        Dim region As String = ""


        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select clave_trabajador,pwd,puesto,region 
                                from empleadosactivos 
                                where clave_trabajador ='" & usuario & "' 
                                and pwd = '" & pwd & "'
                                and puesto='1' 
                                and estatus=1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                userTemp = myReader("clave_trabajador")
                pwdTemp = myReader("pwd")
                puesto = myReader("puesto")
                region = myReader("region")
            End While

            If userTemp.Equals(usuario) And pwdTemp.Equals(pwd) Then
                Return True
            Else
                Return False

            End If

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try



        Return False
    End Function

End Class