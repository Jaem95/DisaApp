﻿Imports System.Data.SqlClient

Public Class ModificarProductosAD

    Public Function ActualizarInformacionProductos(nombre As String, descripcion As String, presentacion As String, cantidadxcaja As String, codigo_barras_pieza As String, codigo_barras_caja As String, codigoDisa As String, proovedor As String, clasificacion As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update productos  set  nombre = '" & nombre & "' ,descripcion ='" & descripcion & "' ,presentacion = '" & presentacion & "',cantidadxcaja = '" & cantidadxcaja & "',codigo_barras_caja = '" & codigo_barras_caja & "' ,codigoDisa = '" & codigoDisa & "',categoria = " & clasificacion & ", proovedor = " & proovedor & " where codigo_barrras = '" & codigo_barras_pieza & "'   and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion

    End Function


    Public Function ActualizarURLFoto(producto As String, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update productos set urlfoto = '" & ruta & "' where codigoDisa =  '" & producto & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function


    Public Function EliminarProducto(producto As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "delete from productos where codigo_barrras = '" + producto + "'"

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function



End Class
