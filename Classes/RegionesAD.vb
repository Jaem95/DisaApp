﻿Imports System.Data.SqlClient
Public Class RegionesAD

    Public Function ConsultarRegiones() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
                                  ,[nombre]
                                  ,[codigoEmpleado]
                                  ,[nombreGerente]
	                              ,(select count(*) from cliente cli where cli.localidad = re.id  and cli.estatus = 1) as cantidadTiendas
	                              ,(select count(*) from ruta ru where ru.idlocalidad = re.id and ru.estatus = 1 ) as cantidadRutas
                              FROM [dbo].[region]  re  where estatus = 1 and re.id <> 1000001
                            ")

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "tabla")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"
            ds.Tables(0).Columns(2).ColumnName = "codigoEmpleado"
            ds.Tables(0).Columns(3).ColumnName = "nombreGerente"
            ds.Tables(0).Columns(4).ColumnName = "cantidadTiendas"
            ds.Tables(0).Columns(5).ColumnName = "cantidadRutas"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ActualizarInformacionRegion(txtRFC As String, txtNomrbreRegion As String, txtDireccion As String, txtColonia As String, txtMunicipio As String, txtCodigoPostal As String, txtNumeroTelefonico As String, txtCodigoGerente As String, txtNombreDelGerente As String, txtSuperficeBodega As String, txtCapacidadCajas As String, ID As String, txtEstado As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update region  set  nombre = '" & txtNomrbreRegion & "' ,estado ='" & txtEstado & "' ,[codigoEmpleado] = '" & txtCodigoGerente & "',[nombreGerente] = '" & txtNombreDelGerente & "',[RFC] = '" & txtRFC & "' ,[Direccion] = '" & txtDireccion & "',[Colonia] = '" & txtColonia & "', [Municipio] = '" & txtMunicipio & "',[CodigoPostal] = '" & txtCodigoPostal & "',[SuperficieBodegas] = '" & txtSuperficeBodega & "'
        , [Telefono] = '" & txtNumeroTelefonico & "' ,[capacidadCajas] = '" & txtCapacidadCajas & "' where [id] = " & ID & "  and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function

    Public Function ActualizarURLFoto(codigoRegion As String, ruta As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("update region set urlfoto = '" & ruta & "' where id =  '" & codigoRegion & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function


    Public Function EliminarRegion(idRegion As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "
                               update   region  set estatus = 2 where id =  '" & idRegion & "'  and estatus = 1
;"

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function


End Class
