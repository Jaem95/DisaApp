﻿Imports System.Data.SqlClient
Public Class EditarPreciosAD

    Public Function BorraPreciosViejos(codigoBarras As String, ubicacion As String, categoria As String) As Boolean
        Dim validacion As Boolean = False
        Dim validacion2 As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        If ubicacion.Equals("") Then
            query = ("delete from  [precioGeneral]  where [productoCb]  = '" & codigoBarras & "'")

        Else
            query = ("delete from  R" & ubicacion & "precio  where [idProductoCb]  = '" & codigoBarras & "'")
        End If

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected = 0 Then
                'Inserto Products
                If ubicacion.Equals("") Then
                    validacion = True
                Else
                    validacion2 = InsertarProducto(codigoBarras, ubicacion, categoria)
                    If validacion2 Then
                        validacion = True
                    End If
                End If

            ElseIf rowsAffected > 0 Then

                validacion = True
            End If



        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion
    End Function
    Public Function RegistrarPrecios(codigoBarras As String, precio As String, rango1 As String, rango2 As String, ubicacion As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        If ubicacion.Equals("") Then
            query = ("insert into [precioGeneral] values('" & codigoBarras & "','" & precio & "','" & rango1 & "','" & rango2 & "',1)")
        Else
            query = ("insert into   R" & ubicacion & "precio values('" & codigoBarras & "','" & precio & "','" & rango1 & "','" & rango2 & "',1)")
        End If

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion



    End Function

    Public Function ActualizarBanderaPrecio(codigoBarras As String, bandera As String, ubicacion As String) As Boolean
        Dim validacion As Boolean = False
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim rowsAffected As Integer = 0

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand()

        myCommand.CommandText = "InsertarProductosBanderaPrueba"
        myCommand.CommandType = CommandType.StoredProcedure
        myCommand.Parameters.AddWithValue("@codigoBarras", codigoBarras)
        myCommand.Parameters.AddWithValue("@region", ubicacion)
        If bandera.Equals("PrecioGeneral") Then
            myCommand.Parameters.AddWithValue("@tipoPrecio", "G")
        ElseIf bandera.Equals("PrecioRegional") Then
            myCommand.Parameters.AddWithValue("@tipoPrecio", "R")
        End If
        myCommand.CommandTimeout = 9000000
        myCommand.Connection = connexio

        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If
        Catch ex As Exception


        Finally
            connexio.Close()
        End Try

        Return validacion


    End Function

    Public Function ConsultarPreciosGenerales(codigoBarras As String) As List(Of String)
        Dim preciosProducto As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select rango1,rango2,precio from [precioGeneral] where [productoCb] ='" & codigoBarras.Trim() & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                preciosProducto.Add(myReader("rango1").ToString())
                preciosProducto.Add(myReader("rango2").ToString())
                preciosProducto.Add(myReader("precio").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return preciosProducto


    End Function


    Public Function ConsultarTipoPrecioActual(codigoBarras As String) As String
        Dim tipoPrecio As String
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select tipoPrecio from banderaPrecio  where idProducto = '" & codigoBarras & "' ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                tipoPrecio = myReader("tipoPrecio").ToString()
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        If tipoPrecio.Equals("G") Then
            tipoPrecio = "PrecioGeneral"
        ElseIf tipoPrecio.Equals("R") Then
            tipoPrecio = "PrecioRegional"
        End If

        Return tipoPrecio


    End Function

    Friend Function ConsultarPreciosPorRegion(codigoBarras As String, region As String) As List(Of String)
        Dim preciosProducto As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select rango1,rango2,precio from [R" + region + "precio] where [idProductoCB] ='" & codigoBarras.Trim() & "' and [status] = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                preciosProducto.Add(myReader("rango1").ToString())
                preciosProducto.Add(myReader("rango2").ToString())
                preciosProducto.Add(myReader("precio").ToString())
            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return preciosProducto
    End Function

    Friend Function InsertarProducto(codigoBarras As String, ubicacion As String, categoria As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0
        Dim query As String = "
                        INSERT INTO [dbo].[R" + ubicacion + "inventario]
                                   ([idProductoCB]
                                   ,[cantidad]
                                   ,[caducidad]
                                   ,[cantidadLimite]
                                   ,[cantidadMinima]
                                   ,[status]
                                   ,[categoria])
                             VALUES
                                   ('" + codigoBarras + "'
                                   ,'1'
                                   ,CONVERT (date, GETDATE())
                                   ,'1'
                                   ,'1'
                                   ,'1'
                                    ," + categoria + ")"



        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()

        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion




    End Function
End Class
