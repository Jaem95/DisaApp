﻿Imports System.Data.SqlClient
Imports System.Data

'Rama Toño

Public Class ADGeneral
    Public Function ConsultarInformacion(usuario As String) As List(Of String)
        Dim informacionUsuario As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select * from empleados where clave_trabajador ='" & usuario.Trim() & "' and estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionUsuario.Add(myReader("nombre").ToString())
                informacionUsuario.Add(myReader("apellidos").ToString())
                informacionUsuario.Add(myReader("fecha_nacimiento").ToString())
                informacionUsuario.Add(myReader("sexo").ToString())
                informacionUsuario.Add(myReader("telefono").ToString())
                informacionUsuario.Add(myReader("direccion").ToString())
                informacionUsuario.Add(myReader("correo").ToString())
                informacionUsuario.Add(myReader("estado_civil").ToString())
                informacionUsuario.Add(myReader("estudios").ToString())
                informacionUsuario.Add(myReader("rfc").ToString())
                informacionUsuario.Add(myReader("curp").ToString())
                informacionUsuario.Add(myReader("hobbie").ToString())
                informacionUsuario.Add(myReader("descripcion").ToString())
                informacionUsuario.Add(myReader("urlfoto").ToString())


            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionUsuario
    End Function

    Friend Function ConsultarCorreos() As StringBuilder
        Dim informacionUsuario As New StringBuilder
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("
	select a.nombre,a.codigoEmpleado,a.nombreGerente,c.correo
	from region a,empleadosactivos  b ,empleados c where b.puesto  = 1 and  b.clave_trabajador = c.clave_trabajador  and b.region = a.id

")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionUsuario.Append(myReader("correo").ToString()).Append("|")

            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionUsuario
    End Function

    Friend Function ConsultaEstudios() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
      ,[tipo]
  From [dbo].[estudios]")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "ABC")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "tipo"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultaPuesto() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
                                  ,[tipo]
                              FROM [dbo].[puesto]")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "XFG")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "tipo"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultaEstadoCivil() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
                                  ,[tipo]
                              FROM [dbo].[estado_civil]")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "EstadoCivil")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "tipo"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultarInformacionProductoParaInventario(codigoBarras As String) As List(Of String)
        Dim informacionProducto As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select productos.nombre,productos.descripcion,presentacion,cantidadxcaja,codigo_barras_caja,codigo_barrras,cat.id categoria,tipoPresentacion,proovedor,urlfoto,codigoDisa from productos,categoriasProductos as cat where productos.estatus = 1 and cat.id = productos.categoria and [codigo_barrras] ='" & codigoBarras.Trim() & "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionProducto.Add(myReader("nombre").ToString())

                informacionProducto.Add(myReader("descripcion").ToString())
                informacionProducto.Add(myReader("presentacion").ToString())

                informacionProducto.Add(myReader("cantidadxcaja").ToString())
                informacionProducto.Add(myReader("codigo_barras_caja").ToString())

                informacionProducto.Add(myReader("codigo_barrras").ToString())
                informacionProducto.Add(myReader("categoria").ToString())

                informacionProducto.Add(myReader("tipoPresentacion").ToString())
                informacionProducto.Add(myReader("proovedor").ToString())

                informacionProducto.Add(myReader("urlfoto").ToString())
                informacionProducto.Add(myReader("codigoDisa").ToString())


            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionProducto
    End Function

    Friend Function ConsultaTipoSexo() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id]
                                  ,[tipo]
                              FROM [dbo].[sexo]")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "Sexo")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "tipo"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function ConsultarEstados() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("SELECT [id_estado]
                                  ,[estado]
                              FROM [dbo].[EstadosMexico]")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "Estados")


            ds.Tables(0).Columns(0).ColumnName = "id_estado"
            ds.Tables(0).Columns(1).ColumnName = "estado"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Friend Function BuscarUbicacionEspecifica(ubicacion As String) As String
        Dim ubi As String = ""

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("   select nombre  from region where id  = " + ubicacion + "
                                    ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                ubi = (myReader("nombre").ToString())



            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ubi
    End Function

    Friend Function ConsultarInformacionRegiones(codigoRegion As String) As List(Of String)
        Dim informacionRegion As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("    SELECT [nombre]
                                          ,[estado]
                                          ,[codigoEmpleado]
                                          ,[nombreGerente]
                                          ,[RFC]
                                          ,[Direccion]
                                          ,[Colonia]
                                          ,[Municipio]
                                          ,[CodigoPostal]
                                          ,[SuperficieBodegas]
                                          ,[Telefono]
                                          ,[capacidadCajas]
                                      FROM [dbo].[region]
                                      where estatus = 1 and id = " & codigoRegion & "
                                    ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionRegion.Add(myReader("nombre").ToString())

                informacionRegion.Add(myReader("estado").ToString())
                informacionRegion.Add(myReader("codigoEmpleado").ToString())

                informacionRegion.Add(myReader("nombreGerente").ToString())
                informacionRegion.Add(myReader("RFC").ToString())

                informacionRegion.Add(myReader("Direccion").ToString())
                informacionRegion.Add(myReader("Colonia").ToString())

                informacionRegion.Add(myReader("Municipio").ToString())
                informacionRegion.Add(myReader("CodigoPostal").ToString())

                informacionRegion.Add(myReader("SuperficieBodegas").ToString())
                informacionRegion.Add(myReader("Telefono").ToString())
                informacionRegion.Add(myReader("capacidadCajas").ToString())



            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionRegion

    End Function

    Public Function ConsultarInformacionProducto(producto As String) As List(Of String)
        Dim informacionProducto As New List(Of String)
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select productos.nombre,productos.descripcion,presentacion,cantidadxcaja,codigo_barras_caja,codigo_barrras,cat.nombre categoria,tipoPresentacion,proovedor,urlfoto,codigoDisa,cat.id as id_cat from productos,categoriasProductos as cat where productos.estatus = 1 and cat.id = productos.categoria and [codigo_barrras] ='" & producto.Trim() & "'")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio

        Try
            connexio.Open()
            Dim myReader As SqlDataReader = myCommand.ExecuteReader()
            While myReader.Read()
                informacionProducto.Add(myReader("nombre").ToString())

                informacionProducto.Add(myReader("descripcion").ToString())
                informacionProducto.Add(myReader("presentacion").ToString())

                informacionProducto.Add(myReader("cantidadxcaja").ToString())
                informacionProducto.Add(myReader("codigo_barras_caja").ToString())

                informacionProducto.Add(myReader("codigo_barrras").ToString())
                informacionProducto.Add(myReader("categoria").ToString())

                informacionProducto.Add(myReader("tipoPresentacion").ToString())
                informacionProducto.Add(myReader("proovedor").ToString())

                informacionProducto.Add(myReader("urlfoto").ToString())
                informacionProducto.Add(myReader("codigoDisa").ToString())
                informacionProducto.Add(myReader("id_cat").ToString())


            End While

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return informacionProducto
    End Function


    Public Function ConsultarProovedor() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select id,nombre from proovedor where estatus = 1")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "Proovedor")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Public Function ConsultarTipoDePieza() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select id,tipo from presentacionProductos where  estatus = 1 order by id ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "TipoPieza")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "tipo"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function


    Public Function ConsultarClasificacion() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select id,nombre from categoriasProductos where estatus = 1 ")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "ClasificacionProductos")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function


    Public Function ConsultarRegiones() As DataSet
        Dim ds As New Data.DataSet
        Dim da As New SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = ("select id,nombre from region where estatus = 1 and id <> 1000001")
        Dim connexio As SqlConnection = New SqlConnection(myConnectionString)
        Dim myCommand As SqlCommand = New SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "Region")


            ds.Tables(0).Columns(0).ColumnName = "id"
            ds.Tables(0).Columns(1).ColumnName = "nombre"


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function








End Class
