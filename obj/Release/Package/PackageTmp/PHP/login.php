<!DOCTYPE HTML>
<?php
	session_start();
	if($_SESSION['pin']!="")
	{
		echo '<script language = javascript>
					
					self.location = "home.php"
					</script>';
	}

?>
<html lang="es-MX">
<head>
	<title>My Weddapp</title>
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    
	<link rel="stylesheet" href="css/bootstrap.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/linea-icon.css" />
	<link rel="stylesheet" href="css/fancy-buttons.css" />
	
	<!--=== Google Fonts ===-->
	<link href='http://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:600,400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<!--=== Other CSS files ===-->
	<link rel="stylesheet" href="css/animate.css" />
	<link rel="stylesheet" href="css/jquery.vegas.css" />
	<link rel="stylesheet" href="css/baraja.css" />
	<link rel="stylesheet" href="css/jquery.bxslider.css" />
	
	<!--=== Main Stylesheets ===-->
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/responsive.css" />
	
	<!--=== Color Scheme, three colors are available red.css, orange.css and gray.css ===-->
	<link rel="stylesheet" id="scheme-source" href="css/schemes/red.css" />
    
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script src="jquery.confirm.js"></script>

<script>
	$.confirm.options = {
    text: "In order to reset your password, we need you to write down your email account.<br><input type='text' class='form-control' id='emailParaPassword' placeholder='Email for reset password' value=''/>",
    title: "Ok, we understand...",
    confirmButton: "Please reset my Password",
    cancelButton: "Cancel",
    post: false,
    confirmButtonClass: "btn-success",
    cancelButtonClass: "btn-default",
    dialogClass: "modal-dialog"
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}
	function pedirEmail()
	{		
			
			$.confirm({
                    confirm: function() 
					{
						event.preventDefault();
						var emailRevisar =$("#emailParaPassword").val();
						if(emailRevisar=="")
						{
							alert("Please write the account email to reset your password.");
						}
						else
						{
							if(isValidEmailAddress(emailRevisar))
							{
								$.ajax({
									type        : "POST",
									url			: "http://myweddapp.com/site/resetPassword.php",
									data: {emailReset:emailRevisar},
									crossDomain : true,
									success     :function(data)
									{		
										alert(data);
									},
									error       :function(e) 
									{

									}
								});
							}
							else
							{
								alert("Please write a valid email address");
							}
						}
                    },
                    cancel: function() {
                       
                    }
                });
	}
	
	
	</script>
	
	<!--=== Internet explorer fix ===-->
	<!-- [if lt IE 9]>
		<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif] -->
	
	
</head>
<body>
	<!--=== Preloader section starts ===-->
	<section id="preloader">
		<div class="loading-circle fa-spin"></div>
	</section>
	<!--=== Preloader section Ends ===-->
	
	<!--=== Header section Starts ===-->
	<div id="header" class="header-section">
    				
		<!-- sticky-bar Starts-->
		<div class="sticky-bar-wrap">
			<div class="sticky-section">
				<div id="topbar-hold" class="nav-hold container">
					<nav class="navbar" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
							</button>
							<!--=== Site Name ===-->
							<img style="position:absolute; margin-top:2%;" width="20%" height="auto" src="images/logo.png"/>
						</div>
						
						<!-- Main Navigation menu Starts -->
						<div class="collapse navbar-collapse navbar-responsive-collapse">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="http://myweddapp.com/">Home</a></li>
  							</ul>
						</div>
						<!-- Main Navigation menu ends-->
					</nav>
				</div>
			</div>
           <div class="social-icons" style="text-align:right; margin-right:2%;">
				<ul>
					<li><a href="http://myweddapp.com/es/site/login.php"><img src="images/banderaEss.png" width="25" height="auto"/></a></li>
					<li><a href="http://myweddapp.com/site/login.php"><img src="images/banderaEnn.png" width="25" height="auto"/></a></li>

				</ul>
			</div>
		</div>
		<!-- sticky-bar Ends-->
		<!--=== Header section Ends ===-->
		
		<!--=== Home Section Starts ===-->
		<div id="section-home" class="home-section-wrap center">
			<div class="section-overlay"></div>
			<div class="container home">
				<div class="row">
					<h1 class="well-come" style="margin-top:100px; font-size:30px;">Login or Register</h1>
                    <div class="col-md-3 col-md-offset-2">
                    			<h1 class="featured-title color-scheme" style="margin-top:90px; font-size:30px;">Login</h1>
								<form action="conectar.php" method="post" role="search">
									
								  <div class="form-group" >
								  		<label for="mensajeI" style="color:#fff;" >Email:    </label>
								    	<input type="text" name="email"  class="form-control"  value="" >
								  </div> 

									<div class="form-group" >
								  		<label for="mensajeI" style="color:#fff;" >Password:    </label>
								    	<input type="password" name="pass"  class="form-control"  value="" >
								  </div> 
									<a href="#" onClick="pedirEmail()"><span>Forgot your password? Click here</span></a>
									<div class="form-group">
                                        <button type="submit" id="nButton" class="fancy-button button-line btn-col small zoom">
                                            Login
                                            <span class="icon color-scheme">
                                                <i class="fa fa-arrow-right"></i>
                                            </span>
                                        </button>
                                    </div>

								</form>	  
					</div>
                    <div class="col-md-5 col-md-offset-1">
                    <h1 class="featured-title color-scheme" style="margin-top:10px; font-size:30px;">Register</h1>
		
					<form role="form" action="signup.php" enctype="multipart/form-data" method="post">
						
								  <div class="form-group col-md-6" >
								  		<label for="mensajeI" style="color:#fff;" >My name:    </label>
								    	<input type="text" name="mname"  class="form-control"  value="" required>
								  </div> 
								   <div class="form-group col-md-6" >
								   	<label for="mensajeI" style="color:#fff;" >Last name: </label>
								    	<input type="text" name="mlastname"  class="form-control"  value="" required>
								   </div> 
								  <div class="form-group col-md-6">
								  		<label for="mensajeI" style="color:#fff;" >Fiance name:</label>
								    	<input type="text" name="fname" class="form-control"  value="" required>
								  </div> 
								   <div class="form-group col-md-6">
								   	<label for="mensajeI" style="color:#fff;" >Fiance Last name:</label>
								    	<input type="text" name="flastname" class="form-control" value="" required>
								   </div> 
                                   <div class="form-group col-md-6">
								   	<label for="paisOrigen" style="color:#fff;" >Country:</label>
								    	<select id="paisOrigen" style="width:100%;" name="paisOrigen">
                                            <option value="AFG">Afghanistan</option>
                                            <option value="ALA">Åland Islands</option>
                                            <option value="ALB">Albania</option>
                                            <option value="DZA">Algeria</option>
                                            <option value="ASM">American Samoa</option>
                                            <option value="AND">Andorra</option>
                                            <option value="AGO">Angola</option>
                                            <option value="AIA">Anguilla</option>
                                            <option value="ATA">Antarctica</option>
                                            <option value="ATG">Antigua and Barbuda</option>
                                            <option value="ARG">Argentina</option>
                                            <option value="ARM">Armenia</option>
                                            <option value="ABW">Aruba</option>
                                            <option value="AUS">Australia</option>
                                            <option value="AUT">Austria</option>
                                            <option value="AZE">Azerbaijan</option>
                                            <option value="BHS">Bahamas</option>
                                            <option value="BHR">Bahrain</option>
                                            <option value="BGD">Bangladesh</option>
                                            <option value="BRB">Barbados</option>
                                            <option value="BLR">Belarus</option>
                                            <option value="BEL">Belgium</option>
                                            <option value="BLZ">Belize</option>
                                            <option value="BEN">Benin</option>
                                            <option value="BMU">Bermuda</option>
                                            <option value="BTN">Bhutan</option>
                                            <option value="BOL">Bolivia, Plurinational State of</option>
                                            <option value="BES">Bonaire, Sint Eustatius and Saba</option>
                                            <option value="BIH">Bosnia and Herzegovina</option>
                                            <option value="BWA">Botswana</option>
                                            <option value="BVT">Bouvet Island</option>
                                            <option value="BRA">Brazil</option>
                                            <option value="IOT">British Indian Ocean Territory</option>
                                            <option value="BRN">Brunei Darussalam</option>
                                            <option value="BGR">Bulgaria</option>
                                            <option value="BFA">Burkina Faso</option>
                                            <option value="BDI">Burundi</option>
                                            <option value="KHM">Cambodia</option>
                                            <option value="CMR">Cameroon</option>
                                            <option value="CAN">Canada</option>
                                            <option value="CPV">Cape Verde</option>
                                            <option value="CYM">Cayman Islands</option>
                                            <option value="CAF">Central African Republic</option>
                                            <option value="TCD">Chad</option>
                                            <option value="CHL">Chile</option>
                                            <option value="CHN">China</option>
                                            <option value="CXR">Christmas Island</option>
                                            <option value="CCK">Cocos (Keeling) Islands</option>
                                            <option value="COL">Colombia</option>
                                            <option value="COM">Comoros</option>
                                            <option value="COG">Congo</option>
                                            <option value="COD">Congo, the Democratic Republic of the</option>
                                            <option value="COK">Cook Islands</option>
                                            <option value="CRI">Costa Rica</option>
                                            <option value="CIV">Côte d'Ivoire</option>
                                            <option value="HRV">Croatia</option>
                                            <option value="CUB">Cuba</option>
                                            <option value="CUW">Curaçao</option>
                                            <option value="CYP">Cyprus</option>
                                            <option value="CZE">Czech Republic</option>
                                            <option value="DNK">Denmark</option>
                                            <option value="DJI">Djibouti</option>
                                            <option value="DMA">Dominica</option>
                                            <option value="DOM">Dominican Republic</option>
                                            <option value="ECU">Ecuador</option>
                                            <option value="EGY">Egypt</option>
                                            <option value="SLV">El Salvador</option>
                                            <option value="GNQ">Equatorial Guinea</option>
                                            <option value="ERI">Eritrea</option>
                                            <option value="EST">Estonia</option>
                                            <option value="ETH">Ethiopia</option>
                                            <option value="FLK">Falkland Islands (Malvinas)</option>
                                            <option value="FRO">Faroe Islands</option>
                                            <option value="FJI">Fiji</option>
                                            <option value="FIN">Finland</option>
                                            <option value="FRA">France</option>
                                            <option value="GUF">French Guiana</option>
                                            <option value="PYF">French Polynesia</option>
                                            <option value="ATF">French Southern Territories</option>
                                            <option value="GAB">Gabon</option>
                                            <option value="GMB">Gambia</option>
                                            <option value="GEO">Georgia</option>
                                            <option value="DEU">Germany</option>
                                            <option value="GHA">Ghana</option>
                                            <option value="GIB">Gibraltar</option>
                                            <option value="GRC">Greece</option>
                                            <option value="GRL">Greenland</option>
                                            <option value="GRD">Grenada</option>
                                            <option value="GLP">Guadeloupe</option>
                                            <option value="GUM">Guam</option>
                                            <option value="GTM">Guatemala</option>
                                            <option value="GGY">Guernsey</option>
                                            <option value="GIN">Guinea</option>
                                            <option value="GNB">Guinea-Bissau</option>
                                            <option value="GUY">Guyana</option>
                                            <option value="HTI">Haiti</option>
                                            <option value="HMD">Heard Island and McDonald Islands</option>
                                            <option value="VAT">Holy See (Vatican City State)</option>
                                            <option value="HND">Honduras</option>
                                            <option value="HKG">Hong Kong</option>
                                            <option value="HUN">Hungary</option>
                                            <option value="ISL">Iceland</option>
                                            <option value="IND">India</option>
                                            <option value="IDN">Indonesia</option>
                                            <option value="IRN">Iran, Islamic Republic of</option>
                                            <option value="IRQ">Iraq</option>
                                            <option value="IRL">Ireland</option>
                                            <option value="IMN">Isle of Man</option>
                                            <option value="ISR">Israel</option>
                                            <option value="ITA">Italy</option>
                                            <option value="JAM">Jamaica</option>
                                            <option value="JPN">Japan</option>
                                            <option value="JEY">Jersey</option>
                                            <option value="JOR">Jordan</option>
                                            <option value="KAZ">Kazakhstan</option>
                                            <option value="KEN">Kenya</option>
                                            <option value="KIR">Kiribati</option>
                                            <option value="PRK">Korea, Democratic People's Republic of</option>
                                            <option value="KOR">Korea, Republic of</option>
                                            <option value="KWT">Kuwait</option>
                                            <option value="KGZ">Kyrgyzstan</option>
                                            <option value="LAO">Lao People's Democratic Republic</option>
                                            <option value="LVA">Latvia</option>
                                            <option value="LBN">Lebanon</option>
                                            <option value="LSO">Lesotho</option>
                                            <option value="LBR">Liberia</option>
                                            <option value="LBY">Libya</option>
                                            <option value="LIE">Liechtenstein</option>
                                            <option value="LTU">Lithuania</option>
                                            <option value="LUX">Luxembourg</option>
                                            <option value="MAC">Macao</option>
                                            <option value="MKD">Macedonia, the former Yugoslav Republic of</option>
                                            <option value="MDG">Madagascar</option>
                                            <option value="MWI">Malawi</option>
                                            <option value="MYS">Malaysia</option>
                                            <option value="MDV">Maldives</option>
                                            <option value="MLI">Mali</option>
                                            <option value="MLT">Malta</option>
                                            <option value="MHL">Marshall Islands</option>
                                            <option value="MTQ">Martinique</option>
                                            <option value="MRT">Mauritania</option>
                                            <option value="MUS">Mauritius</option>
                                            <option value="MYT">Mayotte</option>
                                            <option value="MEX">México</option>
                                            <option value="FSM">Micronesia, Federated States of</option>
                                            <option value="MDA">Moldova, Republic of</option>
                                            <option value="MCO">Monaco</option>
                                            <option value="MNG">Mongolia</option>
                                            <option value="MNE">Montenegro</option>
                                            <option value="MSR">Montserrat</option>
                                            <option value="MAR">Morocco</option>
                                            <option value="MOZ">Mozambique</option>
                                            <option value="MMR">Myanmar</option>
                                            <option value="NAM">Namibia</option>
                                            <option value="NRU">Nauru</option>
                                            <option value="NPL">Nepal</option>
                                            <option value="NLD">Netherlands</option>
                                            <option value="NCL">New Caledonia</option>
                                            <option value="NZL">New Zealand</option>
                                            <option value="NIC">Nicaragua</option>
                                            <option value="NER">Niger</option>
                                            <option value="NGA">Nigeria</option>
                                            <option value="NIU">Niue</option>
                                            <option value="NFK">Norfolk Island</option>
                                            <option value="MNP">Northern Mariana Islands</option>
                                            <option value="NOR">Norway</option>
                                            <option value="OMN">Oman</option>
                                            <option value="PAK">Pakistan</option>
                                            <option value="PLW">Palau</option>
                                            <option value="PSE">Palestinian Territory, Occupied</option>
                                            <option value="PAN">Panama</option>
                                            <option value="PNG">Papua New Guinea</option>
                                            <option value="PRY">Paraguay</option>
                                            <option value="PER">Peru</option>
                                            <option value="PHL">Philippines</option>
                                            <option value="PCN">Pitcairn</option>
                                            <option value="POL">Poland</option>
                                            <option value="PRT">Portugal</option>
                                            <option value="PRI">Puerto Rico</option>
                                            <option value="QAT">Qatar</option>
                                            <option value="REU">Réunion</option>
                                            <option value="ROU">Romania</option>
                                            <option value="RUS">Russian Federation</option>
                                            <option value="RWA">Rwanda</option>
                                            <option value="BLM">Saint Barthélemy</option>
                                            <option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
                                            <option value="KNA">Saint Kitts and Nevis</option>
                                            <option value="LCA">Saint Lucia</option>
                                            <option value="MAF">Saint Martin (French part)</option>
                                            <option value="SPM">Saint Pierre and Miquelon</option>
                                            <option value="VCT">Saint Vincent and the Grenadines</option>
                                            <option value="WSM">Samoa</option>
                                            <option value="SMR">San Marino</option>
                                            <option value="STP">Sao Tome and Principe</option>
                                            <option value="SAU">Saudi Arabia</option>
                                            <option value="SEN">Senegal</option>
                                            <option value="SRB">Serbia</option>
                                            <option value="SYC">Seychelles</option>
                                            <option value="SLE">Sierra Leone</option>
                                            <option value="SGP">Singapore</option>
                                            <option value="SXM">Sint Maarten (Dutch part)</option>
                                            <option value="SVK">Slovakia</option>
                                            <option value="SVN">Slovenia</option>
                                            <option value="SLB">Solomon Islands</option>
                                            <option value="SOM">Somalia</option>
                                            <option value="ZAF">South Africa</option>
                                            <option value="SGS">South Georgia and the South Sandwich Islands</option>
                                            <option value="SSD">South Sudan</option>
                                            <option value="ESP">Spain</option>
                                            <option value="LKA">Sri Lanka</option>
                                            <option value="SDN">Sudan</option>
                                            <option value="SUR">Suriname</option>
                                            <option value="SJM">Svalbard and Jan Mayen</option>
                                            <option value="SWZ">Swaziland</option>
                                            <option value="SWE">Sweden</option>
                                            <option value="CHE">Switzerland</option>
                                            <option value="SYR">Syrian Arab Republic</option>
                                            <option value="TWN">Taiwan, Province of China</option>
                                            <option value="TJK">Tajikistan</option>
                                            <option value="TZA">Tanzania, United Republic of</option>
                                            <option value="THA">Thailand</option>
                                            <option value="TLS">Timor-Leste</option>
                                            <option value="TGO">Togo</option>
                                            <option value="TKL">Tokelau</option>
                                            <option value="TON">Tonga</option>
                                            <option value="TTO">Trinidad and Tobago</option>
                                            <option value="TUN">Tunisia</option>
                                            <option value="TUR">Turkey</option>
                                            <option value="TKM">Turkmenistan</option>
                                            <option value="TCA">Turks and Caicos Islands</option>
                                            <option value="TUV">Tuvalu</option>
                                            <option value="UGA">Uganda</option>
                                            <option value="UKR">Ukraine</option>
                                            <option value="ARE">United Arab Emirates</option>
                                            <option value="GBR">United Kingdom</option>
                                            <option value="USA">United States</option>
                                            <option value="UMI">United States Minor Outlying Islands</option>
                                            <option value="URY">Uruguay</option>
                                            <option value="UZB">Uzbekistan</option>
                                            <option value="VUT">Vanuatu</option>
                                            <option value="VEN">Venezuela, Bolivarian Republic of</option>
                                            <option value="VNM">Viet Nam</option>
                                            <option value="VGB">Virgin Islands, British</option>
                                            <option value="VIR">Virgin Islands, U.S.</option>
                                            <option value="WLF">Wallis and Futuna</option>
                                            <option value="ESH">Western Sahara</option>
                                            <option value="YEM">Yemen</option>
                                            <option value="ZMB">Zambia</option>
                                            <option value="ZWE">Zimbabwe</option>
                                        </select>
								   </div> 
                                   <div class="form-group col-md-6">
        
                                            <label for="mensajeI" style="color:#fff;" >Weeding date (yyyy/mm/dd):</label>
                                            <input type="date" name="wdate" class="form-control" required>
                                </div>
                                   <div class="form-group col-md-6">
								  		<label for="mensajeI" style="color:#fff;" >Email:</label>
								    	<input type="email" name="nemail" class="form-control"  value="" required>
								  </div> 
                                  <div class="form-group col-md-6">
								  		<label for="mensajeI" style="color:#fff;" >Password:</label>
								    	<input type="password" name="npass" class="form-control"  value="" required>
								  </div> 

                                
                                <div class="form-group col-md-6">
        
                                            <label for="mensajeI" style="color:#fff;" >Main photo:</label>
                                            <input type="file" name="fotoo" id="fotoo" required>
                                </div>
                                <div class="form-group col-md-12">
                                  		<span style="color:#fff;" id="terminosCondiciones">I Accept the <a style="color:#E74C3C;" href="http://myweddapp.com/termsAndConditions.pdf" target="_blank">Terms and Conditions</a></span>
    									<input type="checkbox" style="vertical-align:middle;" id="terms" value="Yes" required><br>
    								</div>
                                <div class="form-group col-md-12">
                                    <button type="submit" name="signUp" class="fancy-button button-line btn-col small zoom">
                                        Sign Up
                                        <span class="icon color-scheme">
                                                        <i class="fa fa-pencil-square-o"></i>
                                        </span>
                                    </button>
                                </div>
                                <input type="hidden" name="plataforma" id="plataforma" value="web">

					</form>		  
					</div>
				</div>
            </div>
		</div>
		<!--=== Home Section Ends ===-->
	</div>
	
	<!--=== Download section Starts ===-->
	<section id="section-download" style="background-color:#FFF;" class="download-wrap">
		<div class="container download center">
        	<div class="row">
            	<div class="col-md-10 col-md-offset-1 center section-title">
						<h3 style="color:rgba(51,51,51,1)">Download</h3>
				</div>
            </div>
			<div class="row">
				<div class="col-lg-6">
					<div class="col-md-10 col-md-offset-1 center section-title">
						<h3 style="color:rgba(51,51,51,1)">MyWeddApp</h3>
					</div>
					<div class="download-buttons clearfix"> <!-- Download Buttons -->
						<a class="fancy-button button-line no-text btn-col large zoom" href="https://itunes.apple.com/us/app/myweddapp/id1057756448?l=es&ls=1&mt=8" title="Download from App store">
							<span class="icon">
							<i class="fa fa-apple"></i>
							</span>
						</a>
						<a class="fancy-button button-line btn-col no-text large zoom" href="https://play.google.com/store/apps/details?id=com.gyosolutions.myweddapp" title="Download from Android store">
							<span class="icon">
							<i class="fa fa-android"></i>
							</span>
						</a>
						<a class="fancy-button button-line btn-col no-text large zoom" href="https://www.microsoft.com/es-mx/store/apps/myweddapp/9nblggh699hj" title="Download from Windows store">
							<span class="icon">
							<i class="fa fa-windows"></i>
							</span>
						</a>
					</div>
				</div>
                <div class="col-lg-6">
					<div class="col-md-10 col-md-offset-1 center section-title">
						<h3 style="color:rgba(51,51,51,1)">MyWeddApp -  Guest</h3>
					</div>
					<div class="download-buttons clearfix"> <!-- Download Buttons -->
						<a class="fancy-button button-line no-text btn-col large zoom" href="https://itunes.apple.com/us/app/myweddapp-guest/id1052832908?l=es&ls=1&mt=8" title="Download from App store">
							<span class="icon">
							<i class="fa fa-apple"></i>
							</span>
						</a>
						<a class="fancy-button button-line btn-col no-text large zoom" href="https://play.google.com/store/apps/details?id=com.gyosolutions.myweddappguest" title="Download from Android store">
							<span class="icon">
							<i class="fa fa-android"></i>
							</span>
						</a>
						<a class="fancy-button button-line btn-col no-text large zoom" href="https://www.microsoft.com/es-mx/store/apps/myweddapp-guest/9nblggh68mf4" title="Download from Windows store">
							<span class="icon">
							<i class="fa fa-windows"></i>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--=== Download section Ends ===-->
	
	<!--=== Footer section Starts ===-->
	<div id="section-footer" class="footer-wrap">
		<div class="container footer center">
			<div class="row">
				<div class="col-lg-12">
					<h4 class="footer-title"><!-- Footer Title -->
						<a href="#"><img width="30%" height="auto" src="images/logo.png"/></a>
					</h4>
					
					<!-- Social Links -->
					<div class="social-icons">
						<ul>
							<li><a target="_blank" href="https://www.facebook.com/myweddapp"><i class="fa fa-facebook-square"></i></a></li>
							<li><a target="_blank" href="https://twitter.com/MyWeddApp"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/my.weddapp/"><i class="fa fa-instagram"></i></a></li>
							<li><a target="_blank" href="https://www.pinterest.com/myweddapp/"><i class="fa fa-pinterest-square"></i></a></li>
						</ul>
					</div>
					
					<p class="copyright">GYO Solutions All rights reserved &copy; 2015</p>
				</div>
			</div>
		</div>
	</div>
	<!--=== Footer section Ends ===-->
	
<!--==== Js files ====-->
<!--==== Essential files ====-->


<!--==== Slider and Card style plugin ====
<script type="text/javascript" src="js/jquery.baraja.js"></script>-->
<script type="text/javascript" src="js/jquery.vegas.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>

<!--==== MailChimp Widget plugin ====
<script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>-->

<!--==== Scroll and navigation plugins ====-->
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/jquery.nav.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!--==== Custom Script files ====-->
<script type="text/javascript" src="js/custom.js"></script>

<script>
function changeOrange()
{
	$("#scheme-source").attr("href", "css/schemes/orange.css");
}
function changeRed()
{
	$("#scheme-source").attr("href", "css/schemes/red.css");
}
function changeGray()
{
	$("#scheme-source").attr("href", "css/schemes/gray.css");
}
function changePurple()
{
	$("#scheme-source").attr("href", "css/schemes/purple.css");
}
function changeRose()
{
	$("#scheme-source").attr("href", "css/schemes/rose.css");
}
</script>

</body>
</html>