﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Gerente.aspx.vb" Inherits="DISA.Gerente" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="../DISA.png"/>
<title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {

        window.parent.location.href = window.parent.location.href;
    }
    function Redirigir(pagina)
    {
       // alert('Hola');
        document.getElementById('iframe').src = pagina;
    }
</script>    

<body class="hold-transition skin-blue sidebar-mini" >
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/Paginas/Gerente.aspx" class="logo">
                    <span class="logo-mini"><b>DISA</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DISA</b> Managment</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <%--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">--%>
                                    <asp:Image ID="imageUsuario3" runat="server" class="user-image" />
                                    <span class="hidden-xs">
                                        <asp:Label ID="lblNombre2" runat="server" Text="Label"></asp:Label></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <%--<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--%>
                                        <asp:Image ID="imageUsuario2" runat="server" class="img-circle" />
                                        <p>
                                            <asp:Label ID="lblNombre3" runat="server" Text=""></asp:Label>

                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a  onclick="Redirigir('/Paginas/PaginasGerente/PerfilGerente.aspx')" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                         <asp:Button runat="server" CssClass="btn btn-default btn-flat" Text="Cerrar Sesion" ID="btnCerrarSesion"  OnClick="btnCerrarSesion_Click"/>
          
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                            <asp:Image ID="imageUsuario1" runat="server" class="img-circle" />
                        </div>
                        <div class="pull-left info">
                            <p>
                                <asp:Label ID="lblNombre4" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <!-- Sidebar user panel -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menu Principal</li>
                      
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-group"></i>
                                <span>Empleados</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ConsultaEmpleados.aspx')"><i class="fa fa-circle-o"></i>Consulta de Empleados</a></li>
                                <%--   <a  onclick="Redirigir('/Paginas/PaginasGerente/PerfilGerente.aspx')" class="btn btn-default btn-flat">Perfil</a> --%>
                                <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/RegistroEmpleados.aspx')"><i class="fa fa-circle-o"></i>Registrar Empleados</a></li>
                                <%--   <a  onclick="Redirigir('/Paginas/PaginasGerente/PerfilGerente.aspx')" class="btn btn-default btn-flat">Perfil</a> --%>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-dollar"></i>
                                <span>Cobranza</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ConsultaPagosRepartidores.aspx')"><i class="fa fa-circle-o"></i>Corte de Repartidores</a></li>
                              
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-balance-scale"></i>
                                <span>Reportes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ReporteVendedores.aspx')"><i class="fa fa-circle-o"></i>Reporte de Vendedores</a></li>
                                 <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ReporteRepartidores.aspx')"><i class="fa fa-circle-o"></i>Reporte de Repartidores</a></li>
                                 <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ReporteBodeguero.aspx')"><i class="fa fa-circle-o"></i>Reporte de Bodeguero</a></li>
                                  <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ReportesClientes.aspx')"><i class="fa fa-circle-o"></i>Reporte de Clientes</a></li>
                                <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/HistorialPedidos.aspx')"><i class="fa fa-circle-o"></i>Historial de Pedidos</a></li>
                                  <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/RecordProductos.aspx')"><i class="fa fa-circle-o"></i>Record de Productos</a></li>
                            </ul>
                        </li>


                       


                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-map-marker"></i>
                                <span>Clientes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/ConsultaClientes.aspx')"><i class="fa fa-circle-o"></i>Clientes</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-sheqel"></i>
                                <span>Contacto</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                 <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/Contacto.aspx')"><i class="fa fa-circle-o"></i>Contacto</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-group "></i>
                                <span>Inventario</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                  <li><a href="#" onclick="Redirigir('/Paginas/PaginasGerente/RegistrarProducto.aspx')"><i class="fa fa-circle-o"></i>Dar alta producto</a></li>
                            </ul>
                        </li>


                            <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-group "></i>
                                <span>Encuentas</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/paginaCuestionario/cuestionario.php" target="_blank"><i class="fa fa-circle-o"></i>Encuestas</a></li>
                            
                            </ul>
                        </li>




                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-top:0px;">
                <!-- Main content -->
                <section class="content" style="margin-top:0px; padding-left:0px; padding:0px;">
                    <!-- Small boxes (Stat box) -->
                    <div class="row" style="background-color:#ecf0f5;">
                       <iframe runat="server" id="iframe" src="PaginasGerente/blank.aspx" width="100%" height="700" frameborder="0"  style="background-color:#ecf0f5;">
                       </iframe>
                    </div>
                    <!-- /.row (main row) -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy;2016 <a href="">Hello World Technologies</a>.</strong> All rights
    reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <!-- /.control-sidebar-menu -->
                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->

                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </form>
<!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->    
   <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
    
</body>