﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditarRegiones.aspx.vb" Inherits="DISA.EditarRegiones" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <script type="text/javascript">
        function ConfirmarRegistro() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Desea Realizar Esta Inserccion???")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/Paginas/Admin.aspx" class="logo">
                    <span class="logo-mini"><b>DISA</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DISA</b> Managment</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <%--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">--%>
                                    <asp:Image ID="imageUsuario3" runat="server" class="user-image" />
                                    <span class="hidden-xs">
                                        <asp:Label ID="lblNombre2" runat="server" Text="Label"></asp:Label></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <%--<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--%>
                                        <asp:Image ID="imageUsuario2" runat="server" class="img-circle" />
                                        <p>
                                            <asp:Label ID="lblNombre3" runat="server" Text=""></asp:Label>

                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            <asp:Button runat="server" CssClass="btn btn-default btn-flat" Text="Cerrar Sesion" ID="btnCerrarSesion"  OnClick="btnCerrarSesion_Click"/>
          
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
                    <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                            <asp:Image ID="imageUsuario1" runat="server" class="img-circle" />
                        </div>
                        <div class="pull-left info">
                            <p>
                                <asp:Label ID="lblNombre4" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <!-- Sidebar user panel -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menu Principal</li>
                      
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-cubes"></i>
                                <span>Productos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Productos.aspx"><i class="fa fa-circle-o"></i>Productos Generales</a></li>
                                <li><a href="/Paginas/PaginasAdmin/ModificarProductos.aspx"><i class="fa fa-circle-o"></i>Modificacion de Productos</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistrarProducto.aspx"><i class="fa fa-circle-o"></i>Alta de Productos</a></li>
                                <li><a href="/Paginas/PaginasAdmin/Caducidades.aspx"><i class="fa fa-circle-o"></i>Caducidades y Flujo de
                          <br />
                                    Inventarios</a></li>
                                <li><a href="/Paginas/PaginasAdmin/Categorias.aspx"><i class="fa fa-circle-o"></i>Categorias Generales</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroCategorias.aspx"><i class="fa fa-circle-o"></i>Alta de Categorias</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-dollar"></i>
                                <span>Precios</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/PreciosGenerales.aspx"><i class="fa fa-circle-o"></i>Edicion de Precios</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-balance-scale"></i>
                                <span>Promociones</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/RegistroPromociones.aspx"><i class="fa fa-circle-o"></i>Registro de Promociones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/PromocionesPrecios.aspx"><i class="fa fa-circle-o"></i>Promociones de Precios</a></li>
                                <li><a href="/Paginas/PaginasAdmin/PromocionesProductos.aspx"><i class="fa fa-circle-o"></i>Promociones de Productos</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa   fa-file-text"></i>
                                <span>Reportes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de Productos</a></li>
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de Ventas</a></li>
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de DisaPuntos</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-map-marker"></i>
                                <span>Regiones</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Regiones.aspx"><i class="fa fa-circle-o"></i>Consulta de Regiones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroRegiones.aspx"><i class="fa fa-circle-o"></i>Alta de Regiones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/EditarRegiones.aspx"><i class="fa fa-circle-o"></i>Modifacion de Regiones</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-sheqel"></i>
                                <span>DISA-Puntos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Puntos.aspx"><i class="fa fa-circle-o"></i>Edicion de DISA Puntos</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-group "></i>
                                <span>Fabricantes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Proovedores.aspx"><i class="fa fa-circle-o"></i>Consulta de Fabricantes</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroProovedores.aspx"><i class="fa fa-circle-o"></i>Alta de Fabricantes</a></li>
                            </ul>
                        </li>




                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Ingresar Nueva Region
                    </h1>
                </section>

                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <!-- /.row -->
                            <div class="row">
                                <!-- general form elements -->
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                         <div class="box-header">
                                                <h3 class="box-title">Buscador de Regiones:</h3>
                                                <asp:TextBox ID="txtBuscarRegion" runat="server" Text="" class="form-control" placeholder="Codigo Region"></asp:TextBox>
                                                <br />
                                                <asp:Button ID="btnBuscarRegionEspecifica" runat="server" CssClass="btn btn-success" Text="Buscar Region" />

                                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                            </div>


                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="box-body">
                                                      <div class="form-group">
                                                            <label for="exampleInputEmail1">Codigo de Region</label>
                                                            <br />
                                                            <asp:TextBox ID="txtCodigoRegion" runat="server" placeholder="Codigo Region" CssClass="form-control" Enabled="false"></asp:TextBox>

                                                        </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">RFC</label>
                                                        <asp:TextBox runat="server" ID="txtRFC" placeholder="RFC" CssClass="form-control" />
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Nombre de la Region</label>
                                                        <%--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                        <asp:TextBox runat="server" ID="txtNomrbreRegion" placeholder="Nombre de la Region" CssClass="form-control" />
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Direccion </label>
                                                        <%-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                        <asp:TextBox runat="server" ID="txtDireccion" placeholder="Direccion" CssClass="form-control" />
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Colonia</label>
                                                                <%-- <input type="text" class="form-control" placeholder=".col-xs-3">--%>
                                                                <asp:TextBox runat="server" ID="txtColonia" placeholder="Colonia" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Estado</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstadosDeMex" runat="server" AutoPostBack="false">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Municipio</label>
                                                                <asp:TextBox runat="server" ID="txtMunicipio" placeholder="Municipio" CssClass="form-control" />
                                                            </div>

                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Codigo  Postal</label>
                                                                <asp:TextBox runat="server" ID="txtCodigoPostal" placeholder="Codigo Postal" CssClass="form-control" />
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Numero Telefonico </label>
                                                        <%-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                        <asp:TextBox runat="server" ID="txtNumeroTelefonico" placeholder="Numero Telefonico" CssClass="form-control" />
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">

                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Codigo del Gerente</label>
                                                                <asp:TextBox runat="server" ID="txtCodigoGerente" placeholder="Codigo Gerente" CssClass="form-control" />
                                                            </div>

                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Nombre del Gerente</label>
                                                                <asp:TextBox runat="server" ID="txtNombreDelGerente" placeholder="Nombre Gerente" CssClass="form-control" />
                                                            </div>


                                                        </div>
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">

                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Superfice de Bodega</label>
                                                                <br />
                                                                <asp:TextBox runat="server" ID="txtSuperficeBodega" placeholder="Superficie" CssClass="form-control" />

                                                            </div>

                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Capacidad de Cajas</label>
                                                                <br />
                                                                <asp:TextBox runat="server" ID="txtCapacidadCajas" placeholder="Capacidad de Cajas" CssClass="form-control" />

                                                            </div>


                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Foto de la Bodega</label>
                                                        <asp:FileUpload runat="server" ID="FileUpload1" />
                                                        <br />
                                                        <asp:Label ID="lblFoto" runat="server" Text="" CssClass="label-danger "></asp:Label>
                                                        <br />
                                                         <asp:Button runat="server" ID="btnCambiarFotoRegion" CssClass="btn  btn-success" Text="Actualizar Foto de la Bodega" />
                                                    </div>


                                                </div>
                                                <!-- /.box-body -->

                                                <div class="box-footer">
                                                     <asp:Button runat="server" ID="btnGuardarCambios" class="btn  btn-primary pull-left  btn-lg " Text="Modificar Region" OnClientClick="ConfirmarRegistro()" />
                                                     <asp:Button runat="server" ID="btnEliminarRegion" class="btn btn-danger pull-right col-xs-offset-1 btn-lg " Text="Eliminar Region" OnClick="btnEliminarRegion_Click" />
                                                    <h4>
                                                        <asp:Label runat="server" ID="lblErrorModificaciones" Text="" class=" col-xs-offset-2 label-success  "></asp:Label>
                                                    </h4>

                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnCambiarFotoRegion" />
                                            </Triggers>
                                        </asp:UpdatePanel>




                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <!-- /.box -->
                            </div>
                                <!-- /.row -->

                        </section>
                        <!-- /.content -->
                    </ContentTemplate>


                </asp:UpdatePanel>



            </div>
            <!-- /.content-wrapper -->




            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy;2016 <a href="">Hello World Technologies</a>.</strong> All rights
    reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <!-- /.control-sidebar-menu -->
                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->

                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>