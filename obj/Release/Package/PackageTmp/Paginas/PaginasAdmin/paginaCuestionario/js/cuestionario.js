/******************Variables Globales*********************/
var divAbiertas=document.getElementById('preguntasAbiertas');
var divOpciones=document.getElementById('preguntasOpciones');

divAbiertas.style.display = 'none';
divOpciones.style.display = 'none';

function desplegarFormatoAbierta(){
	divOpciones.style.display = 'none';
	divAbiertas.style.display='block';
}

function desplegarFormatoOpciones(){
	divAbiertas.style.display = 'none';

	var numPreguntas= document.getElementById('numOpciones').value;
	var div='<br> <p class="text-left">Opciones: </p>';
	div=div+'<input type="text" name="numOp" id="numOp" value="'+numPreguntas+'" hidden>';
	for (var i = 1; i <=numPreguntas; i++) {
		div=div+'<input type="text" id="opcion'+i+'" name="opcion'+i+'" placeholder="opcion'+i+'"></input>';
	}
	$("#opciones").html(div);


	divOpciones.style.display='block';
}