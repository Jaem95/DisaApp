<?php 
 header('Content-Type: text/html; charset=UTF-8');
	$serverName = "disa.database.windows.net"; //serverName\instanceName
	$connectionInfo = array( "Database"=>"disa", "UID"=>"DISA", "PWD"=>"asid2016*");
	$conn = sqlsrv_connect( $serverName, $connectionInfo);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Cuestionariossss</title>
   <link type="text/css" href="css/bootstrap.min.css" rel="stylesheet">
   <link type="text/css" href="css/cuestionario.css" rel="stylesheet">
   <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>
<div id="header" class="text-center">
<a href="checkAllAns.php" class="btn btn-default">Checar Respuestas</a>
	<h3 >Cuestionarios Repartidor/Vendedor</h3>      
	  <div class="row">
		  <div class="col-md-offset-2 col-md-4 col-xs-6 text-center">
			  	<button type="button" class="btn-large btn-default btn-lg" onclick="desplegarFormatoAbierta()">
			  		 Pregunta Abierta  <strong> +</strong>
			  	</button>
		  </div>
		  <div class="col-md-4 col-xs-6 text-center">
		  		<button type="button" class="btn-large btn-default btn-lg"  href="#modal1" data-toggle="modal" data-target="#myModal">
			  		Pregunta Opciones  <strong> +</strong> 
			  	</button>
		  </div>
	  </div>
	  
</div>
<!-- *************************Abiertas********************* -->
<!-- *************************Abiertas********************* -->
<!-- *************************Abiertas********************* -->
<div class="container" id="preguntasAbiertas">
	<h5 class="text-center">Preguntas Abiertas</h5>
	<label class="text-left">
	Pregunta: </label>
	<form method="post" action="cuestionario.php" >
	<textarea class="text-area-personal form-control" rows="3" placeholder="Introduce la pregunta " id="textarea-abierta" name="textarea-abierta">
		
	</textarea>

	<button type="submit" class="btn-large text-rigth" name="guardarAbiertas">
		Guardar
	</button>
	</form>
</div>
<!-- *************************Opciones********************* -->
<!-- *************************Opciones********************* -->
<!-- *************************Opciones********************* -->
<div class="container" id="preguntasOpciones">
	<h5 class="text-center">Pregunta Opcional</h5>
	<form method="post" action="cuestionario.php" >
	<label class="text-left">
	Pregunta: </label>
	<input type="text" name="inputPreguntaOpcional" placeholder="Introduce la pregunta">
	<div id="opciones">
		
	</div>
	<button type="submit" name="guardarOpciones" class="btn-large text-rigth">
		Guardar
	</button>
	</form>
</div>
  
 <!-- *************************MODALES********************* -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>¿Cuántas opciones tendra la pregunta?</p>
          <input type="number" name="numOpciones" id="numOpciones" >
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="desplegarFormatoOpciones()">Aceptar</button>
        </div>
      </div>
      
    </div>
  </div>


<footer class="page-footer">
	<div class="container text-center">
	<div class="row">
		<div class="col-md-offset-2 col-md-7 text-center">
			<h5>Preguntas guardadas</h5>
		</div>
		<div class="col-md-3 text-center">
		</div>
	</div>

	<!-- *************************INICIO SQL PREGUNTAS********************* -->
	<table class="text-center striped">
		<thead>
			<th>Pregunta</th>
			<th>Tipo</th>
			<th>Acción</th>
		</thead>
		<tbody>
			
			<?php 
				$sql="select * from Pregunta where estatus=1 ORDER BY id DESC";
				$stmt = sqlsrv_query( $conn, $sql );

		while(( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) || sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)!= null) 
			{
				
			?>
			<tr>
			<form method="post" action="cuestionario.php">
				<input type="text" name="id_p" id="id_p" value="<?php echo $row['id'];?>" hidden>
				<td><?php echo utf8_encode ($row['pregunta']);?></td>
				<td><?php echo $row['tipo'];?></td>
				<td>
					<button type="submit" class="btn" name="eliminar">
						<i class="small material-icons" >delete</i>
					</button>
					
				</td>
			</form>
			</tr>
			<?php 
				if($row['tipo']=='opcion'){
				$idd=$row['id'];
				$sql99="select * from OpcionPregunta where idPregunta=$idd";
				$stmt99 = sqlsrv_query( $conn, $sql99 );
				while(( $row2 = sqlsrv_fetch_array( $stmt99, SQLSRV_FETCH_ASSOC) ) || sqlsrv_fetch_array( $stmt99, SQLSRV_FETCH_ASSOC)!= null) 
				{
					?> 
						<tr style="background-color: #CEF6CE; color:#000000;">
							<td >Opcion: </td>
							<td colspan="2"><?php echo utf8_encode ($row2['opcion']);?> </td>
						</tr>
					<?php
				}
				}
			}?>
			
		</tbody>
	</table>
	<!-- *************************FIN SQL PREGUNTAS************************-->
   </div>
</footer>

<?php 
	/*******************Eliminando las preguntas*********************/
	if (isset($_POST['eliminar'])){
		//echo '<script type="text/javascript">alert("hola");</script>';
		$idEliminar=$_POST['id_p'];
		$sqlUpdate="UPDATE Pregunta SET estatus=2 WHERE id=$idEliminar";
		$stmntu = sqlsrv_query( $conn, $sqlUpdate );
		if( $stmntu === false ) {
		     die( print_r( sqlsrv_errors(), true));
		}
		echo "<meta http-equiv='refresh' content='0'>";
	}
	/*******************Guardando Opciones*********************/
	if (isset($_POST['guardarOpciones'])){

		$value=utf8_encode($_POST['inputPreguntaOpcional']);
		$numTotalOp=$_POST['numOp'];
		$sqlInsert="insert into Pregunta values('$value','opcion',1);";
		$stmnt1 = sqlsrv_query( $conn, $sqlInsert );
		if( $stmnt1 === false ) {
		     die( print_r( sqlsrv_errors(), true));
		}else{
			$id=null;
			$sqlSearchId="select top 1 id from Pregunta  ORDER BY id DESC;";
			$stmnt2 = sqlsrv_query( $conn, $sqlSearchId );
			while(( $row = sqlsrv_fetch_array( $stmnt2, SQLSRV_FETCH_ASSOC) ) || sqlsrv_fetch_array( $stmnt2, SQLSRV_FETCH_ASSOC)!= null) 
			{
				$id=$row[id];
			}
			$sqlInsertOp="";
			for ($i=1; $i <=$numTotalOp ; $i++) { 
				$valOp=utf8_encode($_POST['opcion'.$i]);
				$sqlInsertOp=$sqlInsertOp."insert into OpcionPregunta values('$id','$valOp');";
			}
			$stmnt3 = sqlsrv_query( $conn, $sqlInsertOp );
			if( $stmnt3 === false ) {
			     die( print_r( sqlsrv_errors(), true));
			}
		}
		echo "<meta http-equiv='refresh' content='0'>";
	}
	/*******************Guardando Abiertas*********************/
	if (isset($_POST['guardarAbiertas'])){
		$value=utf8_encode($_POST['textarea-abierta']);
		$sqlInsert="insert into Pregunta values('$value','abierta',1);";
		$stmnt1 = sqlsrv_query( $conn, $sqlInsert );
		if( $stmnt1 === false ) {
		     die( print_r( sqlsrv_errors(), true));
		}
		echo "<meta http-equiv='refresh' content='0'>";
	}
	
?>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/cuestionario.js"></script>
    <script src="js/dp_cuestionario.js"></script>
    <script src="js/materialize.js"></script>
</body>
</html>