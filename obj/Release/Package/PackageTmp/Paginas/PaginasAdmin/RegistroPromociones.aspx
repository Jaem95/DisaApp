﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RegistroPromociones.aspx.vb" Inherits="DISA.RegistroPromociones" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
 <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

 <script type="text/javascript">
     function AbrirModal() {
         alert('Hola');
         $(document).ready(function () {
             $("#btnModal").click(function () {
                 $("#DescuentoRegional").modal();
                 $("#DescuentoRegional").modal();
             });
         });
     }


     function ConfirmarRegistro() {
         var confirm_value = document.createElement("INPUT");
         confirm_value.type = "hidden";
         confirm_value.name = "confirm_value";
         if (confirm("¿Desea Actualizar la Promocion?")) {
             confirm_value.value = "Yes";
         } else {
             confirm_value.value = "No";
         }
         document.forms[0].appendChild(confirm_value);
     }

     function mostrarPrecioGeneral() {
         document.getElementById('btnModalPrecioRegional').style.display = 'none';

         document.getElementById('btnModalRegaloGeneral').style.display = 'none';
         document.getElementById('btnModalRegaloRegional').style.display = 'none';

         document.getElementById('btnModalPrecioGeneral').style.display = 'block';
         

     }
     function mostrarPrecioRegional() {

         document.getElementById('btnModalRegaloGeneral').style.display = 'none';
         document.getElementById('btnModalRegaloRegional').style.display = 'none';

         document.getElementById('btnModalPrecioGeneral').style.display = 'none';
         document.getElementById('btnModalPrecioRegional').style.display = 'block';

     }
     function mostrarRegaloGeneral() {

         document.getElementById('btnModalRegaloRegional').style.display = 'none';
         document.getElementById('btnModalPrecioGeneral').style.display = 'none';
         document.getElementById('btnModalPrecioRegional').style.display = 'none';

         document.getElementById('btnModalRegaloGeneral').style.display = 'block';
      
       
     }
     function mostrarRegaloRegional() {

         document.getElementById('btnModalPrecioGeneral').style.display = 'none';
         document.getElementById('btnModalPrecioRegional').style.display = 'none';

         document.getElementById('btnModalRegaloGeneral').style.display = 'none';
         document.getElementById('btnModalRegaloRegional').style.display = 'block';
     
     }


     function actualizar()
     {
         location.reload();
     }
        
</script>

</head>
<body class="hold-transition skin-blue sidebar-mini" >
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/Paginas/Admin.aspx" class="logo">
                    <span class="logo-mini"><b>DISA</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DISA</b> Managment</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <%--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">--%>
                                    <asp:Image ID="imageUsuario3" runat="server" class="user-image" />
                                    <span class="hidden-xs">
                                        <asp:Label ID="lblNombre2" runat="server" Text="Label"></asp:Label></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <%--<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--%>
                                        <asp:Image ID="imageUsuario2" runat="server" class="img-circle" />
                                        <p>
                                            <asp:Label ID="lblNombre3" runat="server" Text=""></asp:Label>

                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="/Paginas/PaginasComun/Perfil.aspx" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            <asp:Button runat="server" CssClass="btn btn-default btn-flat" Text="Cerrar Sesion" ID="btnCerrarSesion"  OnClick="btnCerrarSesion_Click"/>
          
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
                   <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                            <asp:Image ID="imageUsuario1" runat="server" class="img-circle" />
                        </div>
                        <div class="pull-left info">
                            <p>
                                <asp:Label ID="lblNombre4" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <!-- Sidebar user panel -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menu Principal</li>
                       
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-cubes"></i>
                                <span>Productos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Productos.aspx"><i class="fa fa-circle-o"></i>Productos Generales</a></li>
                                <li><a href="/Paginas/PaginasAdmin/ModificarProductos.aspx"><i class="fa fa-circle-o"></i>Modificacion de Productos</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistrarProducto.aspx"><i class="fa fa-circle-o"></i>Alta de Productos</a></li>
                                <li><a href="/Paginas/PaginasAdmin/Caducidades.aspx"><i class="fa fa-circle-o"></i>Caducidades y Flujo de
                          <br />
                                    Inventarios</a></li>
                                <li><a href="/Paginas/PaginasAdmin/Categorias.aspx"><i class="fa fa-circle-o"></i>Categorias Generales</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroCategorias.aspx"><i class="fa fa-circle-o"></i>Alta de Categorias</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-dollar"></i>
                                <span>Precios</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/PreciosGenerales.aspx"><i class="fa fa-circle-o"></i>Edicion de Precios</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-balance-scale"></i>
                                <span>Promociones</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/RegistroPromociones.aspx"><i class="fa fa-circle-o"></i>Registro de Promociones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/PromocionesPrecios.aspx"><i class="fa fa-circle-o"></i>Promociones de Precios</a></li>
                                <li><a href="/Paginas/PaginasAdmin/PromocionesProductos.aspx"><i class="fa fa-circle-o"></i>Promociones de Productos</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa   fa-file-text"></i>
                                <span>Reportes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de Productos</a></li>
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de Ventas</a></li>
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de DisaPuntos</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-map-marker"></i>
                                <span>Regiones</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Regiones.aspx"><i class="fa fa-circle-o"></i>Consulta de Regiones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroRegiones.aspx"><i class="fa fa-circle-o"></i>Alta de Regiones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/EditarRegiones.aspx"><i class="fa fa-circle-o"></i>Modifacion de Regiones</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-sheqel"></i>
                                <span>DISA-Puntos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Puntos.aspx"><i class="fa fa-circle-o"></i>Edicion de DISA Puntos</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-group "></i>
                                <span>Fabricantes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Proovedores.aspx"><i class="fa fa-circle-o"></i>Consulta de Fabricantes</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroProovedores.aspx"><i class="fa fa-circle-o"></i>Alta de Fabricantes</a></li>
                            </ul>
                        </li>




                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Edicion de Promociones</h1>

                </section>

                <asp:UpdatePanel runat="server"  UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">

                            <!-- SELECT2 EXAMPLE -->
                            <div class="box box-default" align="center">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Informacion de la categoria</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body" align="center">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Producto</label>
                                                <asp:DropDownList ID="ddlProductos" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductos_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label>
                                                    Codigo de Barras
                                                </label>
                                                <asp:TextBox runat="server" placeholder="Descripcion de la categoria " class="form-control" ID="txtProducto"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Descripcion
                                                </label>
                                                <asp:TextBox runat="server" placeholder="Descripcion del producto " class="form-control" ID="txtDescripcion"></asp:TextBox>
                                            </div>

                                            <div class="form-group">
                                                <label>
                                                    Presentacion
                                                </label>
                                                <asp:TextBox runat="server" placeholder="Presentacion " class="form-control" ID="txtPresentacion"></asp:TextBox>
                                            </div>

                                            <div class="form-group">
                                                <label>
                                                    Cantidad por Caja
                                                </label>
                                                <asp:TextBox runat="server" placeholder="Cantidad por caja " class="form-control" ID="txtCantidadCaja"></asp:TextBox>
                                            </div>

                                            <div class="form-group">
                                                <asp:Image ID="imagenCategoria" runat="server" class="profile-user-img img-responsive img-circle" />
                                            </div>
                                            <!-- /.form-group -->
                                            <div class="form-group">
                                                <label>Tipo de Promocion</label>

                                                <asp:DropDownList runat="server" ID="ddlTipoPromocion"  AutoPostBack="true" OnSelectedIndexChanged="ddlTipoPromocion_SelectedIndexChanged" Visible ="false">
                                                    <asp:ListItem Text="-----" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Descuento de Precio" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Regalo de Producto" Value="2"></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                             <div class="form-group">
                                                <label>Tipo Lugar de Promocion </label>

                                                <asp:DropDownList runat="server" ID="ddlTipoLugarPromocion" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoLugarPromocion_SelectedIndexChanged" Visible ="false">
                                                    <asp:ListItem Text="-----" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="General" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Regional" Value="2"></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>



                                            <!-- /.form-group -->
                                        </div>
                                        <!-- /.col -->                                      
                                    </div>
                                    <!-- /.row -->

                                    <div class="row">
                                        <div class="col-md-12 ">
                                            <div class="form-group ">
                                                <button type="button" id="btnModalPrecioGeneral" class="btn btn-info" data-toggle="modal" data-target="#DescuentoPrecioGeneral" style="display: none;">Edicion General Precio</button>
                                                <button type="button" id="btnModalPrecioRegional" class="btn btn-info" data-toggle="modal" data-target="#DescuentoPrecioRegional"style="display: none;">Edicion Regional Precio </button>
                                              
                                                <button type="button" id="btnModalRegaloGeneral" class="btn btn-info" data-toggle="modal" data-target="#RegaloGeneral" style="display: none;"">Edicion General Regalo </button>
                                                 <button type="button" id="btnModalRegaloRegional" class="btn btn-info" data-toggle="modal" data-target="#RegaloRegional" style="display: none;">Edicion Regional Regalo</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->

                                 <!--MODAL  PrecioGeneral-->
                                <div id="DescuentoPrecioGeneral" class="modal fade" role="dialog">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <asp:UpdatePanel  runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Descuento Precio General</h4>
                                                        <br />
                                                        <div class="form-group ">
                                                            <label>Descuento (%) :</label>
                                                            <asp:TextBox runat="server" ID="txtDescuento" placeholder="Descuento  del producto"></asp:TextBox>
                                                            <%-- <button type="button" class="btn" data-toggle="modal" data-target="#DescuentoRegional">Aplicar a Todos</button>--%>
                                                            <asp:Button runat="server" ID="btnCalcularPrecioGeneral" Text="Calcular Descuento" CssClass="btn btn-success" data-controls-modal="exampleModal" data-backdrop="static" data-keyboard="false" OnClick="btnCalcularPrecioGeneral_Click" />
                                                        </div>

                                                    </div>
                                                    <div class="modal-header  form-inline" align="center">
                                                        <div class="form-group ">
                                                            <label>Fecha Inicio:</label>
                                                            <asp:Calendar ID="fechaInicio" runat="server"></asp:Calendar>
                                                        </div> 
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <div class="form-group ">
                                                            <label>Fecha Final:</label>
                                                            <asp:Calendar ID="fechaFinal" runat="server"></asp:Calendar>
                                                        </div>

                                                    </div>
                                                    <div class="modal-header" align="center">
                                                        <div class="form-group ">
                                                            <asp:Label runat="server" ID="lblErrorPrecioGeneral" Text="" CssClass="label-danger"></asp:Label>
                                                        </div>

                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="col-md-12">
                                                            <div class="box">
                                                                <div class="box-body">
                                                                    <table class="table table-bordered">
                                                                        <tr>
                                                                            <th>Cantidad de Productos</th>
                                                                            <th>Precio Regular</th>
                                                                            <th>Precio con Descuento</th>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblR1" Text="Rango 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPR1" Text="Precio Regular 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecioCalculado1" Text="Precio Calculado 1"></asp:Label></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblR2" Text="Rango 2"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPR2" Text="Precio Regular 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecioCalculado2" Text="Precio Calculado 1"></asp:Label></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblR3" Text="Rango 3"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPR3" Text="Precio Regular 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecioCalculado3" Text="Precio Calculado 1"></asp:Label></td>

                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                                <!-- /.box-body -->

                                                            </div>
                                                            <!-- /.box -->
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <div class="modal-footer">
                                                        <asp:Button runat="server" ID="btnActualizarPromo" CssClass="btn btn-info" Text="Aplicar Cambios" OnClientClick="ConfirmarRegistro();" OnClick="btnActualizarPromo_Click" />

                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </div>
                                <!--Fin Modal PrecioGeneral -->

                                <!--MODAL  PrecioRegional-->
                                <div id="DescuentoPrecioRegional" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Descuento Precio Regional</h4>
                                                        <br />
                                                        <div class="form-group ">
                                                            <label>Descuento (%) :</label>
                                                            <asp:TextBox runat="server" ID="txtDescuentoRegional" placeholder="Descuento  del producto"></asp:TextBox>
                                                            <%-- <button type="button" class="btn" data-toggle="modal" data-target="#DescuentoRegional">Aplicar a Todos</button>--%>
                                                            <asp:Button runat="server" ID="btnCalcularPrecioRegional" Text="Calcular Descuento" CssClass="btn btn-success" data-controls-modal="exampleModal" data-backdrop="static" data-keyboard="false" OnClick="btnCalcularPrecioRegional_Click" />
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label>Region a Aplicar Promocion</label>
                                                        <asp:DropDownList ID="ddlRegiones" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRegiones_SelectedIndexChanged">

                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="modal-header  form-inline" align="center">
                                                        <div class="form-group ">
                                                            <label>Fecha Inicio:</label>
                                                            <asp:Calendar ID="fechaInicialPromoRegional" runat="server"></asp:Calendar>
                                                        </div>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <div class="form-group ">
                                                            <label>Fecha Final:</label>
                                                            <asp:Calendar ID="fechaFinalPromoRegional" runat="server"></asp:Calendar>
                                                        </div>

                                                    </div>
                                                    <div class="modal-header" align="center">
                                                        <div class="form-group ">
                                                            <asp:Label runat="server" ID="lblModalErrorRegional" Text="" CssClass="label-danger"></asp:Label>
                                                        </div>

                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="col-md-12">
                                                            <div class="box">
                                                                <div class="box-body">
                                                                    <table class="table table-bordered">
                                                                        <tr>
                                                                            <th>Cantidad de Productos</th>
                                                                            <th>Precio Regular</th>
                                                                            <th>Precio con Descuento</th>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblRango1Regional" Text="Rango 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecio1Regional" Text="Precio Regular 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecioRegionalCalculado" Text="Precio Calculado 1"></asp:Label></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblRango2Regional" Text="Rango 2"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecio2Regional" Text="Precio Regular 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecioRegionalCalculado2" Text="Precio Calculado 1"></asp:Label></td>

                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblRango3Regional" Text="Rango 3"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecio3Regional" Text="Precio Regular 1"></asp:Label></td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblPrecioRegionalCalculado3" Text="Precio Calculado 1"></asp:Label></td>

                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                                <!-- /.box-body -->

                                                            </div>
                                                            <!-- /.box -->
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <div class="modal-footer">
                                                        <asp:Button runat="server" ID="btnActualizarPromoRegional" CssClass="btn btn-info" Text="Aplicar Cambios" OnClientClick="ConfirmarRegistro();"  OnClick="btnActualizarPromoRegional_Click"/>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <!--Fin Modal PrecioRegional  -->

                                 <!--MODAL  RegaloGeneral-->
                                <div id="RegaloGeneral" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Regalo de Producto General</h4>
                                                        <br />
                                                        <div class="form-group ">
                                                            <label>Cantiad Minma de Compra:</label>
                                                            <asp:TextBox runat="server" ID="txtCantidadMinimaGeneral" placeholder="$"></asp:TextBox>
                                                            <%-- <button type="button" class="btn" data-toggle="modal" data-target="#DescuentoRegional">Aplicar a Todos</button>--%>
                                                            <%--<asp:Button runat="server" ID="Button1" Text="Calcular Descuento" CssClass="btn btn-success" data-controls-modal="exampleModal" data-backdrop="static" data-keyboard="false" OnClick="btnCalcularPrecioRegional_Click" />--%>
                                                        </div>

                                                    </div>
                                                  
                                                    <div class="modal-header  form-inline" align="center">
                                                        <div class="form-group ">
                                                            <label>Fecha Inicio:</label>
                                                            <asp:Calendar ID="calendarioInicialPromoRegaloGeneral" runat="server"></asp:Calendar>
                                                        </div>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <div class="form-group ">
                                                            <label>Fecha Final:</label>
                                                            <asp:Calendar ID="calendarioFinalPromoRegaloGeneral" runat="server"></asp:Calendar>
                                                        </div>

                                                    </div>
                                                    <div class="modal-header" align="center">
                                                        <div class="form-group ">
                                                            <asp:Label runat="server" ID="lblRegaloGeneral" Text="" CssClass="label-danger"></asp:Label>
                                                        </div>

                                                    </div>
                                                     <div class="modal-header" align="center">
                                                        <div class="form-group ">
                                                            <label>Producto a Regalar:</label>
                                                            <asp:DropDownList ID="ddlProductoRegaloGeneral" runat="server" >
                                                            </asp:DropDownList>
                                                        </div>

                                                          <div class="form-group ">
                                                            <label>Cantidad:</label>
                                                            <asp:TextBox runat="server" id="txtCantidadRegaloGeneral" placeholder="Cantidad a Regalar"></asp:TextBox>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <asp:Button runat="server" ID="btnRegistrarPromoRegaloGeneral" CssClass="btn btn-info" Text="Aplicar Cambios" OnClientClick="ConfirmarRegistro();"  OnClick="btnRegistrarPromoRegaloGeneral_Click"/>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <!--Fin Modal RegaloREgional  -->

                                <!--MODAL  RegaloRegional-->
                                <div id="RegaloRegional" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Regalo de Producto por Region</h4>
                                                        <br />
                                                        <div class="form-group ">
                                                            <label>Cantiad Minma de Compra:</label>
                                                            <asp:TextBox runat="server" ID="txtCantidadMinimaRegional" placeholder="$"></asp:TextBox>
                                                            <%-- <button type="button" class="btn" data-toggle="modal" data-target="#DescuentoRegional">Aplicar a Todos</button>--%>
                                                            <%--<asp:Button runat="server" ID="Button1" Text="Calcular Descuento" CssClass="btn btn-success" data-controls-modal="exampleModal" data-backdrop="static" data-keyboard="false" OnClick="btnCalcularPrecioRegional_Click" />--%>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label>Region a Aplicar Promocion</label>
                                                        <asp:DropDownList ID="ddlRegionesRegalo" runat="server" >
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="modal-header  form-inline" align="center">
                                                        <div class="form-group ">
                                                            <label>Fecha Inicio:</label>
                                                            <asp:Calendar ID="calendarioInicialRegaloRegional" runat="server"></asp:Calendar>
                                                        </div>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <div class="form-group ">
                                                            <label>Fecha Final:</label>
                                                            <asp:Calendar ID="calendarioFinalRegaloRegional" runat="server"></asp:Calendar>
                                                        </div>

                                                    </div>
                                                    <div class="modal-header" align="center">
                                                        <div class="form-group ">
                                                            <asp:Label runat="server" ID="lblRegaloRegional" Text="" CssClass="label-danger"></asp:Label>
                                                        </div>

                                                    </div>
                                                     <div class="modal-header" align="center">
                                                        <div class="form-group ">
                                                            <label>Producto a Regalar:</label>
                                                            <asp:DropDownList ID="ddlproductoRegaloRegional" runat="server" >
                                                            </asp:DropDownList>
                                                        </div>

                                                          <div class="form-group ">
                                                            <label>Cantidad:</label>
                                                            <asp:TextBox runat="server" id="txtCantidadRegalo" placeholder="Cantidad a Regalar"></asp:TextBox>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <asp:Button runat="server" ID="btnRegistrarPromoRegaloRegional" CssClass="btn btn-info" Text="Aplicar Cambios" OnClientClick="ConfirmarRegistro();"  OnClick="btnRegistrarPromoRegaloRegional_Click"/>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <!--Fin Modal RegaloREgional  -->

                            </div>
                            <!-- /.box -->
                        </section>
                        <!-- /.content -->
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy;2016 <a href="">Hello World Technologies</a>.</strong> All rights
    reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <!-- /.control-sidebar-menu -->
                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->

                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
    </form>
   
    <!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

</body>
</html>

