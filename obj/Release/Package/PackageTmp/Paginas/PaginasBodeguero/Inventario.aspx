﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Inventario.aspx.vb" Inherits="DISA.Inventario" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
 <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css" />
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css" />
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css" />

   <!-- Load the float right --> 
  <link rel="stylesheet" href="../../dist/css/skins/my_style.css" />

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" >
    <form id="form1" runat="server"> <asp:ScriptManager runat="server"></asp:ScriptManager>
       
            <div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        <span class="logo-mini"><b>DISA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>DISA</b> Managment</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <%--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">--%>
                <asp:Image ID="imageUsuario3" runat="server" class="user-image" />
              <span class="hidden-xs">
                  <asp:Label ID="lblNombre2" runat="server" Text="Label"></asp:Label></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <%--<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--%>
                 <asp:Image ID="imageUsuario2" runat="server" class="img-circle" />
                <p>
                    <asp:Label ID="lblNombre3" runat="server" Text=""></asp:Label>
                  
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="/Paginas/PaginasComun/Perfil.aspx" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Cerrar Sesion</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
          <div class="pull-left image">

              <asp:Image ID="imageUsuario1" runat="server" class="img-circle" />
          </div>
          <div class="pull-left info">
              <p>
                  <asp:Label ID="lblNombre4" runat="server" Text=""></asp:Label>
              </p>
          </div>
      </div>
     
      <!-- Sidebar user panel -->
                         
                          <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Menu Principal</li>
            <li class="treeview">
                <a href="/Paginas/PaginasBodeguero/PedidosPendientes.aspx">
                    <!-- This was used for the image <i class="fa fa-user"></i> -->
                    <i class="fa fa-circle-o"></i>
                    <span>Pedidos</span>
                </a>
            </li>
            <li class="treeview">
                <a href="/Paginas/PaginasBodeguero/Devoluciones.aspx">
                    <!--<i class="fa fa-cubes"></i>-->
                    <i class="fa fa-circle-o"></i>
                    <span>Devoluciones</span>
                </a>
            </li>

            <li class="treeview">
                <a href="/Paginas/PaginasBodeguero/Inventario.aspx">
                    <!--<i class="fa  fa-dollar"></i>-->
                    <i class="fa fa-circle-o"></i>
                    <span>Inventario</span>
                </a>
            </li>

            <li class="treeview">
                <a href="/Paginas/PaginasBodeguero/ValidarManager.aspx">
                    <i class="fa fa-circle-o"></i>
                    <span>Conteo</span>
                </a>
            </li>
        </ul>
                        </section>
                        <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Inventario
      </h1>
    </section>

        <asp:UpdatePanel runat="server">
          <ContentTemplate >
           <!-- Main content -->
                <section class="content">
                <div class="row">
                <div class="col-xs-12">
          

                <div class="box">
                <div class="box-header">

                    <div class="box-header">
                        <div>
                            <div>
                                <asp:TextBox ID="txtBuscarProducto" runat="server" Text=""  class="form-control" Width="30%" placeholder="Codigo Disa"  ></asp:TextBox>
                                <asp:Button ID="btnBuscarProducto" runat="server" Text ="Buscar Producto"  CssClass="btn btn-info"/>
                                <h4><asp:Label ID="lblError" runat="server" Text="" CssClass="label label-danger" class="float-right"></asp:Label></h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                  

                </div>


            <div class="box-header">

                <div class="box-header">
                    <h3 class="box-title">Ordenar por:</h3> <asp:DropDownList ID="DropDownList1" runat="server"  AutoPostBack="True">
                                                                <asp:ListItem Value="CodigoDisa"> Codigo Disa </asp:ListItem>
                                                                <asp:ListItem Value="CodigoBarras"> Codigo de Barras </asp:ListItem>
                                                                <asp:ListItem Value="NombreProducto"> Nombre </asp:ListItem>
                                                            </asp:DropDownList>
                </div>

            </div>
              
                <!-- /.box-header -->
                <div class="box-body">
                <table id="example1" class="table table-bordered table-striped table-hover">

                       <asp:GridView ID="Tabla1" runat="server"   class="table table-bordered table-striped"  AutoGenerateColumns="false">
                                <Columns>
                                    <asp:BoundField DataField="codigoDisa"  HeaderStyle-HorizontalAlign="Center" HeaderText="Codigo Disa"   /> 
                                    <asp:BoundField DataField="codigoBarras"  HeaderStyle-HorizontalAlign="Center" HeaderText="Codigo de Barras"  /> 
                                    <asp:BoundField DataField="nombre"  HeaderStyle-HorizontalAlign="Center" HeaderText="Producto"   /> 
                                    <asp:BoundField DataField="caducidad"  HeaderStyle-HorizontalAlign="Center" HeaderText="Caducidad"   /> 
                                    <asp:BoundField DataField="cantidad"  HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad"   />
                                </Columns>
                        </asp:GridView>

                </table>
        
                </div>
                <!-- /.box-body -->
                </div>
                <!-- /.box -->
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
                </section>
           <!-- /.content -->
           </ContentTemplate>
          

         </asp:UpdatePanel>   
  


  </div>
  <!-- /.content-wrapper -->


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy;2016 <a href=""> Hello World Technologies</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <!-- /.control-sidebar-menu -->
      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
    </form>
   
    <!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<script>
    var d = new Date();
    var mon = d.getMonth();
    var day = d.getDate();
    var year = d.getFullYear();
    document.getElementById("display-fecha").innerHTML ="Fecha(dd/mm/aaaa): " + day + "/" + mon + "/" + year;
</script>

</body>
</html>
