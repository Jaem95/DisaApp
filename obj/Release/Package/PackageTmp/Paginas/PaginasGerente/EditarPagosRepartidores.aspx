﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditarPagosRepartidores.aspx.vb" Inherits="DISA.EditarPagosRepartidores" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {
       
        window.parent.location.href = window.parent.location.href;
    }
</script>  
<script type="text/javascript">
    function ConfirmarRegistro() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("¿Desea realizar este registro?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>  
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content" >
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding:0px 15px 0 15px;">
                    <h1>Corte de Repartidores
                    </h1>
                </section>

                  <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">


                                    <div class="box">
                                        <div class="box-header">

                                            <div class="box-header">
                                                <h3 class="box-title">ID de Repartidor:  <asp:Label ID="lblNombreRepartidor" runat="server" Text=" A0002" ></asp:Label> </h3>    <h3 class="box-title  pull-right "><asp:Label  runat ="server" CssClass="pull-right " ID="lblFecha"></asp:Label> </h3>
                                                <br />
                                                 <h3 class="box-title">Nombre del Repartidor:  <asp:Label ID="Label1" runat="server" Text=" Fulano tal" ></asp:Label> </h3>  
                                            </div>


                                        </div>

                                        <!-- /.box-header -->
                                        <div class="box-body"   >

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" align="center">
                                                        <h4 class="box-title"><label class="label-info ">Entrega de Ficha</label></h4>
                                                    </div>

                                                    <div class="form-group">


                                                      <%--  <div class="col-sm-3">
                                                            <label>Numero de Ficha:</label>
                                                            <asp:TextBox runat="server" ID="txtNumeroFicha" CssClass="form-control " TextMode="Number"></asp:TextBox>
                                                            <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtNumeroFicha" ForeColor="Red" Text="De"></asp:RequiredFieldValidator>
                                                        </div>--%>


                                                        <div class="col-sm-3 ">
                                                            <label>Cantidad a ingresar </label>
                                                            <asp:TextBox runat="server" ID="txtCantidadAIngresar" CssClass="form-control " TextMode="Number" OnTextChanged="txtCantidadAIngresar_TextChanged" AutoPostBack="true"  CausesValidation="false  "></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCantidadAIngresar"  ForeColor="Red"  Text="Debe de ingresar una cantidad" ValidationGroup="One"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>Cantidad  Esperada </label>
                                                            <asp:TextBox runat="server" ID="txtCantidadEsperada" CssClass="form-control " TextMode="Number" OnTextChanged="txtCantidadAIngresar_TextChanged" AutoPostBack="true" CausesValidation="false" ></asp:TextBox>
                                                             <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCantidadEsperada"  ForeColor="Red"  Text="Debe de ingresar una cantidad" ValidationGroup="One"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-sm-3 ">
                                                            <label>Diferencia </label>
                                                            <asp:TextBox runat="server" ID="txtDiferencia" CssClass="form-control " TextMode="Number" Enabled="false"></asp:TextBox>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>Imagen de la ficha</label>
                                                            <asp:FileUpload runat="server" ID="fuImagenFicha"  Enabled="false" />
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="fuImagenFicha" ForeColor="Red" Text="Tienes que ingresar una imagen"></asp:RequiredFieldValidator>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                             <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" align="center">
                                                        <div class=" col-sm-offset-4 col-sm-4">
                                                            <br />
                                                              <asp:Button  runat="server"  Text="Confirmar Pago de Ficha" ID="btnPagoFicha" CssClass="btn btn-default" ValidationGroup="One"/>
                                                        </div>
                                                      
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" align="center">
                                                        <h4 class="box-title"><label class="label-success ">Entrega de Caja(Efectivo)</label></h4>
                                                    </div>

                                                    <div class="form-group">


                                                        <div class="col-sm-4">
                                                            <label>Cantidad a Entregar:</label>
                                                            <asp:TextBox runat="server" ID="txtCantidadAEntregarEfectivi" CssClass="form-control " TextMode="Number"  AutoPostBack="true" OnTextChanged="txtCantidadAEntregarEfectivi_TextChanged" ValidationGroup="Two"></asp:TextBox>
                                                            <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtCantidadAEntregarEfectivi" ForeColor="Red" ValidationGroup="Two" Text="Debes de ingresar una cantidad"></asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <label>Cantidad Esperada:</label>
                                                           <asp:TextBox runat="server" ID="txtCantidadEfectivoEsperada" CssClass="form-control " TextMode="Number" OnTextChanged="txtCantidadAEntregarEfectivi_TextChanged" AutoPostBack="true" ValidationGroup="Two"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCantidadEfectivoEsperada" ForeColor="Red" ValidationGroup="Two" Text="Debes de ingresar un cantidad"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-sm-4 ">
                                                            <label>Diferencia </label>
                                                            <asp:TextBox runat="server" ID="txtDiferenciaEfectivo" CssClass="form-control " TextMode="Number" Enabled="false" ValidationGroup="Two"></asp:TextBox>
                                                        </div>
                                                       

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" align="center">
                                                        <div class=" col-sm-offset-4 col-sm-4">
                                                            <br />
                                                              <asp:Button  runat="server"  Text="Confirmar Pago de Caja" ID="btnPagoCaja" CssClass="btn btn-default " ValidationGroup="Two"/>
                                                        </div>
                                                      
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </ContentTemplate>
                      <Triggers>
                          <asp:PostBackTrigger ControlID="btnPagoFicha" />
                          <asp:PostBackTrigger ControlID="btnPagoCaja" />
                      </Triggers>

                </asp:UpdatePanel>

              



            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>