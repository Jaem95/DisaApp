﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RegistrarProducto.aspx.vb" Inherits="DISA.RegistrarProducto1" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {
       
        window.parent.location.href = window.parent.location.href;
    }
</script>  
<script type="text/javascript">
    function ConfirmarRegistro() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("¿Desea realizar este registro?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>  
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content" >
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding:0px 15px 0 15px;">
                    <h1>Registro de Productos
                    </h1>
                </section>

                  <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <div class="box-header">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group ">
                                                        <h4><label>Numero de Nota</label></h4>
                                                        <asp:TextBox runat="server" ID="txtNota"  CssClass="form-control"  placeholder="Numero de Nota"></asp:TextBox>
                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtNota" BorderColor="Red"  ForeColor="red" Text="No se permite campos vacios" ></asp:RequiredFieldValidator>
                                                    </div>

                                                </div>
                                                <%--<div class="col-md-4">
                                                    <div class="form-group ">
                                                         <h4><label>Producto</label></h4>
                                                        <asp:DropDownList runat="server" ID="ddlProductos" DataSourceID="DSProductos" DataTextField="nombre" DataValueField="codigo_barrras" CssClass="form-control ">
                                                        </asp:DropDownList>
                                                        <asp:SqlDataSource runat="server" ID="DSProductos" ConnectionString='<%$ ConnectionStrings:DB %>'></asp:SqlDataSource>
                                                    </div>
                                                </div>--%>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <h4><label>Fabricante</label></h4>
                                                        <asp:DropDownList runat="server" ID="ddlProovedores" DataSourceID="DSProveedores" DataTextField="nombre" DataValueField="id" CssClass="form-control">
                                                        </asp:DropDownList>

                                                        <asp:SqlDataSource runat="server" ID="DSProveedores" ConnectionString='<%$ ConnectionStrings:DB %>' SelectCommand="SELECT [id], [nombre] FROM [proovedor] WHERE ([estatus] = 1)">
                                                        
                                                        </asp:SqlDataSource>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="form-inline">
                                                    <div class="col-md-3">
                                                        <h4><label>Buscador de Producto</label></h4>
                                                        <asp:TextBox runat="server" ID="txtBuscarProducto" CssClass="form-control" Width="100%" placeholder="Codigo Barras"></asp:TextBox>
                                                                      <br />
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtBuscarProducto" BorderColor="Red" ForeColor="Red" Text="No se permiten campos vacios" ValidationGroup="One"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <h4><label>Fecha Caducidad:</label></h4>
                                                        <asp:TextBox runat="server" ID="txtFechaCaducidad" CssClass="form-control" Width="100%" placeholder="Fecha Caducidad" TextMode="Date" ></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtFechaCaducidad" Text="Debes de ingresar una fecha" ValidationGroup="One" ForeColor="Red" ></asp:RequiredFieldValidator>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <h4><label>Cantidad Recibida:</label></h4>
                                                        <asp:TextBox runat="server" ID="txtCantidadRecibida" CssClass="form-control" Width="100%" placeholder="Cantidad"  TextMode="Number" ></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator  runat="server" ControlToValidate="txtCantidadRecibida" Text="Debes de ingresar una numero" ValidationGroup="One" ForeColor="Red" ></asp:RequiredFieldValidator>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <br />
                                                        <br />
                                                         <asp:Button runat="server" ID="btnBuscarProducto" CssClass="form-control" Text="Buscar Producto" class="btn-success " ValidationGroup="One" />
                                          

                                                    </div>

        


                                                </div>

                                            </div>

                                        </div>

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                             <div class="row">
                                                 <div class="col-md-12">
                                                      <table id="example1" class="table table-bordered table-striped table-hover">
                                                         <asp:GridView runat="server" ID="TablaProductos" class="table table-bordered table-striped" AutoGenerateColumns="false">
                                                             <Columns>
                                                                 <asp:BoundField DataField="CodigoBarras" HeaderStyle-HorizontalAlign="Center" HeaderText="Codigo Barras" />
                                                                  <asp:BoundField DataField="Producto" HeaderStyle-HorizontalAlign="Center" HeaderText="Producto" />
                                                                  <asp:BoundField DataField="Cantidad" HeaderStyle-HorizontalAlign="Center" HeaderText="Cantidad Caja Barras" />
                                                                  <asp:BoundField DataField="Caducidad" HeaderStyle-HorizontalAlign="Center" HeaderText="Caducidad" />

                                                                 <asp:CommandField DeleteText="" SelectText="Eliminar" ShowSelectButton="True" />
                                                             </Columns>


                                                         </asp:GridView>
                                                     </table>
                                                  
                                                </div>


                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                      <asp:Button runat="server" ID="btnRegistrarProductos" Text="Ingresar productos al inventario" CssClass="form-control  btn-info" Enabled="false" />

                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </ContentTemplate>


                </asp:UpdatePanel>

              



            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>