﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RecordProductos.aspx.vb" Inherits="DISA.RecordProductos" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=0.5, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {
       
        window.parent.location.href = window.parent.location.href;
    }
</script>  
<script type="text/javascript">
    function ConfirmarRegistro() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("¿Desea realizar este registro?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>  
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content" >
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding:0px 15px 0 15px;">
                    <h1>Record de Productos
                    </h1>
                </section>

                 <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">


                                    <div class="box">
                                        <div class="box-header">

                                            <div class="box-header">

                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group ">
                                                                <h3 class="box-title">Buscador de Productos:</h3>
                                                                <asp:TextBox ID="txtProducto" runat="server" Text="" class="form-control" placeholder="Producto"></asp:TextBox>
                                                                <br />
                                                                <asp:Button ID="btnBuscar" runat="server" Text="Buscar Producto" CssClass="btn btn-info" OnClick="btnBuscarRecord_Click1" />
                                                                <br />
                                                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                                            </div>
                                                        </div>
                             

                                                          <div class="col-md-4">
                                                            <div class="form-group ">
                                                                  <h3 class="box-title">Ordenar por:</h3>
                                                                <br />
                                                                <h3><label for="selectOrder">Ordenar Por:</label></h3>
                                                                <asp:DropDownList runat="server" ID="selectOrder" AutoPostBack="true" OnSelectedIndexChanged="selectOrder_SelectedIndexChanged" CssClass="form-control ">
                                                                    <asp:ListItem Value="categoria">Categoria</asp:ListItem>
                                                                    <asp:ListItem Value="proveedor">Fabricante</asp:ListItem>
                                                                    <asp:ListItem Value="cantidad">Cantidad</asp:ListItem>
                                                                    

                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                              

                                               
                                            </div>


                                        </div>

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped table-hover">
                                                <asp:GridView ID="Tabla1" runat="server" class="table table-bordered table-striped" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="codigoDisa" HeaderText="Codigo Disa" ReadOnly="True" SortExpression="codigoDisa"></asp:BoundField>
                                                        <asp:BoundField DataField="producto" HeaderText="Producto" ReadOnly="True" SortExpression="producto"></asp:BoundField>
                                                        <asp:BoundField DataField="cantidad" HeaderText="Cantidad" ReadOnly="True" SortExpression="cantidad"></asp:BoundField>
                                                        <asp:BoundField DataField="categoria" HeaderText="Categoria" ReadOnly="True" SortExpression="categoria"></asp:BoundField>
                                                        <asp:BoundField DataField="proveedor" HeaderText="Proveedor" ReadOnly="True" SortExpression="proveedor"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </table>

                                        </div>
                    
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </ContentTemplate>


                </asp:UpdatePanel>

              



            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>

