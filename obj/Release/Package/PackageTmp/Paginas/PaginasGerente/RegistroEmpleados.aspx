﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RegistroEmpleados.aspx.vb" Inherits="DISA.RegistroEmpleados" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {
       
        window.parent.location.href = window.parent.location.href;
    }
</script>  
<script type="text/javascript">
    function ConfirmarRegistro() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("¿Desea realizar este registro?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>  
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content" >
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding:0px 15px 0 15px;">
                    <h1>Registro Empleados
                    </h1>
                </section>

                <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <!-- /.row -->
                            <div class="row">
                                <!-- general form elements -->
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="box-body">

                                                    <div class="form-group ">

                                                        <div class="row">
                                                            <div class=" col-md-6">
                                                                <label for="exampleInputEmail1">Nombre de la Empleado:</label>
                                                                <asp:TextBox runat="server" ID="txtNombreDelEmpleado" placeholder="Nombres" CssClass="form-control" />
                                                            </div>
                                                            <div class=" col-md-6">
                                                                <label for="exampleInputEmail1">Apellidos del Empleado</label>
                                                              
                                                                <asp:TextBox runat="server" ID="txtApellidosEmpleado" placeholder="Apellidos" CssClass="form-control" />
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="form-inline">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label for="exampleInputEmail1">Fecha de Nacimiento:</label>
                                                                <asp:TextBox runat="server" ID="txtDia" placeholder="DD" CssClass="form-control" Width="50px" />
                                                                /
                                                                 <asp:TextBox runat="server" ID="txtMes" placeholder="MM" CssClass="form-control" Width="50px" />
                                                                /
                                                                 <asp:TextBox runat="server" ID="txtAnio" placeholder="YYYY" CssClass="form-control" Width="55px" />
                                                            </div>
                                                             <div class="col-md-4">
                                                                <label for="exampleInputEmail1">CURP </label>

                                                                <asp:TextBox runat="server" ID="txtCurp" placeholder="CURP" CssClass="form-control" Width="80%" />
                                                            </div>
                                                             <div class="col-md-4">
                                                                <label for="exampleInputEmail1">RFC </label>

                                                                <asp:TextBox runat="server" ID="txtRFC" placeholder="RFC" CssClass="form-control"   Width="80%" />
                                                            </div>

                                                        </div>
                                   
                                                           
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Direccion </label>
                                                        <asp:TextBox runat="server" ID="txtDireccion" placeholder="Direccion" CssClass="form-control" />
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Estado Civil</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstadoCivil" runat="server" AutoPostBack="false" Width="100%">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Estado</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstadosDeMex" runat="server" AutoPostBack="false" Width="100%" >
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Ultimos Estudios</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstudios" runat="server" AutoPostBack="false" Width="100%" >
                                                                    
                                                                </asp:DropDownList>
                                                            </div>
                                                               <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Sexo</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlSexo" runat="server" AutoPostBack="false" Width="100%" >
                                                                   
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class=" row">
                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Telefono </label>
                                                                <%-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                                <asp:TextBox runat="server" ID="txtNumeroTelefonico" placeholder="Numero Telefonico" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Celular</label>
                                                                <%-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                                <asp:TextBox runat="server" ID="txtNumeroTelefonico2" placeholder="Numero Telefonico" CssClass="form-control" />

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">
                                                             <div class="col-xs-4">
                                                                <label for="exampleInputEmail1">Correo Electronico</label>
                                                                <asp:TextBox runat="server" ID="txtCorreoElectronico" placeholder="Correo Electronico" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <label for="exampleInputEmail1">Region</label>
                                                                  <br />
                                                                <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="false" Width="100%" >
                                                                </asp:DropDownList>
                                                            </div>
                                                              <div class="col-xs-4">
                                                                <label for="exampleInputEmail1">Puesto al que aplica</label>
                                                                  <br />
                                                                <asp:DropDownList ID="ddlPuesto" runat="server" AutoPostBack="false" Width="100%" >
                                                                    <asp:ListItem>Vendedor</asp:ListItem>
                                                                    <asp:ListItem>Bodeguero</asp:ListItem>
                                                                    <asp:ListItem>Repartidor</asp:ListItem>
                                                                    <asp:ListItem>Chofer</asp:ListItem>
                                                                    <asp:ListItem>Gerente</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                           


                                                        </div>
                                                    </div>
                                                      <div class="form-group">
                                                        <label>Foto del Trabajador</label>
                                                        <asp:FileUpload runat="server" ID="FileUpload1" />
                                                        <br />
                                                        <asp:Label ID="lblFoto" runat="server" Text="" CssClass="label-danger "></asp:Label>
                                                        <br />
                                                        <%-- <asp:Button runat="server" ID="btnCambiarFotoProducto" CssClass="btn  btn-success" Text="Actualizar Foto del Producto" />--%>
                                                    </div>

                                                 
                                                    <!-- /.box-body -->

                                                    <div class="box-footer">
                                                        <asp:Button runat="server" ID="btnGuardarCambios" class="btn  btn-primary pull-left" Text="Registrar Empleado" OnClientClick="ConfirmarRegistro()" OnClick="btnGuardarCambios_Click" />
                                                        <%-- <asp:Button runat="server" ID="btnEliminarRegion" class="btn btn-danger pull-right col-xs-offset-1 btn-lg " Text="Eliminar Producto" />--%>
                                                        <h4>
                                                            <asp:Label runat="server" ID="lblErrorModificaciones" Text="" class=" col-xs-offset-2 label-success  "></asp:Label>
                                                        </h4>

                                                    </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <!-- /.box -->
                            </div>
                                <!-- /.row -->

                        </section>
                        <!-- /.content -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnGuardarCambios" />
                    </Triggers>
                </asp:UpdatePanel>



            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>
