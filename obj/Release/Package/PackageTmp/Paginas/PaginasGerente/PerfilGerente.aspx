﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PerfilGerente.aspx.vb" Inherits="DISA.PerfilGerente"  EnableEventValidation="false"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {
       
        window.parent.location.href = window.parent.location.href;
    }
</script>    
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content" >
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding:0px 15px 0 15px;">
                    <h1>Perfil
                    </h1>
                </section>

                <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">

                            <div class="row">
                                <div class="col-md-3">

                                    <!-- Profile Image -->
                                    <div class="box box-primary">
                                        <div class="box-body box-profile">
                                            <%--<img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">--%>
                                            <asp:Image ID="imagenPerfil" runat="server" class="profile-user-img img-responsive img-circle" />

                                            <h3 class="profile-username text-center">
                                                <asp:Label ID="lblNombre1" runat="server" Text=""></asp:Label>

                                            </h3>

                                            <p class="text-muted text-center">
                                                <asp:Label ID="lblPuesto" runat="server" Text=""></asp:Label>
                                            </p>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->

                                    <!-- About Me Box -->
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Informacion de Usuario</h3>
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <strong><i class="fa fa-book margin-r-5"></i>Usuario DISA</strong>

                                            <p class="text-muted">
                                                <asp:Label ID="lblUsuarioDISA" runat="server" Text=""></asp:Label>
                                            </p>

                                            <hr>

                                            <strong><i class="fa fa-map-marker margin-r-5"></i>Ubicacion</strong>

                                            <p class="text-muted">
                                                <asp:Label ID="lblUbicacion" runat="server" Text=""></asp:Label>
                                            </p>

                                            <hr>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->

                                <div class="col-md-9">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active" id="liActivity"><a href="#activity" data-toggle="tab">Informacion Personal</a></li>
                                            <li id="liBusqueda"><a href="#busqueda" data-toggle="tab">Busqueda de  Personal</a></li>
                                            <%--<li><a href="#timeline" data-toggle="tab">Timeline</a></li>--%>
                                            <%-- <li><a href="#settings" data-toggle="tab">Informacion Personal 2</a></li>--%>
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane" id="settings">
                                                <!-- Post -->
                                                <div class="post">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                                                        <span class="username">
                                                            <a href="#">Jonathan Burke Jr.</a>
                                                            <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                                        </span>
                                                        <span class="description">Shared publicly - 7:30 PM today</span>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <p>
                                                        Lorem ipsum represents a long-held tradition for designers,
                    typographers and the like. Some people hate it and argue for
                    its demise, but others ignore the hate as they create awesome
                    tools to help create filler text for everyone from bacon lovers
                    to Charlie Sheen fans.
                                                    </p>
                                                    <ul class="list-inline">
                                                        <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i>Share</a></li>
                                                        <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i>Like</a>
                                                        </li>
                                                        <li class="pull-right">
                                                            <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i>Comments
                        (5)</a></li>
                                                    </ul>

                                                    <input class="form-control input-sm" type="text" placeholder="Type a comment">
                                                </div>
                                                <!-- /.post -->

                                                <!-- Post -->
                                                <div class="post clearfix">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="../../dist/img/user7-128x128.jpg" alt="User Image">
                                                        <span class="username">
                                                            <a href="#">Sarah Ross</a>
                                                            <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                                        </span>
                                                        <span class="description">Sent you a message - 3 days ago</span>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <p>
                                                        Lorem ipsum represents a long-held tradition for designers,
                    typographers and the like. Some people hate it and argue for
                    its demise, but others ignore the hate as they create awesome
                    tools to help create filler text for everyone from bacon lovers
                    to Charlie Sheen fans.
                                                    </p>

                                                    <form class="form-horizontal">
                                                        <div class="form-group margin-bottom-none">
                                                            <div class="col-sm-9">
                                                                <input class="form-control input-sm" placeholder="Response">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <button type="submit" class="btn btn-danger pull-right btn-block btn-sm">Send</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- /.post -->

                                                <!-- Post -->
                                                <div class="post">
                                                    <div class="user-block">
                                                        <img class="img-circle img-bordered-sm" src="../../dist/img/user6-128x128.jpg" alt="User Image">
                                                        <span class="username">
                                                            <a href="#">Adam Jones</a>
                                                            <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                                                        </span>
                                                        <span class="description">Posted 5 photos - 5 days ago</span>
                                                    </div>
                                                    <!-- /.user-block -->
                                                    <div class="row margin-bottom">
                                                        <div class="col-sm-6">
                                                            <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
                                                        </div>
                                                        <!-- /.col -->
                                                        <div class="col-sm-6">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <img class="img-responsive" src="../../dist/img/photo2.png" alt="Photo">
                                                                    <br>
                                                                    <img class="img-responsive" src="../../dist/img/photo3.jpg" alt="Photo">
                                                                </div>
                                                                <!-- /.col -->
                                                                <div class="col-sm-6">
                                                                    <img class="img-responsive" src="../../dist/img/photo4.jpg" alt="Photo">
                                                                    <br>
                                                                    <img class="img-responsive" src="../../dist/img/photo1.png" alt="Photo">
                                                                </div>
                                                                <!-- /.col -->
                                                            </div>
                                                            <!-- /.row -->
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                    <!-- /.row -->

                                                    <ul class="list-inline">
                                                        <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i>Share</a></li>
                                                        <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i>Like</a>
                                                        </li>
                                                        <li class="pull-right">
                                                            <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i>Comments
                        (5)</a></li>
                                                    </ul>

                                                    <input class="form-control input-sm" type="text" placeholder="Type a comment">
                                                </div>
                                                <!-- /.post -->
                                            </div>

                                            <div class="active tab-pane" id="activity">
                                                <asp:UpdatePanel runat="server" ID="PanelDePerfil">
                                                    <ContentTemplate>
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Nombre</label>
                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtNombre" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Telefono</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtTelefono" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Direccion</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtDireccion" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Correo Electronico</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtCorreo" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">RFC</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtRFC" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Hobbies</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtHobbies" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Descripcion</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtDesc" runat="server" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">

                                                                    <asp:Button runat="server" ID="btnActualizarInfo" CssClass="btn  btn-danger" Text="Actualizar Informacion" />
                                                                    <asp:Label ID="lblError" runat="server" Text="" CssClass="label-danger"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Actualizar Foto de Perfil</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <%--<asp:FileUpload runat="server"  ID="FileUpload2" />--%>
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:FileUpload runat="server" ID="FileUpload1" />
                                                                            <br />
                                                                            <asp:Label ID="lblFotoPerfil" runat="server" Text="" CssClass="label-danger "></asp:Label>
                                                                            <br />
                                                                            <asp:Button runat="server" ID="btnSubirFotoPerfil" CssClass="btn  btn-danger" Text="Subir Foto" />
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:PostBackTrigger ControlID="btnSubirFotoPerfil" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <br />
                                                                </div>
                                                            </div>




                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <%-- <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">

                                  <asp:Label ID="lblFotoPerfil" runat="server" Text="Actualizar Foto de Perfil" CssClass="label-danger"></asp:Label>
                                  <asp:FileUpload runat="server"  ID="FileUpload1" />
                                  <asp:Button  runat="server"  ID="btnSubirFotoPerfil" CssClass ="btn  btn-danger" Text="Subir Foto"  />
                                
                                </div>
                              </div>--%>
                                            </div>

                                            <div class="tab-pane" id="busqueda">

                                                <asp:UpdatePanel runat="server" ID="PanelDeBusqueda">
                                                    <ContentTemplate>
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Clave de Trabajador:</label>

                                                                <div class="col-sm-10">
                                                                    <asp:TextBox ID="txtClaveBusqueda" runat="server" placeholder="Clave del Trabajador"></asp:TextBox>
                                                                    <asp:Button ID="btnBuscarTrabajador" runat="server" Text="Buscar Trabajador" CssClass="btn  btn-info" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-offset-2 col-sm-10">

                                                                    <asp:Label ID="lblErrorBusqueda" runat="server" Text="" CssClass="label-danger"></asp:Label>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Nombre</label>
                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtNombreBuscado" runat="server" class="form-control" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Direccion</label>
                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtDireccionBuscado" runat="server" class="form-control" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Telefono</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtTelefonoBuscado" runat="server" class="form-control" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Correo Electronico</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtCorreoBuscado" runat="server" class="form-control" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Hobbies</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtHobbiesBuscado" runat="server" class="form-control" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputName" class="col-sm-2 control-label">Descripcion</label>

                                                                <div class="col-sm-10">
                                                                    <%-- <input type="email" class="form-control" id="inputName" placeholder="Name">--%>
                                                                    <asp:TextBox ID="txtDescripcionBuscado" runat="server" class="form-control" Enabled="true"></asp:TextBox>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                            </div>

                                            <!-- /.tab-pane -->
                                        </div>

                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- /.nav-tabs-custom -->
                                </div>

                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                        </section>
                        <!-- /.content -->
                    </ContentTemplate>
                </asp:UpdatePanel>



            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>
