﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="DISA.Login" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
   <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" />
  <!-- iCheck -->
    <link href="plugins/iCheck/square/blue.css" rel="stylesheet" />
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
    <form runat="server">
        
<div class="login-box">
  <div class="login-logo">
    <h1>DISA</h1>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Ingresa para iniciar sesión</p>
      <div class="form-group has-feedback">
          <%--<input type="email" class="form-control" placeholder="Codigo DISA">--%>
          <asp:TextBox ID="txtUsuario" class="form-control"  runat="server" placeholder="Codigo DISA" ></asp:TextBox>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <%--<input type="password" class="form-control" placeholder="Password">--%>
        <asp:TextBox ID="txtPassword" class="form-control"  runat="server" placeholder="Contraseña" TextMode="Password" ></asp:TextBox>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
       <%-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>--%>
        <!-- /.col -->
        <div class="col-xs-4">
           <asp:Button ID="btnLogin" runat="server" Text="Sign In" class="btn btn-primary btn-block btn-flat" />
          <%--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--%>
        </div>
         
        <!-- /.col -->
      </div>
      <br />
      <div class="row">
          <div class="col-xs-4">
             <asp:Label ID="lblError" runat="server" Text="Label" CssClass="alert-danger"></asp:Label>
          <%--<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>--%>
        </div>
      </div>
  </div>
  <!-- /.login-box-body -->
</div>

    </form>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<!-- Javascript -->
        <script src="assetsLogin/js/jquery-1.11.1.min.js"></script>
        <script src="assetsLogin/bootstrap/js/bootstrap.min.js"></script>
        <script src="assetsLogin/js/jquery.backstretch.min.js"></script>
        <script src="assetsLogin/js/scripts.js"></script>
</body>
</html>

