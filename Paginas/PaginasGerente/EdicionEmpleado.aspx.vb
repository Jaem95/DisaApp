﻿Public Class EdicionEmpleado
    Inherits System.Web.UI.Page
    Dim codigoTrabajador As String = ""
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim TrabajadoresAD As TrabajadoresAD = New TrabajadoresAD()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return
        codigoTrabajador = Request.QueryString("Accion")
        Dim informacionTrabajador As New List(Of String)
        Dim fechaNacimiento As New List(Of String)
        Dim ds As New DataSet

        ds = ADGeneral.ConsultarRegiones()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRegion.DataSource = ds.Tables("Region").DefaultView
            ddlRegion.DataValueField = "id"
            ddlRegion.DataTextField = "nombre"
            ddlRegion.DataBind()

        End If
        ds = ADGeneral.ConsultarEstados()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEstadosDeMex.DataSource = ds.Tables("Estados").DefaultView
            ddlEstadosDeMex.DataValueField = "id_estado"
            ddlEstadosDeMex.DataTextField = "estado"
            ddlEstadosDeMex.DataBind()

        End If

        ds = ADGeneral.ConsultaTipoSexo()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlSexo.DataSource = ds.Tables("Sexo").DefaultView
            ddlSexo.DataValueField = "id"
            ddlSexo.DataTextField = "tipo"
            ddlSexo.DataBind()

        End If


        ds = ADGeneral.ConsultaEstadoCivil()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEstadoCivil.DataSource = ds.Tables("EstadoCivil").DefaultView
            ddlEstadoCivil.DataValueField = "id"
            ddlEstadoCivil.DataTextField = "tipo"
            ddlEstadoCivil.DataBind()

        End If

        ds = ADGeneral.ConsultaEstudios()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEstudios.DataSource = ds.Tables("ABC").DefaultView
            ddlEstudios.DataValueField = "id"
            ddlEstudios.DataTextField = "tipo"
            ddlEstudios.DataBind()

        End If

        ds = ADGeneral.ConsultaPuesto()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlPuesto.DataSource = ds.Tables("XFG").DefaultView
            ddlPuesto.DataValueField = "id"
            ddlPuesto.DataTextField = "tipo"
            ddlPuesto.DataBind()

        End If

        'ds = TrabajadoresAD.ConsultarEmpleados
        'If ds.Tables(0).Rows.Count > 0 Then
        '    ddlEmpleados.DataSource = ds.Tables("tabla").DefaultView
        '    ddlEmpleados.DataValueField = "nombre"
        '    ddlEmpleados.DataTextField = "clave_trabajador"
        '    ddlEmpleados.DataBind()

        'End If


        informacionTrabajador = TrabajadoresAD.ConsultarTrabajadorEspecifico2(codigoTrabajador)
        If informacionTrabajador.Count > 0 Then
            txtNombreDelEmpleado.Text = informacionTrabajador(0)
            txtApellidosEmpleado.Text = informacionTrabajador(1)
            txtNombreBaja.Text = informacionTrabajador(0) + " " + informacionTrabajador(1)
            fechaNacimiento.AddRange(informacionTrabajador(2).Split("/"))
            ddlSexo.SelectedValue = informacionTrabajador(3)
            txtNumeroTelefonico.Text = informacionTrabajador(4)
            txtDireccion.Text = informacionTrabajador(5)
            ddlEstadoCivil.SelectedValue = informacionTrabajador(6)
            ddlEstudios.SelectedValue = informacionTrabajador(7)
            txtCurp.Text = informacionTrabajador(8)
            ddlEstadosDeMex.SelectedValue = informacionTrabajador(9)
            txtRFC.Text = informacionTrabajador(10)
            txtNumeroTelefonico2.Text = informacionTrabajador(11)
            txtCorreoElectronico.Text = informacionTrabajador(12)
            ddlRegion.SelectedValue = informacionTrabajador(13)
            ddlPuesto.SelectedValue = informacionTrabajador(14)



            txtDia.Text = fechaNacimiento(0)
            txtMes.Text = fechaNacimiento(1)
            txtAnio.Text = fechaNacimiento(2)







        End If



        txtEmpleado.Text = codigoTrabajador
        txtCodigoBaja.Text = codigoTrabajador
        txtEmpleado.Enabled = False

    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return
        Dim validacion As Boolean = False


        Dim nombreEmpleado As String = txtNombreDelEmpleado.Text
        Dim apelllidoEmpleado As String = txtApellidosEmpleado.Text

        Dim fechasNacimiento As String = txtDia.Text + "/" + txtMes.Text + "/" + txtAnio.Text


        Dim curp As String = txtCurp.Text

        Dim rfc As String = txtRFC.Text

        Dim direccion As String = txtDireccion.Text

        Dim estadoCivil As String = ddlEstadoCivil.SelectedValue

        Dim estado As String = ddlEstadosDeMex.SelectedValue


        Dim ultimosEstudios As String = ddlEstudios.SelectedValue

        Dim numeroTelefonico As String = txtNumeroTelefonico.Text

        Dim numeroCelular As String = txtNumeroTelefonico2.Text

        Dim correoElectronico As String = txtCorreoElectronico.Text

        Dim region As String = ddlRegion.SelectedValue

        Dim puesto As String = ddlPuesto.SelectedValue

        Dim sexo As String = ddlSexo.SelectedValue

        If nombreEmpleado.Equals("") Or apelllidoEmpleado.Equals("") Or fechasNacimiento.Equals("") Or curp.Equals("") Or rfc.Equals("") Or direccion.Equals("") Or numeroTelefonico.Equals("") Or Not IsNumeric(numeroTelefonico) Or Not IsNumeric(numeroCelular) Or numeroCelular.Equals("") Or correoElectronico.Equals("") Or txtAnio.Text.Equals("") Or txtDia.Text.Equals("") Or txtMes.Text.Equals("") Or Not IsNumeric(txtMes.Text) Or Not IsNumeric(txtAnio.Text) Or Not IsNumeric(txtDia.Text) Then

            lblErrorModificaciones.Text = "En el Registro no puedes dejar campos vacios!"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La informacion es incorrecta,verificar numeros y campos vacios!');</script>", False)
        Else
            validacion = TrabajadoresAD.ModificarEmpleado(Request.QueryString("Accion"), nombreEmpleado, apelllidoEmpleado, fechasNacimiento, curp, rfc, direccion, estadoCivil, estado, ultimosEstudios, numeroTelefonico, numeroCelular, correoElectronico, region, puesto, sexo)
            If validacion Then
                validacion = TrabajadoresAD.ModificarEmpleadoActivo(Request.QueryString("Accion"), ddlPuesto.SelectedValue, ddlRegion.SelectedValue)
                If validacion Then
                    'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Modificacion correcta!');</script>", False)
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                           alert('Modificacion Correcta!');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/ConsultaEmpleados.aspx';
                                        
                                    </script>    ", False)
                End If




            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro,el usuario a ingresar ya existe! checar RFC o CURP');</script>", False)

            End If
        End If



    End Sub

    Protected Sub ddlEmpleados_SelectedIndexChanged(sender As Object, e As EventArgs)
        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Vamo a eliminar');</script>", False)

    End Sub

    Protected Sub btnElminarCliente_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return
        Dim codigoTrabajador As String = txtCodigoBaja.Text
        Dim razon As String = txtRazon.Text

        If razon.Equals("") Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Tiene que ingresar la razon de la eliminicacion');</script>", False)
        Else

            Dim validacion As Boolean = False

            validacion = TrabajadoresAD.EliminarTrabajador(codigoTrabajador, razon, CType((Session("usuario")), String))
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                           alert('Eliminacion Correcta!');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/ConsultaEmpleados.aspx';
                                        
                                    </script>    ", False)
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error');</script>", False)
            End If

        End If
    End Sub
End Class