﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ConsultaReporteVendedores
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''lblIDVendedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblIDVendedor As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlPeriodo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPeriodo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblNombreVendedor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNombreVendedor As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlRuta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRuta As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''DSRuta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents DSRuta As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''SqlDataSource1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SqlDataSource1 As Global.System.Web.UI.WebControls.SqlDataSource
    
    '''<summary>
    '''txtTotales control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotales As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTotalesPedidos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalesPedidos As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPromedioVenta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPromedioVenta As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtVentaMasAlta control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtVentaMasAlta As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtVentaMasBaja control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtVentaMasBaja As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPromedioTienda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPromedioTienda As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTiendaMayor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTiendaMayor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtIDTiendaMayor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtIDTiendaMayor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtRutaMayor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRutaMayor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTiendaMenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTiendaMenor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtIDTiendaMenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtIDTiendaMenor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtRutaMenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRutaMenor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTiendaRutaMayor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTiendaRutaMayor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtIDTiendaRutaMayor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtIDTiendaRutaMayor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTiendaRutaMenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTiendaRutaMenor As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtIDTiendaRutaMenor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtIDTiendaRutaMenor As Global.System.Web.UI.WebControls.TextBox
End Class
