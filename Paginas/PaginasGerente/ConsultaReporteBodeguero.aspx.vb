﻿Public Class ConsultaReporteBodeguero
    Inherits System.Web.UI.Page
    Public Shared codigoTrabajadores As String = ""
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Public Shared informacion As New List(Of String)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteBodegueroEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

        informacion = ReportesAD.ReporteBodegueroEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Mayor")
        txtRutaMax.Text = informacion(1)

        informacion = ReportesAD.ReporteBodegueroEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Menor")
        txtRutaMin.Text = informacion(1)
    End Sub

    Protected Sub ddlPeriodo_SelectedIndexChanged1(sender As Object, e As EventArgs)
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteBodegueroEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

        informacion = ReportesAD.ReporteBodegueroEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Mayor")
        txtRutaMax.Text = informacion(1)

        informacion = ReportesAD.ReporteBodegueroEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Menor")
        txtRutaMin.Text = informacion(1)
    End Sub

    Private Sub PonerInformacion(informacion As List(Of String))
        txtErrores.Text = informacion(2)
        txtTipoErrorMasFrecuente.Text = informacion(3)
        txtPedidosRealizados.Text = informacion(5)
        txtPromedioPedidos.Text = ""
        'txtRutaMax.Text = informacion(8)
        'txtRutaMin.Text = informacion(2)
        txtRotos.Text = ""
        txtMonto.Text = informacion(4)


        lblIDBodeguero.Text = codigoTrabajadores
        lblNombreBodeguero.Text = informacion(1)


    End Sub

End Class