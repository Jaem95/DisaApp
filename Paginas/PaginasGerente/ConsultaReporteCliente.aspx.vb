﻿Public Class ConsultaReporteCliente
    Inherits System.Web.UI.Page
    Public Shared codigoTrabajadores As String = ""
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Public Shared informacion As New List(Of String)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteClienteEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

        informacion = ReportesAD.ReporteClienteGastadosEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)
        txtPuntosGastados.Text = informacion(0)
    End Sub
    Protected Sub ddlPeriodo_SelectedIndexChanged1(sender As Object, e As EventArgs)
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteClienteEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

        informacion = ReportesAD.ReporteClienteGastadosEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)
        txtPuntosGastados.Text = informacion(0)
    End Sub
    Private Sub PonerInformacion(informacion As List(Of String))
        txtDireccion.Text = informacion(2)
        txtResponsable.Text = informacion(3)
        txtRuta.Text = informacion(4)
        txtDisaDisponibles.Text = informacion(9)
        txtPuntosGastados.Text = ""
        txtPedidosRealizados.Text = informacion(6)
        txtNumeroDevoluciones.Text = informacion(8)
        txtPromedioVenta.Text = informacion(7)

        lblIDCliente.Text = codigoTrabajadores
        lblNombreCliente.Text = informacion(1)


    End Sub
End Class