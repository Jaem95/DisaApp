﻿Public Class HistorialPedidos
    Inherits System.Web.UI.Page
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Dim ds As New DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return

        ds = ReportesAD.HistorialPedidos(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay folios"

        End If
    End Sub
    Protected Sub ddlPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs)
        ds = ReportesAD.HistorialPedidos(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay repartidores"

        End If
    End Sub

    Protected Sub btnBuscarFolio_Click1(sender As Object, e As EventArgs)
        Page.Validate()
        If Page.IsValid Then
            ds = ReportesAD.HistorialPedidosFolio(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, txtCodigoFolio.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay folios"

            End If
        End If
    End Sub





End Class