﻿Public Class ConsultaReporteRepartidor
    Inherits System.Web.UI.Page
    Public Shared codigoTrabajadores As String = ""
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Public Shared informacion As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then Return
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteRepartidorEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

    End Sub

    Protected Sub ddlPeriodo_SelectedIndexChanged1(sender As Object, e As EventArgs)
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteRepartidorEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

    End Sub

    Private Sub PonerInformacion(informacion As List(Of String))
        txtTotales.Text = informacion(11)
        txtTotalesPedidos.Text = informacion(12)
        txtPromedioVenta.Text = informacion(6)
        txtVentaMasAlta.Text = informacion(7)
        txtVentaMasBaja.Text = informacion(8)
        txtEntregas.Text = informacion(2)
        txtPromTiempo.Text = informacion(3)
        txtTiempoMayor.Text = informacion(4)
        txtTiempoMenor.Text = informacion(5)
        txtDevoluciones.Text = informacion(9)
        txtEMF.Text = informacion(10)


        lblIDRepartidor.Text = codigoTrabajadores
        lblNombreRepartidor.Text = informacion(1)


    End Sub

End Class