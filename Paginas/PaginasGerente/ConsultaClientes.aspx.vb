﻿Public Class ConsultaClientes
    Inherits System.Web.UI.Page
    Dim ClientesAD As ClientesAD = New ClientesAD()
    Dim ds As New Data.DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsPostBack Then Return


        'ds = ClientesAD.ConsultarClientes()

        'If ds.Tables(0).Rows.Count > 0 Then
        '    Tabla1.DataSource = ds.Tables("tabla")
        '    Tabla1.DataBind()

        'Else
        '    lblError.Text = "No hay Clientes"
        '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No hay ningun cliente registrado ');</script>", False)
        'End If

    End Sub

    Protected Sub btnBuscarCliente_Click(sender As Object, e As EventArgs) Handles btnBuscarCliente.Click
        lblError.Text = ""
        Dim cliente As String = txtCodigoCliente.Text

        If cliente.Equals("") Then
            lblError.Text = "No puede dejar campos vacios"
        Else
            ds = ClientesAD.ConsultarClientes(cliente)

            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay Clientes"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No hay ningun cliente registrado con ese ID');</script>", False)
            End If

        End If


    End Sub


    Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Tabla1.SelectedIndexChanging
        Dim Indice As Integer = e.NewSelectedIndex
        Dim codigoTrabajador As String = Tabla1.Rows(Indice).Cells(0).Text

        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                           //alert('Hola');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/EdicionCliente.aspx?ID=" & codigoTrabajador.Trim() & "';
                                        
                                    </script>    ", False)




    End Sub
End Class