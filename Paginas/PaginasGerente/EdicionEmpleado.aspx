﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EdicionEmpleado.aspx.vb" Inherits="DISA.EdicionEmpleado" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {

        window.parent.location.href = window.parent.location.href;
    }
    function Redirigir(pagina)
    {
       // alert('Hola');
        document.getElementById('iframe').src = pagina;
    }
</script>  
<script type="text/javascript">
    function ConfirmarRegistro() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("¿Desea realizar este registro?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>  
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content" >
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding:0px 15px 0 15px;">
                    <h1>Registro Empleados
                    </h1>
                </section>

                <asp:UpdatePanel runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <!-- /.row -->
                            <div class="row">
                                <!-- general form elements -->
                                <div class="col-xs-12">
                                    <div class="box box-primary">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div class="box-header">

                                                    <div class="box-header">
                                                        <h3 class="box-title">Empleado a Modificar:</h3>
                                                        <asp:TextBox ID="txtEmpleado" runat="server" Text="" class="form-control" placeholder="Codigo de Empleado"></asp:TextBox>
                                                        <br />
                                                      
                                                    </div>


                                                </div>
                                                <div class="box-body">

                                                    <div class="form-group ">

                                                        <div class="row">
                                                            <div class=" col-md-6">
                                                                <label for="exampleInputEmail1">Nombre de la Empleado:</label>
                                                                <asp:TextBox runat="server" ID="txtNombreDelEmpleado" placeholder="Nombres" CssClass="form-control" />
                                                            </div>
                                                            <div class=" col-md-6">
                                                                <label for="exampleInputEmail1">Apellidos del Empleado</label>
                                                              
                                                                <asp:TextBox runat="server" ID="txtApellidosEmpleado" placeholder="Apellidos" CssClass="form-control" />
                                                            </div>

                                                        </div>
                                                    </div>


                                                    <div class="form-inline">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label for="exampleInputEmail1">Fecha de Nacimiento:</label>
                                                                <asp:TextBox runat="server" ID="txtDia" placeholder="DD" CssClass="form-control" Width="50px" />
                                                                /
                                                                 <asp:TextBox runat="server" ID="txtMes" placeholder="MM" CssClass="form-control" Width="50px" />
                                                                /
                                                                 <asp:TextBox runat="server" ID="txtAnio" placeholder="YYYY" CssClass="form-control" Width="55px" />
                                                            </div>
                                                             <div class="col-md-4">
                                                                <label for="exampleInputEmail1">CURP </label>

                                                                <asp:TextBox runat="server" ID="txtCurp" placeholder="CURP" CssClass="form-control" Width="80%" />
                                                            </div>
                                                             <div class="col-md-4">
                                                                <label for="exampleInputEmail1">RFC </label>

                                                                <asp:TextBox runat="server" ID="txtRFC" placeholder="RFC" CssClass="form-control"   Width="80%" />
                                                            </div>

                                                        </div>
                                   
                                                           
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Direccion </label>
                                                        <asp:TextBox runat="server" ID="txtDireccion" placeholder="Direccion" CssClass="form-control" />
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Estado Civil</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstadoCivil" runat="server" AutoPostBack="false" Width="100%">
                                                                   
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Estado</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstadosDeMex" runat="server" AutoPostBack="false" Width="100%" >
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Ultimos Estudios</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlEstudios" runat="server" AutoPostBack="false" Width="100%" >
                                                                   
                                                                </asp:DropDownList>
                                                            </div>
                                                               <div class="col-xs-3">
                                                                <label for="exampleInputEmail1">Sexo</label>
                                                                <br />
                                                                <asp:DropDownList ID="ddlSexo" runat="server" AutoPostBack="false" Width="100%" >
                                                                   
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class=" row">
                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Telefono </label>
                                                                <%-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                                <asp:TextBox runat="server" ID="txtNumeroTelefonico" placeholder="Numero Telefonico" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <label for="exampleInputEmail1">Celular</label>
                                                                <%-- <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">--%>
                                                                <asp:TextBox runat="server" ID="txtNumeroTelefonico2" placeholder="Numero Telefonico" CssClass="form-control" />

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class=" form-group ">

                                                        <div class="row">
                                                             <div class="col-xs-4">
                                                                <label for="exampleInputEmail1">Correo Electronico</label>
                                                                <asp:TextBox runat="server" ID="txtCorreoElectronico" placeholder="Correo Electronico" CssClass="form-control" />
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <label for="exampleInputEmail1">Region</label>
                                                                  <br />
                                                                <asp:DropDownList ID="ddlRegion" runat="server" AutoPostBack="false" Width="100%" >
                                                                </asp:DropDownList>
                                                            </div>
                                                              <div class="col-xs-4">
                                                                <label for="exampleInputEmail1">Puesto al que aplica</label>
                                                                  <br />
                                                                <asp:DropDownList ID="ddlPuesto" runat="server" AutoPostBack="false" Width="100%" >
                                                                  
                                                                </asp:DropDownList>
                                                            </div>

                                                           


                                                        </div>
                                                    </div>
                                                     

                                                 
                                                    <!-- /.box-body -->

                                                    <div class="box-footer">
                                                        <asp:Button runat="server" ID="btnGuardarCambios" class="btn  btn-primary pull-left" Text="Modificar Empleado" OnClientClick="ConfirmarRegistro()" OnClick="btnGuardarCambios_Click" />
                                                        <button type="button" id="btnEliminarUsuario" class="btn btn-danger pull-right " data-toggle="modal" data-target="#EliminarTrabajador">Eliminar Trabajador </button>
                                                        <%-- <asp:Button runat="server" ID="btnEliminarRegion" class="btn btn-danger pull-right col-xs-offset-1 btn-lg " Text="Eliminar Producto" />--%>
                                                        <h4>
                                                            <asp:Label runat="server" ID="lblErrorModificaciones" Text="" class=" col-xs-offset-2 label-success  "></asp:Label>
                                                        </h4>

                                                    </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                            <!-- /.box -->
                            </div>
                                <!-- /.row -->

                        </section>
                        <!-- /.content -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnGuardarCambios" />
                    </Triggers>
                </asp:UpdatePanel>

                <div id="EliminarTrabajador" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Dar de baja empleado</h4>
                                    </div>
                                
                                 
                                    <div class="modal-body">

                                         <div class=" form-group ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Codigo del Trabajador:</label>
                                                  <asp:TextBox runat ="server" ID="txtCodigoBaja" CssClass="form-control"   Width ="100%" Enabled="false" ></asp:TextBox>
                                                </div
                                                <div class="col-md-6">
                                                     <label>Nombre del Trabajador:</label>
                                                    <asp:TextBox runat ="server" ID="txtNombreBaja" CssClass="form-control"   Width ="45%" Enabled="false" ></asp:TextBox>
                                                </div>

                                            </div>

                                        </div>
                                        <div class=" form-group  " align="center">
                                            <div class="row">
                                                <div class=" col-md-offset-1  col-md-10" >
                                                    <label>¿Porque nos deja?</label>
                                                    <asp:TextBox  runat="server" ID="txtRazon"  TextMode="MultiLine"  CssClass="form-control"></asp:TextBox>

                                                </div>
                                            </div>

                                        </div>
                                        
                                    </div>
                                    <div class="modal-footer">
                                       <asp:Button runat ="server"  ID="btnElminarCliente" CssClass="btn btn-success" Text="Eliminar Trabajador" OnClick="btnElminarCliente_Click" OnClientClick="ConfirmarRegistro()" />

                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                </div>
                                <!--Fin Modal PrecioGeneral -->



            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>
