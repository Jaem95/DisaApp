﻿Public Class RecordProductos
    Inherits System.Web.UI.Page
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Dim ds As New DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return

        ds = ReportesAD.RecorProductos(CType((Session("ubicacion")), String), "categoria")
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay repartidores"

        End If
    End Sub
    Protected Sub selectOrder_SelectedIndexChanged(sender As Object, e As EventArgs)
        ds = ReportesAD.RecorProductos(CType((Session("ubicacion")), String), selectOrder.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay repartidores"

        End If
    End Sub

    Protected Sub btnBuscarRecord_Click1(sender As Object, e As EventArgs)
        Page.Validate()
        If Page.IsValid Then
            ds = ReportesAD.RecorProductosInd(CType((Session("ubicacion")), String), txtProducto.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay repartidores"

            End If
        End If
    End Sub
End Class