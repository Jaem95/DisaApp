﻿Public Class ConsultaPagosRepartidores
    Inherits System.Web.UI.Page
    Dim ubicacion As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ubicacion = CType((Session("ubicacion")), String)

        SqlDataSource1.SelectCommand = "select a.id, b.vendedor, concat(c.nombre,' ',c.apellidos) as nombre,sum(a.monto_cambios)as monto_cambios,sum(a.monto_cobrado) as monto_esparado, sum(a.monto_devolucion) as monto_devolucion, sum(a.monto_cambios) + sum(a.monto_cobrado) as total,case when a.status = 1  then 'Pediente' when  a.status = 2 then 'Cobrado'  end as status 
from R" + ubicacion + "PRoductosVenta a,R" + ubicacion + "pedidos b,empleados c 
where a.idPedido = b.id  and c.clave_trabajador = b.vendedor  and a.fecha = convert(varchar(10),  dateadd(hour,-6,getdate()), 103)
group by vendedor,monto_cambios,nombre,apellidos,a.fecha,a.status,a.id"


    End Sub

    Protected Sub btnBuscarRepartidor_Click(sender As Object, e As EventArgs) Handles btnBuscarRepartidor.Click
        Page.Validate()
        If Page.IsValid Then
            SqlDataSource1.SelectCommand = "select a.id, b.vendedor, concat(c.nombre,' ',c.apellidos) as nombre,sum(a.monto_cambios)as monto_cambios,sum(a.monto_cobrado) as monto_esparado, sum(a.monto_devolucion) as monto_devolucion, sum(a.monto_cambios) + sum(a.monto_cobrado) as total,case when a.status = 1  then 'Pediente' when  a.status = 2 then 'Cobrado'  end as status 
from R" + ubicacion + "PRoductosVenta a,R" + ubicacion + "pedidos b,empleados c 
where a.idPedido = b.id  and c.clave_trabajador = b.vendedor  and a.fecha = convert(varchar(10),  dateadd(hour,-6,getdate()), 103) and b.vendedor = '" + txtBuscarRepartidor.Text + "' 
group by vendedor,monto_cambios,nombre,apellidos,a.fecha,a.status,a.id"
        End If



    End Sub
End Class