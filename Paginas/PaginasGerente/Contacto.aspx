﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Contacto.aspx.vb" Inherits="DISA.Contacto" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<script type="text/javascript">
    function RefreshParent() {
       
        window.parent.location.href = window.parent.location.href;
    }
</script>  
<script type="text/javascript">
    function ConfirmarRegistro() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("¿Desea realizar este registro?")) {
            confirm_value.value = "Yes";
        } else {
            confirm_value.value = "No";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>
    
       <style type="text/css">
        .bodyGIF
        {
            margin: 0;
            padding: 0;
            font-family: Arial;
        }
       .modalGIF
        {
            position: fixed;
            z-index: 999;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8;
        }
        .centerGIF
        {
            z-index: 1000;
            margin: 300px auto;
            padding: 10px;
            width: 130px;
            background-color: White;
            border-radius: 10px;
            filter: alpha(opacity=100);
            opacity: 1;
            -moz-opacity: 1;
        }
        .center2GIF
        {
            height: 128px;
            width: 128px;
        }
    </style>
      
<body class="hold-transition  skin-blue   sidebar-mini">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="wrapper" style="background-color:#ecf0f5;">
            <!-- Content Wrapper. Contains page content -->
            <div class="content">
                <!-- Content Header (Page header) -->
                <section class="content-header" style="padding: 0px 15px 0 15px;">
                    <h1>Solicitudes y Comentarios
                    </h1>
                </section>

                <asp:UpdatePanel runat="server" ID="contacto">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">


                                    <div class="box">
                                        <div class="box-header">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group ">

                                                        <h4>
                                                            <label>ID Tienda:</label></h4>
                                                        <asp:TextBox runat="server" placeholder="ID Tienda" ID="txtTienda" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtTienda" ErrorMessage="Debes de llenar este campo" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:CompareValidator runat="server" ControlToValidate="txtTienda" ErrorMessage="El dato debe de ser numerico" Operator="DataTypeCheck" Type="Integer" ForeColor="red"></asp:CompareValidator>

                                                    </div>

                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">

                                                        <h4>
                                                            <label>Nombre Tienda:</label></h4>
                                                        <asp:TextBox runat="server" placeholder="Nombre Tienda" ID="txtNombreTienda" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNombreTienda" ErrorMessage="Debes de llenar este campo" ForeColor="Red"></asp:RequiredFieldValidator>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">

                                                        <h4>
                                                            <label>Ruta:</label></h4>
                                                        <asp:TextBox runat="server" placeholder="Ruta" ID="txtRuta" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtRuta" ErrorMessage="Debes de llenar este campo" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <br />
                                                        <asp:CompareValidator runat="server" ControlToValidate="txtRuta" ErrorMessage="El dato debe de ser numerico" Operator="DataTypeCheck" Type="Integer" ForeColor="red"></asp:CompareValidator>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">

                                                    <div class="form-group ">

                                                        <h4>
                                                            <label>ID Region:</label></h4>
                                                        <asp:TextBox runat="server" placeholder="Region" ID="txtRegion" Enabled="false" CssClass="form-control"></asp:TextBox>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <h4>
                                                            <label>Solicitud:</label>

                                                        </h4>
                                                        <asp:TextBox runat="server" ID="txtSolicitud" TextMode="MultiLine" CssClass=" form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtSolicitud" ErrorMessage="Debes de ingresar una solicitud" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="form-group">
                                                        <h4>
                                                            <label>Comentarios:</label>

                                                        </h4>
                                                        <asp:TextBox runat="server" ID="txtComentarios" TextMode="MultiLine" CssClass=" form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtComentarios" ErrorMessage="Debes de ingresar un comentario" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="form-group center-block">
                                                        <div class="col-md-offset-2  col-md-8">
                                                            <asp:Button runat="server" Text="Enviar" CssClass="form-control  btn-info center-block  " OnClick="Unnamed_Click" />
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!-- /.box-body -->

                                    </div>

                                  
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </ContentTemplate>


                </asp:UpdatePanel>


                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="contacto">
                    <ProgressTemplate>
                        <div class="modalGIF">
                            <div class="centerGIF">
                                <%--<asp:Image alt="" ImageUrl="~/images/loader.gif" runat="server" />--%>
                                <img alt="" src="http://www.aspsnippets.com/Demos/loader4.gif" />
                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>





            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- ./wrapper -->
    </form>
    <!-- jQuery 2.2.3 -->
    <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="../../plugins/chartjs/Chart.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
    <!-- page script -->
    <%-- <script type="text/javascript">
        function cambioVentana() {
            
            //console.log("Venta Activa:" + ventaActiva);
           // console.log();
            //(document.getElementById(ventaActiva)).removeClass();
            $("liActivity, activity").removeClass("active");
            $()

            (document.getElementById(busqueda)).addClass("active");
            (document.getElementById(liBusqueda)).addClass("active");

        };
    </script>--%>
</body>
</html>