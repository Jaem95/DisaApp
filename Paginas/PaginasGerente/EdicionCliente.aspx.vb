﻿Public Class EdicionCliente
    Inherits System.Web.UI.Page
    Dim codigoTrabajador As String = ""
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ClientesAD As ClientesAD = New ClientesAD()
    Dim informacionCliente As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        codigoTrabajador = Request.QueryString("ID")
        If IsPostBack Then Return


        informacionCliente = ClientesAD.ConsultarClientesLista(codigoTrabajador)
        txtCliente.Text = codigoTrabajador
        txtNombreDeTienda.Text = informacionCliente(0)
        txtReponsableTienda.Text = informacionCliente(2)
        txtDireccion.Text = informacionCliente(3)
        txtNumeroTelefonico.Text = informacionCliente(4)
        txtNumeroTelefonico2.Text = informacionCliente(5)
        txtRFC.Text = informacionCliente(6)
        txtCorreoElectronico.Text = informacionCliente(7)
        txtLongitud.Text = informacionCliente(8)
        txtLatitud.Text = informacionCliente(9)








    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs)

        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return

        Dim nombreTienda As String = txtNombreDeTienda.Text
        Dim responsableTienda As String = txtReponsableTienda.Text
        Dim direccion As String = txtDireccion.Text
        Dim telefono As String = txtNumeroTelefonico.Text
        Dim celular As String = txtNumeroTelefonico2.Text
        Dim correo As String = txtCorreoElectronico.Text
        Dim longitud As String = txtLongitud.Text
        Dim latitud As String = txtLatitud.Text
        Dim rfc As String = txtRFC.Text

        If nombreTienda.Equals("") Or responsableTienda.Equals("") Or direccion.Equals("") Or telefono.Equals("") Or Not IsNumeric(telefono) Or celular.Equals("") Or Not IsNumeric(celular) Or correo.Equals("") Or longitud.Equals("") Or Not IsNumeric(longitud) Or latitud.Equals("") Or Not IsNumeric(latitud) Or rfc.Equals("") Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios o ingresar caracteres invalidos');</script>", False)
        Else
            Dim validacion As Boolean = False

            validacion = ClientesAD.ActualizarInformacion(nombreTienda, ddlTrabajador.SelectedItem.ToString, direccion, telefono, celular, correo, longitud, latitud, rfc, Request.QueryString("ID"))
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                            alert('Modificación Correcta');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/ConsultaClientes.aspx?';
                                        
                                    </script>    ", False)
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en  actualización');</script>", False)
            End If

        End If
    End Sub

    Protected Sub btnEliminarUsuario_Click(sender As Object, e As EventArgs) Handles btnEliminarUsuario.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return

        Dim validacion As Boolean = False

        validacion = ClientesAD.EliminarCliente(Request.QueryString("ID"))
        If validacion Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                            alert('Modificación Correcta');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/ConsultaClientes.aspx?';
                                        
                                    </script>    ", False)
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en  actualización');</script>", False)
        End If


    End Sub

    Protected Sub ddlTrabajador_SelectedIndexChanged(sender As Object, e As EventArgs)
        txtReponsableTienda.Text = ddlTrabajador.SelectedValue

    End Sub

    'Protected Sub DSVendedores_DataBinding(sender As Object, e As EventArgs)
    '    ddlTrabajador.SelectedValue = informacionCliente(2)
    'End Sub
End Class