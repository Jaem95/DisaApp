﻿'Falta ingresar los productos a la tabla



Imports System.Globalization
Imports System.Threading

Public Class RegistrarProducto1
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Public Shared dtProductos As New DataTable
    Dim InventariosAD As InventariosAD = New InventariosAD()



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")

        If Not Me.IsPostBack Then
            dtProductos = New DataTable()
            dtProductos.Columns.Add("CodigoBarras", GetType(String))
            dtProductos.Columns.Add("Producto", GetType(String))
            dtProductos.Columns.Add("Cantidad", GetType(String))
            dtProductos.Columns.Add("Caducidad", GetType(String))
            Dim region As String = CType((Session("ubicacion")), String)

        End If







    End Sub

    Protected Sub btnBuscarProducto_Click(sender As Object, e As EventArgs) Handles btnBuscarProducto.Click
        If Page.IsValid Then
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Todo va bien');</script>", False)
            Dim informacionProducto As New List(Of String)

            Dim indice As Integer = 0
            Dim codigoBarras As String = ""
            Dim val As Boolean = True

            For Each row In dtProductos.Rows
                codigoBarras = TablaProductos.Rows(indice).Cells(0).Text
                If codigoBarras.Equals(txtBuscarProducto.Text) Then
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('El producto ya esta en la lista');</script>", False)
                    val = False
                    Exit For

                End If
                indice = indice + 1
            Next


            If val Then
                informacionProducto = ADGeneral.ConsultarInformacionProducto(txtBuscarProducto.Text.Trim)
                If informacionProducto.Count > 0 Then
                    'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Todo va bien');</script>", False)
                    dtProductos.Rows.Add(txtBuscarProducto.Text, informacionProducto(0), txtCantidadRecibida.Text, txtFechaCaducidad.Text.ToString())
                    TablaProductos.DataSource = dtProductos
                    TablaProductos.DataBind()

                    btnRegistrarProductos.Enabled = True
                    CleanControls(Me.Controls)
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No existe el articulo');</script>", False)

                End If

            End If





        End If


    End Sub


    Public Sub CleanControls(ByVal controles As ControlCollection)
        For Each control As Control In controles
            If TypeOf control Is TextBox Then
                DirectCast(control, TextBox).Text = String.Empty
            ElseIf TypeOf control Is DropDownList Then
                DirectCast(control, DropDownList).ClearSelection()
            ElseIf TypeOf control Is RadioButtonList Then
                DirectCast(control, RadioButtonList).ClearSelection()
            ElseIf TypeOf control Is CheckBoxList Then
                DirectCast(control, CheckBoxList).ClearSelection()
            ElseIf TypeOf control Is RadioButton Then
                DirectCast(control, RadioButton).Checked = False
            ElseIf TypeOf control Is CheckBox Then
                DirectCast(control, CheckBox).Checked = False
            ElseIf control.HasControls() Then
                CleanControls(control.Controls)
            End If
        Next
    End Sub

    Protected Sub TablaProductos_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles TablaProductos.SelectedIndexChanging
        Dim Indice As Integer = e.NewSelectedIndex
        Dim codigoBarras As String = TablaProductos.Rows(Indice).Cells(0).Text

        dtProductos.Rows.RemoveAt(Indice)
        TablaProductos.DataSource = dtProductos
        TablaProductos.DataBind()


    End Sub

    Protected Sub btnRegistrarProductos_Click(sender As Object, e As EventArgs) Handles btnRegistrarProductos.Click
        Dim codigoBarras As String = ""
        Dim indice As Integer = 0
        Dim validacion As Boolean = False
        Dim contador As Integer = 0

        For Each row In dtProductos.Rows
            codigoBarras = TablaProductos.Rows(indice).Cells(0).Text
            validacion = InventariosAD.ConsultarExistencia(codigoBarras, CType((Session("ubicacion")), String))
            If validacion Then
                validacion = InventariosAD.ActualizarInventario(codigoBarras, CType((Session("ubicacion")), String), TablaProductos.Rows(indice).Cells(2).Text, TablaProductos.Rows(indice).Cells(3).Text)
                If validacion Then
                    contador = contador + 1
                End If
            Else
                Dim informacionProducto As New List(Of String)
                informacionProducto = ADGeneral.ConsultarInformacionProductoParaInventario(codigoBarras)

                validacion = InventariosAD.InsertarNuevoProducto(codigoBarras, TablaProductos.Rows(indice).Cells(2).Text, TablaProductos.Rows(indice).Cells(3).Text, 100, 10, 1, informacionProducto(6), CType((Session("ubicacion")), String))
                If validacion Then
                    contador = contador + 1
                End If

            End If


            indice = indice + 1
        Next

        If dtProductos.Rows.Count = contador Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Registro de productos correcto');</script>", False)

        End If

    End Sub


End Class