﻿Public Class ConsultaEmpleados
    Inherits System.Web.UI.Page
    Dim TrabajadoresAD As TrabajadoresAD = New TrabajadoresAD()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return

        Dim ds As New Data.DataSet
        ds = TrabajadoresAD.ConsultarEmpleados()

        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No existe el empleado que ingreso"

        End If



    End Sub

    Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Tabla1.SelectedIndexChanging
        Dim Indice As Integer = e.NewSelectedIndex
        Dim codigoTrabajador As String = Tabla1.Rows(Indice).Cells(0).Text

        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                           //alert('Hola');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/EdicionEmpleado.aspx?Accion=" & codigoTrabajador.Trim() & "';
                                        
                                    </script>    ", False)




    End Sub

    Protected Sub btnBuscarProducto_Click(sender As Object, e As EventArgs) Handles btnBuscarEmpleado.Click

        Dim codigoTrabajador As String = txtBuscarEmpleado.Text()
        Dim ds As New Data.DataSet

        If codigoTrabajador.Equals("") Then
            lblError.Text = "Ingresa algun dato valido"
        Else
            ds = TrabajadoresAD.ConsultarTrabajadorEspecifico(codigoTrabajador)
            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay productos"

            End If

        End If

    End Sub

End Class