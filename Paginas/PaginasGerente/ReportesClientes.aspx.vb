﻿Public Class ReportesClientes
    Inherits System.Web.UI.Page
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Dim ds As New DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return

        ds = ReportesAD.ReporteClientes(CType((Session("ubicacion")), String), 12)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay clientes"

        End If

    End Sub

    Protected Sub ddlPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs)
        ds = ReportesAD.ReporteClientes(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay clientes"

        End If
    End Sub

    Protected Sub btnBuscarRepartidor_Click1(sender As Object, e As EventArgs)
        Page.Validate()
        If Page.IsValid Then
            ds = ReportesAD.ReporteClientesButton(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, txtBuscarClientes.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay clientes"

            End If
        End If
    End Sub
End Class