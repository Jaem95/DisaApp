﻿Public Class EditarPagosRepartidores
    Inherits System.Web.UI.Page
    Dim CobranzaAD As CobranzaAD = New CobranzaAD()
    Dim Completado As Boolean


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If IsPostBack Then Return
        Completado = False
        lblFecha.Text = DateTime.Today.ToLongDateString
        Dim vendedor As String = Request.QueryString("ID")
        Dim nombre As String = Request.QueryString("Nombre")
        Dim monto As String = Request.QueryString("Monto")

        btnPagoFicha.Enabled = False
        btnPagoCaja.Enabled = False

        'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('" + idTransaccion + "');</script>", False)
        Label1.Text = nombre
        lblNombreRepartidor.Text = vendedor
        txtCantidadEsperada.Text = monto
        txtCantidadEfectivoEsperada.Text = monto
    End Sub

    Protected Sub txtCantidadAIngresar_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadAIngresar.TextChanged

        Page.Validate("One")
        If Page.IsValid Then

            txtDiferencia.Text = txtCantidadAIngresar.Text - txtCantidadEsperada.Text
            fuImagenFicha.Enabled = True
            btnPagoFicha.Enabled = True

            If txtCantidadAIngresar.Text - txtCantidadEsperada.Text = 0 Then
                btnPagoCaja.Enabled = False
                txtCantidadAEntregarEfectivi.Enabled = False
                txtCantidadEfectivoEsperada.Enabled = False
                Completado = True

            End If

        End If


    End Sub

    Protected Sub txtCantidadAEntregarEfectivi_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadAEntregarEfectivi.TextChanged
        Page.Validate("Two")
        If Page.IsValid Then

            txtDiferenciaEfectivo.Text = txtCantidadAEntregarEfectivi.Text - txtCantidadEfectivoEsperada.Text
            btnPagoCaja.Enabled = True

            If txtCantidadAEntregarEfectivi.Text - txtCantidadEfectivoEsperada.Text = 0 Then
                btnPagoFicha.Enabled = False
                txtCantidadAIngresar.Enabled = False
                txtCantidadEsperada.Enabled = False
                Completado = True

            End If
        End If

    End Sub

    Protected Sub btnPagoFicha_Click(sender As Object, e As EventArgs) Handles btnPagoFicha.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return
        Dim path As String = Server.MapPath("~/ImagenesRecibos/")
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""

        If fuImagenFicha.HasFile Then
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(fuImagenFicha.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                    Exit For
                End If
            Next
            If fileOK Then
                Dim validacion As Boolean = False

                validacion = CobranzaAD.InsertarFicha(fuImagenFicha.FileName, txtCantidadAIngresar.Text, txtCantidadEsperada.Text, txtDiferencia.Text, lblNombreRepartidor.Text, FormatDateTime(Now, 2), Request.QueryString("IDTran"), CType((Session("ubicacion")), String))
                If validacion Then
                    '  ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Caputra de Ficha Correcta imagenes!');</script>", False)
                    Try
                        nombre = fuImagenFicha.FileName()
                        ruta = "~/ImagenesRecibos/" & nombre
                        fuImagenFicha.PostedFile.SaveAs(path & fuImagenFicha.FileName)
                        If txtDiferencia.Text = 0 Then
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                           alert('Captura de Ficha Correcta');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/ConsultaPagosRepartidores.aspx';
                                        
                                    </script>    ", False)
                        Else
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Caputra de Ficha Correcta !');</script>", False)

                        End If


                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registo!');</script>", False)

                    End Try

                End If

            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo imagenes!');</script>", False)


            End If
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Tienes que Seleccionar una foto!!');</script>", False)
        End If
    End Sub

    Protected Sub btnPagoCaja_Click(sender As Object, e As EventArgs) Handles btnPagoCaja.Click
        Page.Validate("Two")
        If Page.IsValid Then
            Dim validacion As Boolean = False

            validacion = CobranzaAD.InsertarPago(txtCantidadAEntregarEfectivi.Text, txtCantidadEfectivoEsperada.Text, txtDiferenciaEfectivo.Text, lblNombreRepartidor.Text, FormatDateTime(Now, 2), Request.QueryString("IDTran"), CType((Session("ubicacion")), String))

            If validacion Then

                If txtDiferenciaEfectivo.Text = 0 Then
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script type='text/javascript'>
                                   
                                           alert('Captura de Pago Correcta');
                                            parent.document.getElementById('iframe').src = '/Paginas/PaginasGerente/ConsultaPagosRepartidores.aspx';
                                        
                                    </script>    ", False)
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Caputra de Pago Correcta !');</script>", False)

                End If

                ' ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Caputra de Pago Correcta !');</script>", False)

            End If
        End If
    End Sub
End Class



'Acutallizar el estatus de pedido!!