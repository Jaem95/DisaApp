﻿'Genera la contraseña para el usuario
Public Class RegistroEmpleados
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim TrabajadoresAD As TrabajadoresAD = New TrabajadoresAD()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return
        Dim ds As New DataSet

        ds = ADGeneral.ConsultarRegiones()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRegion.DataSource = ds.Tables("Region").DefaultView
            ddlRegion.DataValueField = "id"
            ddlRegion.DataTextField = "nombre"
            ddlRegion.DataBind()

        End If
        ds = ADGeneral.ConsultarEstados()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEstadosDeMex.DataSource = ds.Tables("Estados").DefaultView
            ddlEstadosDeMex.DataValueField = "id_estado"
            ddlEstadosDeMex.DataTextField = "estado"
            ddlEstadosDeMex.DataBind()

        End If

        ds = ADGeneral.ConsultaTipoSexo()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlSexo.DataSource = ds.Tables("Sexo").DefaultView
            ddlSexo.DataValueField = "id"
            ddlSexo.DataTextField = "tipo"
            ddlSexo.DataBind()

        End If


        ds = ADGeneral.ConsultaEstadoCivil()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEstadoCivil.DataSource = ds.Tables("EstadoCivil").DefaultView
            ddlEstadoCivil.DataValueField = "id"
            ddlEstadoCivil.DataTextField = "tipo"
            ddlEstadoCivil.DataBind()

        End If

        ds = ADGeneral.ConsultaEstudios()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlEstudios.DataSource = ds.Tables("ABC").DefaultView
            ddlEstudios.DataValueField = "id"
            ddlEstudios.DataTextField = "tipo"
            ddlEstudios.DataBind()

        End If

        ds = ADGeneral.ConsultaPuesto()

        If ds.Tables(0).Rows.Count > 0 Then
            ddlPuesto.DataSource = ds.Tables("XFG").DefaultView
            ddlPuesto.DataValueField = "id"
            ddlPuesto.DataTextField = "tipo"
            ddlPuesto.DataBind()

        End If

    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs)

        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return

        lblErrorModificaciones.Text = ""
        Dim validacion As Boolean = False
        Dim idGenerado As String = 0

        Dim path As String = Server.MapPath("~/ImagenesPerfil/")
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""


        If FileUpload1.HasFile Then
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                    Exit For
                End If
            Next
            If fileOK Then
                Dim nombreEmpleado As String = txtNombreDelEmpleado.Text
                Dim apelllidoEmpleado As String = txtApellidosEmpleado.Text
                Dim fechasNacimiento As String = txtDia.Text + "/" + txtMes.Text + "/" + txtAnio.Text
                Dim curp As String = txtCurp.Text
                Dim rfc As String = txtRFC.Text
                Dim direccion As String = txtDireccion.Text
                Dim estadoCivil As String = ddlEstadoCivil.SelectedValue
                Dim estado As String = ddlEstadosDeMex.SelectedValue
                Dim ultimosEstudios As String = ddlEstudios.SelectedValue
                Dim numeroTelefonico As String = txtNumeroTelefonico.Text
                Dim numeroCelular As String = txtNumeroTelefonico2.Text
                Dim correoElectronico As String = txtCorreoElectronico.Text
                Dim region As String = ddlRegion.SelectedValue
                Dim puesto As String = ddlPuesto.SelectedValue
                Dim sexo As String = ddlSexo.SelectedValue

                If nombreEmpleado.Equals("") Or apelllidoEmpleado.Equals("") Or fechasNacimiento.Equals("") Or curp.Equals("") Or direccion.Equals("") Or numeroTelefonico.Equals("") Or Not IsNumeric(numeroTelefonico) Or correoElectronico.Equals("") Or txtAnio.Text.Equals("") Or txtDia.Text.Equals("") Or txtMes.Text.Equals("") Or Not IsNumeric(txtMes.Text) Or Not IsNumeric(txtAnio.Text) Or Not IsNumeric(txtDia.Text) Then
                    lblErrorModificaciones.Text = "En el Registro no puedes dejar campos vacios!"
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La informacion es incorrecta,verificar numeros y campos vacios!');</script>", False)
                Else
                    idGenerado = TrabajadoresAD.RegistrarTrababajador(nombreEmpleado, apelllidoEmpleado, fechasNacimiento, curp, rfc, direccion, estadoCivil, estado, ultimosEstudios, numeroTelefonico, numeroCelular, correoElectronico, region, puesto, sexo)
                    If idGenerado.ToString() IsNot "" Then
                        Try
                            nombre = FileUpload1.FileName()
                            ruta = "~/ImagenesPerfil/" & nombre

                            If TrabajadoresAD.SubirFoto(idGenerado, ruta) Then
                                FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)

                                lblErrorModificaciones.Text = "ALTA DE EMPLEADO CORRECTA!!"
                                Dim pwd As String = CreateRandomPassword(10)
                                validacion = TrabajadoresAD.ActivarClientes(idGenerado, pwd, ddlPuesto.SelectedValue, region)
                                If validacion Then
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Alta de trabajador correcta, el ID que se genero fue: " & idGenerado & ", y su contraseña: " + pwd + "');</script>", False)
                                    lblErrorModificaciones.Text = "Alta de trabajador correcta, el ID que se genero fue: " & idGenerado & ", y su contraseña: " + pwd + ""

                                End If


                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Alta de trabajador correcta, el ID que se genero fue: " & idGenerado & "');</script>", False)


                            Else

                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en creacion de Regiones');</script>", False)

                            End If

                        Catch ex As Exception
                            lblErrorModificaciones.Text = "Error en el registro de region!"
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registo!');</script>", False)

                        End Try

                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro,el usuario a ingresar ya existe! checar RFC o CURP');</script>", False)
                    End If
                End If

            Else
                lblFoto.Text = "Solo fotos!."
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo imagenes!');</script>", False)


            End If
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Tienes que Seleccionar una foto!!');</script>", False)
            lblErrorModificaciones.Text = "Tienes que seleccionar una foto!"


        End If



    End Sub



    Public Function CreateRandomPassword(PasswordLength As Integer) As String

        Dim _allowedChars As String = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789!@$?"
        Dim randomBytes(0 To PasswordLength - 1) As Byte
        Dim chars(0 To PasswordLength - 1) As Char
        Dim allowedCharCount As Integer = _allowedChars.Length
        For i As Integer = 0 To PasswordLength - 1

            Dim randomObj As New Random()
            randomObj.NextBytes(randomBytes)
            chars(i) = _allowedChars(CInt(randomBytes(i) Mod allowedCharCount))
        Next

        Return New String(chars)
    End Function



End Class