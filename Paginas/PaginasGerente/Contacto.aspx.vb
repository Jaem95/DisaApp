﻿Imports System.Text
Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Net.Mail
Imports System.Net
Imports System
Imports System.Globalization
Imports System.Threading
Imports EASendMail
Imports System.IO
Imports System.Web.Mail

Public Class Contacto
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtRegion.Text = CType((Session("ubicacion")), String)

    End Sub

    Protected Sub Unnamed_Click(sender As Object, e As EventArgs)
        If Page.IsValid Then
            'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Todo va bien');</script>", False)
            Dim idTienda As String = txtTienda.Text
            Dim nombreTienda As String = txtNombreTienda.Text
            Dim ruta As String = txtRuta.Text
            Dim solicitud As String = txtSolicitud.Text
            Dim comentario As String = txtComentarios.Text
            Dim correos As New StringBuilder
            Dim datos As Boolean = False


            correos = ADGeneral.ConsultarCorreos()

            For Each item In correos.ToString.Split("|")

                If item IsNot ("") Then
                    'Enviamos Correos

                    Dim oMail As New EASendMail.SmtpMail("TryIt")
                    Dim oSmtp As New EASendMail.SmtpClient()


                    ' Your hotmail email address
                    oMail.From = "disamng@hotmail.com"

                    ' Set recipient email address, please change it to yours
                    oMail.To = item.Trim

                    ' Set email subject
                    oMail.Subject = "Correo DISA"


                    ' Set email body
                    oMail.TextBody = "El gerente de la tienda:" + idTienda + " con el nombre:" + nombreTienda + " que es parte de la ruta:" + ruta + " tiene la siguiente solicitud: " + vbCrLf + solicitud + vbCrLf + " mas esta  solicitud  tiene como comentario:" + vbCrLf + comentario + vbCrLf + "Gracias."

                    ' Hotmail SMTP server address
                    Dim oServer As New SmtpServer("smtp-mail.outlook.com")

                    ' Hotmail user authentication should use your 
                    ' email address as the user name. 
                    oServer.User = "disamng@hotmail.com"
                    oServer.Password = "disa2016"

                    ' set 25 port, if you want to use 587 port, please change 25 to 587
                    oServer.Port = 587

                    ' detect SSL/TLS connection automatically
                    oServer.ConnectType = SmtpConnectType.ConnectSSLAuto

                    Try

                        Console.WriteLine("start to send email over SSL ...")
                        oSmtp.SendMail(oServer, oMail)
                        Console.WriteLine("email was sent successfully!")
                        datos = True


                    Catch ep As Exception

                        Console.WriteLine("failed to send email with the following error:")
                        Console.WriteLine(ep.Message)
                    End Try
                End If



            Next

            If datos Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Mensajes enviados correctamente!');</script>", False)
            End If



        End If
    End Sub
End Class