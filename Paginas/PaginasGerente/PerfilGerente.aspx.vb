﻿Public Class PerfilGerente
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim PerfilAD As PerfilAD = New PerfilAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)

        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            txtNombre.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            txtCorreo.Text = informacionUsuario(6)
            txtDesc.Text = informacionUsuario(12)
            txtDireccion.Text = informacionUsuario(5)
            txtHobbies.Text = informacionUsuario(11)
            txtRFC.Text = informacionUsuario(9)
            txtTelefono.Text = informacionUsuario(4)
            lblNombre1.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            If tipoUsuario.Equals("ADMIN") Then
                lblPuesto.Text = "Administrador"

            End If
            If tipoUsuario.Equals("GERENTE") Then
                lblPuesto.Text = "Gerente de Zona"

            End If
            If tipoUsuario.Equals("BOD") Then
                lblPuesto.Text = "Bodeguero"

            End If
            lblUbicacion.Text = ADGeneral.BuscarUbicacionEspecifica(ubicacion)

            lblUsuarioDISA.Text = usuario

            imagenPerfil.ImageUrl = informacionUsuario(13)

        End If



    End Sub



    Protected Sub btnActualizarInfo_Click(sender As Object, e As EventArgs) Handles btnActualizarInfo.Click
        lblError.Text = ""
        Dim validacion As Boolean = False

        If txtCorreo.Text().Equals("") Or txtDesc.Text.Equals("") Or txtDireccion.Text.Equals("") Or txtHobbies.Text.Equals("") Or txtNombre.Text.Equals("") Or txtRFC.Text.Equals("") Or txtTelefono.Text.Equals("") Then
            lblError.Text = "Si vas actualizar informacion no puedes dejar ningun campo vacio"
        Else
            lblError.Text = "Si vas actualizar tu informacion no puedes dejar ningun campo vacio"
            validacion = PerfilAD.actualizarInformacion(txtTelefono.Text.ToString(), txtDireccion.Text.ToString(), txtCorreo.Text.ToString(), txtRFC.Text.ToString(), txtHobbies.Text.ToString(), txtDesc.Text.ToString(), CType((Session("usuario")), String))
            If validacion Then
                lblError.Text = ("Actualizacion Correcta")
            Else
                lblError.Text = "Error"

            End If


        End If
    End Sub


    Protected Sub btnBuscarTrabajador_Click(sender As Object, e As EventArgs) Handles btnBuscarTrabajador.Click
        'Dim script As String = "cambioVentana();"
        'ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)

        lblErrorBusqueda.Text = ""
        Dim informacionUsuario As New List(Of String)
        If txtClaveBusqueda.Text.Equals("") Then
            lblErrorBusqueda.Equals("Error, no puedes dejar ningun campo vacio")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(txtClaveBusqueda.Text())

            If informacionUsuario.Count > 0 Then
                txtNombreBuscado.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                txtCorreoBuscado.Text = informacionUsuario(6)
                txtTelefonoBuscado.Text = informacionUsuario(4)
                'txtPuestoBuscado.Text = informacionUsuario(0)
                txtDireccionBuscado.Text = informacionUsuario(5)
                txtDescripcionBuscado.Text = informacionUsuario(12)
                txtHobbiesBuscado.Text = informacionUsuario(11)
            Else
                lblErrorBusqueda.Text = "El usuario no existe"

            End If






        End If



    End Sub

    Protected Sub btnSubirFotoPerfil_Click(sender As Object, e As EventArgs) Handles btnSubirFotoPerfil.Click
        Dim path As String = Server.MapPath("~/ImagenesPerfil/")
        Console.Write(path)
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""

        If FileUpload1.HasFile Then
            Console.Write("Entre al if")
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                End If
                Console.Write("La extencion esta bien")
            Next
            If fileOK Then
                Try
                    nombre = FileUpload1.FileName()
                    ruta = "~/ImagenesPerfil/" & nombre
                    Console.Write(nombre & "_" & ruta)
                    If PerfilAD.ActualizarURLPerfil(CType((Session("usuario")), String), ruta) Then
                        FileUpload1.PostedFile.SaveAs(path &
                        FileUpload1.FileName)
                        Console.Write(CType((Session("usuario")), String))
                        lblFotoPerfil.Text = "File uploaded!"
                        Dim script As String = "RefreshParent();"
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)

                    Else
                        lblFotoPerfil.Text = "Error en la actualizacion!"

                    End If



                Catch ex As Exception
                    Console.Write(ex.ToString())

                    lblFotoPerfil.Text = "File could not be uploaded."
                End Try
            Else
                lblFotoPerfil.Text = "Cannot accept files of this type."
            End If
        End If
    End Sub
End Class