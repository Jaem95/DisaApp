﻿Public Class ConsultaReporteVendedores
    Inherits System.Web.UI.Page
    Public Shared codigoTrabajadores As String = ""
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Public Shared informacion As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then Return
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteVendedorEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

        informacion = ReportesAD.ReporteVendedorEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Mayor")
        txtTiendaMayor.Text = informacion(0)
        txtIDTiendaMayor.Text = informacion(1)
        txtRutaMayor.Text = informacion(2)

        informacion = ReportesAD.ReporteVendedorEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Menor")

        txtTiendaMenor.Text = informacion(0)
        txtIDTiendaMenor.Text = informacion(1)
        txtRutaMenor.Text = informacion(2)

        informacion = ReportesAD.ReporteVendedorEspecifico_3(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Mayor")

        txtTiendaRutaMayor.Text = informacion(0)
        txtIDTiendaRutaMayor.Text = informacion(1)

        informacion = ReportesAD.ReporteVendedorEspecifico_3(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Menor")

        txtTiendaRutaMenor.Text = informacion(0)
        txtIDTiendaRutaMenor.Text = informacion(1)








    End Sub

    Protected Sub ddlPeriodo_SelectedIndexChanged1(sender As Object, e As EventArgs)
        codigoTrabajadores = Request.QueryString("ID")
        informacion = ReportesAD.ReporteVendedorEspecifico(CType((Session("ubicacion")), String), ddlPeriodo.SelectedValue, codigoTrabajadores)

        PonerInformacion(informacion)

        informacion = ReportesAD.ReporteVendedorEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Mayor")
        txtTiendaMayor.Text = informacion(0)
        txtIDTiendaMayor.Text = informacion(1)
        txtRutaMayor.Text = informacion(2)

        informacion = ReportesAD.ReporteVendedorEspecifico_2(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Menor")

        txtTiendaMenor.Text = informacion(0)
        txtIDTiendaMenor.Text = informacion(1)
        txtRutaMenor.Text = informacion(2)

        informacion = ReportesAD.ReporteVendedorEspecifico_3(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Mayor")

        txtTiendaRutaMayor.Text = informacion(0)
        txtIDTiendaRutaMayor.Text = informacion(1)

        informacion = ReportesAD.ReporteVendedorEspecifico_3(CType((Session("ubicacion")), String), codigoTrabajadores, ddlPeriodo.SelectedValue, "Menor")

        txtTiendaRutaMenor.Text = informacion(0)
        txtIDTiendaRutaMenor.Text = informacion(1)

    End Sub

    Private Sub PonerInformacion(informacion As List(Of String))
        txtTotales.Text = informacion(0)
        txtTotalesPedidos.Text = informacion(1)
        txtPromedioVenta.Text = informacion(4)
        txtPromedioTienda.Text = informacion(6)
        txtVentaMasAlta.Text = informacion(7)
        txtVentaMasBaja.Text = informacion(8)

        lblIDVendedor.Text = codigoTrabajadores
        lblNombreVendedor.Text = informacion(5)


    End Sub
End Class