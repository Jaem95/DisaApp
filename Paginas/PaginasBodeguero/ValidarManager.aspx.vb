﻿Public Class ValidarManager
    Inherits System.Web.UI.Page
    Dim AD As PedidosPendientesAD = New PedidosPendientesAD()
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim pendingRequests As String
    Dim deliveredRequests As String
    Dim informacionUsuario As New List(Of String)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count > 0 Then
                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)
            End If
        End If
    End Sub


    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim usuario As String = txtUsuario.Text
        Dim pwd As String = txtPassword.Text
        Dim validacion As Boolean = False
        Dim tipoUsuario As String = ""
        Dim informacionUsuario As New List(Of String)

        If usuario.Equals("") Or pwd.Equals("") Then
            lblError.Text = "No debes de dejar campos vacios!"
        Else
            validacion = AD.checkForManager(usuario, pwd)
            If validacion.Equals(True) Then
                Session.Add("manager", validacion)
                Response.Redirect("/Paginas/PaginasBodeguero/ConteoManual.aspx")
            Else
                lblError.Text = "Acceso Denegado"
            End If
        End If

    End Sub
End Class