﻿Public Class ConteoManual
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim InventarioAD As InventarioAD = New InventarioAD()
    Dim usuario As String
    Dim pwd As String
    Dim manager As Boolean
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim pendingRequests As String
    Dim deliveredRequests As String
    Dim informacionUsuario As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        manager = CType((Session("manager")), Boolean)


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            If manager Then

                informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

                If informacionUsuario.Count > 0 Then
                    lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                    lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                    lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                    imageUsuario1.ImageUrl = informacionUsuario(13)
                    imageUsuario2.ImageUrl = informacionUsuario(13)
                    imageUsuario3.ImageUrl = informacionUsuario(13)

                    If DropDownList1.SelectedValue.Equals("CodigoDisa") Then
                        ds = InventarioAD.getProductsInStock(ubicacion, "codigoDisa")
                    ElseIf DropDownList1.SelectedValue.Equals("NombreProducto") Then
                        ds = InventarioAD.getProductsInStock(ubicacion, "nombre")
                    ElseIf DropDownList1.SelectedValue.Equals("CodigoBarras") Then
                        ds = InventarioAD.getProductsInStock(ubicacion, "codigoBarras")
                    End If
                Else
                    ds = InventarioAD.getProductsInStock(ubicacion, "codigoDisa")
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    Tabla1.DataSource = ds.Tables("tabla")
                    Tabla1.DataBind()

                Else
                    lblError.Text = "No hay Productos"
                End If

            Else
                Response.Redirect("Paginas/ValidarManager.aspx")
            End If

        End If

    End Sub
    Protected Sub btnBuscarProducto_Click(sender As Object, e As EventArgs) Handles btnBuscarProducto.Click
        Dim codigoDisa As String = txtBuscarProducto.Text()
        Dim ds As New Data.DataSet

        If codigoDisa.Equals("") Then
            lblError.Text = "Ingresa algun dato valido"
        Else
            ds = InventarioAD.getSpecificProductInStock(ubicacion, codigoDisa)
            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay Productos con codigo " & codigoDisa

            End If

        End If
    End Sub

    Protected Sub bConteoManual_Click(sender As Object, e As EventArgs) Handles bConteoManual.Click
        For Each gvr As GridViewRow In Tabla1.Rows
            Dim tb As TextBox = DirectCast(gvr.FindControl("tbCantidad"), TextBox)
            Dim msg As String = tb.Text
            MsgBox(msg)
        Next
    End Sub
End Class