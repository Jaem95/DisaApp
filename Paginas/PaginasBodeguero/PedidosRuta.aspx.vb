﻿Public Class PedidosRuta
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ADPedidos As PedidosPendientesAD = New PedidosPendientesAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim pendingRequests As String
    Dim deliveredRequests As String
    Dim informacionUsuario As New List(Of String)
    Dim infoRepartidor As New List(Of List(Of String))
    Dim idRepartidor As String = ""
    Dim index As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim info As New List(Of String)
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        idRepartidor = Request.QueryString("idRepartidor")


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count > 0 Then
                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                pendingRequests = ADPedidos.getPendingRequests(ubicacion)
                deliveredRequests = ADPedidos.getDeliveredRequesst(ubicacion)

                infoRepartidor = ADPedidos.getRequestsRepartidor(idRepartidor, ubicacion)
                System.Diagnostics.Debug.WriteLine(infoRepartidor)

                info = infoRepartidor(index)

                lblName.Text = info(3)
                lblRuta.Text = info(2)
                lblPedido.Text = info(1)

                ds = ADPedidos.getRequestProducts(info(1))
                If ds.Tables(0).Rows.Count > 0 Then
                    Tabla1.DataSource = ds.Tables("tabla")
                    Tabla1.DataBind()
                Else
                End If

            End If
        End If

    End Sub

    'Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Tabla1.SelectedIndexChanging
    '    Dim Indice As Integer = e.NewSelectedIndex
    '    Dim idRepartidor As String = Tabla1.Rows(Indice).Cells(0).Text
    '    Response.Redirect("/Paginas/PaginasBodeguero/PedidosRuta.aspx?idRepartidor=" & idRepartidor.Trim())

    'End Sub
    Protected Sub btnLeftNav_Click(sender As Object, e As EventArgs) Handles btnLeftNav.Click
        Dim ds As New Data.DataSet
        Dim info As New List(Of String)
        If index > 0 Then
            info = New List(Of String)
            index -= 1
            info = infoRepartidor(index)

            lblName.Text = info(3)
            lblRuta.Text = info(2)
            lblPedido.Text = info(1)

            ds = ADPedidos.getRequestProducts(info(1))

            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()
            Else
            End If
        Else
        End If
        btnConfirmar.Text = "Confirmar Pedido"
    End Sub

    Protected Sub btnRightNav_Click(sender As Object, e As EventArgs) Handles btnRightNav.Click
        Dim info As New List(Of String)
        Dim ds As New Data.DataSet
        If index < infoRepartidor.Count Then
            info = New List(Of String)
            index += 1
            info = infoRepartidor(index)

            lblName.Text = info(3)
            lblRuta.Text = info(2)
            lblPedido.Text = info(1)

            ds = ADPedidos.getRequestProducts(info(1))

            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()
            Else
            End If

        Else
        End If

        If index.Equals(infoRepartidor.Count) Then
            btnConfirmar.Text = "Confimación de entrega total"
        Else
            btnConfirmar.Text = "Confirmar Pedido"
        End If

    End Sub

End Class