﻿Public Class Admin
    Inherits System.Web.UI.Page
    Dim ADAdmin As LoginAD = New LoginAD()
    Dim ADGeneral As ADGeneral = New ADGeneral()

    Dim usuario As String = ""
    Dim pwd As String = ""
    Dim ubicacion As String = ""
    Dim tipoUsuario As String = ""
    Dim informacionUsuario As New List(Of String)




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)

        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)


            lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)

            imageUsuario1.ImageUrl = informacionUsuario(13)
            imageUsuario2.ImageUrl = informacionUsuario(13)
            imageUsuario3.ImageUrl = informacionUsuario(13)








        End If





    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class