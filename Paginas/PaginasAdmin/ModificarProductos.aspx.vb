﻿Public Class ModificarProductos
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ModificarProdAD As ModificarProductosAD = New ModificarProductosAD()

    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionProducto As New List(Of String)

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Accion = Request.QueryString("Accion")


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                ds = ADGeneral.ConsultarClasificacion

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlClasificacion.DataSource = ds.Tables("ClasificacionProductos").DefaultView
                    ddlClasificacion.DataValueField = "id"
                    ddlClasificacion.DataTextField = "nombre"
                    ddlClasificacion.DataBind()

                End If

                ds = ADGeneral.ConsultarTipoDePieza

                'If ds.Tables(0).Rows.Count > 0 Then
                '    ddlTipoDePieza.DataSource = ds.Tables("TipoPieza").DefaultView
                '    ddlTipoDePieza.DataValueField = "id"
                '    ddlTipoDePieza.DataTextField = "tipo"
                '    ddlTipoDePieza.DataBind()

                'End If

                ds = ADGeneral.ConsultarProovedor

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlProovedor.DataSource = ds.Tables("Proovedor").DefaultView
                    ddlProovedor.DataValueField = "id"
                    ddlProovedor.DataTextField = "nombre"
                    ddlProovedor.DataBind()

                End If


                If Accion Is Nothing Then
                    lblError.Text = ""
                Else
                    txtBuscarProducto.Text = Request.QueryString("Accion").ToString()
                    informacionProducto = ADGeneral.ConsultarInformacionProducto(Request.QueryString("Accion").ToString())
                    PonerInformacionEnCampos(informacionProducto, "")


                End If

            End If


        End If

    End Sub



    Public Sub PonerInformacionEnCampos(informacion As List(Of String), codigoDeBarras As String)
        txtNombreProducto.Text = informacion(0)
        txtDescripcionProducto.Text = informacion(1)
        txtPresentacion.Text = informacion(2)
        txtCantidadPorCaja.Text = informacion(3)
        txtCodigoBarrasCaja.Text = informacion(4)
        txtCodigoBarrasPieza.Text = informacion(5)

        ddlClasificacion.SelectedValue = informacion(6)
        'ddlTipoDePieza.SelectedValue = informacion(7)
        ddlProovedor.SelectedValue = informacion(8)
        If codigoDeBarras.Equals("") Then
            txtCodigoDisa.Text = informacion(10)
        Else
            txtCodigoDisa.Text = informacion(10)
        End If

        If informacion(7).Equals("5") Then
            txtCodigoBarrasCaja.Enabled = False

        End If

    End Sub

    Protected Sub btnBuscarProductoEspecifico_Click(sender As Object, e As EventArgs) Handles btnBuscarProductoEspecifico.Click
        lblError.Text = ""
        lblErrorModificaciones.Text = ""

        Dim codigoDeBarras As String = txtBuscarProducto.Text()
        Dim informacionProducto As New List(Of String)

        If codigoDeBarras.Equals("") Then
            lblError.Text = "Codigo Disa Invalido!"
        Else
            informacionProducto = ADGeneral.ConsultarInformacionProducto(codigoDeBarras)

            If informacionProducto.Count > 0 Then
                PonerInformacionEnCampos(informacionProducto, codigoDeBarras)
            Else
                lblError.Text = "Codigo Inexistente"

            End If
        End If


    End Sub


    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        lblError.Text = ""
        lblErrorModificaciones.Text = ""

        Dim validacion As Boolean = False

        If txtCodigoBarrasCaja.Enabled = False Then
            If txtNombreProducto.Text.Equals("") Or txtDescripcionProducto.Text.Equals("") Or txtPresentacion.Text.Equals("") Or txtCantidadPorCaja.Text.Equals("") Or txtCodigoBarrasPieza.Text.Equals("") Or txtCodigoDisa.Text.Equals("") Then
                lblErrorModificaciones.Text = "Si vas actualizar informacion no puede dejar campos vacios!"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios');</script>", False)
            Else
                'lblErrorModificaciones.Text = "Vamos bien"
                validacion = ModificarProdAD.ActualizarInformacionProductos(txtNombreProducto.Text(), txtDescripcionProducto.Text(), txtPresentacion.Text(), txtCantidadPorCaja.Text(), txtCodigoBarrasPieza.Text(), txtCodigoBarrasCaja.Text(), txtCodigoDisa.Text(), ddlProovedor.SelectedValue().ToString(), ddlClasificacion.SelectedValue.ToString())
                If validacion Then
                    lblErrorModificaciones.Text = "Actualizacion Correcta"
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/Productos.aspx")

                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Incorrecta');</script>", False)
                End If
            End If
        Else

            If txtNombreProducto.Text.Equals("") Or txtDescripcionProducto.Text.Equals("") Or txtPresentacion.Text.Equals("") Or txtCantidadPorCaja.Text.Equals("") Or txtCodigoBarrasPieza.Text.Equals("") Or txtCodigoBarrasCaja.Text.Equals("") Or txtCodigoDisa.Text.Equals("") Then
                lblErrorModificaciones.Text = "Si vas actualizar informacion no puede dejar campos vacios!"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios');</script>", False)
            Else
                'lblErrorModificaciones.Text = "Vamos bien"
                validacion = ModificarProdAD.ActualizarInformacionProductos(txtNombreProducto.Text(), txtDescripcionProducto.Text(), txtPresentacion.Text(), txtCantidadPorCaja.Text(), txtCodigoBarrasPieza.Text(), txtCodigoBarrasCaja.Text(), txtCodigoDisa.Text(), ddlProovedor.SelectedValue().ToString(), ddlClasificacion.SelectedValue.ToString())
                If validacion Then
                    lblErrorModificaciones.Text = "Actualizacion Correcta"
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/Productos.aspx")

                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Incorrecta');</script>", False)
                End If
            End If


        End If



    End Sub

    Protected Sub btnCambiarFotoProducto_Click(sender As Object, e As EventArgs) Handles btnCambiarFotoProducto.Click
        lblError.Text = ""
        lblErrorModificaciones.Text = ""

        Dim path As String = Server.MapPath("~/ImagenesProductos/")
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""
        Dim codigoDisa As String = txtCodigoDisa.Text()

        If FileUpload1.HasFile Then
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                End If
            Next
            If fileOK Then
                Try
                    nombre = FileUpload1.FileName()
                    ruta = "~/ImagenesProductos/" & nombre

                    If ModificarProdAD.ActualizarURLFoto(txtCodigoDisa.Text(), ruta) Then
                        FileUpload1.PostedFile.SaveAs(path &
                        FileUpload1.FileName)
                        lblFoto.Text = "Foto Actualizado!!"
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Foto Actualizada');</script>", False)


                    Else
                        lblFoto.Text = "Error en la actualizacion!"
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en la actualizacion');</script>", False)

                    End If



                Catch ex As Exception
                    lblFoto.Text = "No se pudo subir el archivo!."
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en la actualizacion');</script>", False)
                End Try
            Else
                lblFoto.Text = "Solo fotos!."
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo Fotos');</script>", False)
            End If
        End If
    End Sub


    Protected Sub btmEliminarProducto_Click(sender As Object, e As EventArgs) Handles btmEliminarProducto.Click
        lblError.Text = ""
        lblErrorModificaciones.Text = ""


        Dim validacion As Boolean = False

        If txtCodigoDisa.Text.Equals("") Then
            lblErrorModificaciones.Text = "Si vas actualizar informacion no puede dejar campos vacios!"
        Else
            validacion = ModificarProdAD.EliminarProducto(txtCodigoBarrasPieza.Text)
            If validacion Then
                lblErrorModificaciones.Text = "Eliminiacion Correcta!"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Eliminacion Correcta');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/Productos.aspx")
            Else
                lblErrorModificaciones.Text = "Eliminicacion Fallida!"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Eliminacion Incorrecta, el producto esta ligado a pedidos');</script>", False)
            End If
        End If

    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub



End Class