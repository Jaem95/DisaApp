﻿Public Class RegistroProovedores
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProovedoresAD As ProovedoresAD = New ProovedoresAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionProovedor As New List(Of String)

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                ds = ADGeneral.ConsultarEstados()

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlEstadosDeMex.DataSource = ds.Tables("Estados").DefaultView
                    ddlEstadosDeMex.DataValueField = "id_estado"
                    ddlEstadosDeMex.DataTextField = "estado"
                    ddlEstadosDeMex.DataBind()

                End If


                'If Accion Is Nothing Then
                '    lblErrorModificaciones.Text = ""
                'Else
                '    txtCodigoProvedor.Text = Request.QueryString("ID").ToString()
                '    informacionProovedor = ProovedoresAD.ConsultarProveedores(Request.QueryString("ID").ToString())
                '    If informacionProovedor.Count > 0 Then
                '        PonerInformacionEnCampos(informacionProovedor)
                '    End If



                'End If

            End If


        End If

    End Sub



    'Public Sub PonerInformacionEnCampos(informacion As List(Of String))
    '    txtCodigoProvedor.Text = Request.QueryString("ID").ToString()
    '    txtNombreEmpresa.Text = informacion(0)
    '    txtNombreDelTitular.Text = informacion(1)
    '    txtDireccion.Text = informacion(2)
    '    txtColonia.Text = informacion(3)
    '    ddlEstadosDeMex.SelectedValue = informacion(4)
    '    txtMunicipio.Text = informacion(5)
    '    txtCodigoPostal.Text = informacion(6)
    '    txtNumeroTelefonico.Text = informacion(7)
    '    txtNumeroTelefonico2.Text = informacion(8)
    '    txtPaginaWeb.Text = informacion(9)
    '    txtCorreoElectronico.Text = informacion(10)
    '    txtRFC.Text = informacion(11)
    '    txtCuentaBancaria.Text = informacion(12)
    'End Sub




    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click

        Dim confirmValue As String = Request.Form("confirm_value")

        If confirmValue = "No" Then Return
        Dim nombreEmpreas As String = txtNombreEmpresa.Text
        Dim nombreTitular As String = txtNombreDelTitular.Text
        Dim direcionEmpresa As String = txtDireccion.Text
        Dim colonia As String = txtColonia.Text
        Dim estadoEmpresa As String = ddlEstadosDeMex.SelectedValue
        Dim empresaMunicipio As String = txtMunicipio.Text
        Dim codigoPostal As String = txtCodigoPostal.Text
        Dim numero1 As String = txtNumeroTelefonico.Text
        Dim numero2 As String = txtNumeroTelefonico2.Text
        Dim direcionWeb As String = txtPaginaWeb.Text
        Dim correoElectronico As String = txtCorreoElectronico.Text
        Dim rfc As String = txtRFC.Text
        Dim cuentaBancaria As String = txtCuentaBancaria.Text
        Dim validacion As Boolean = False

        If nombreEmpreas.Equals("") Or nombreTitular.Equals("") Or direcionEmpresa.Equals("") Or colonia.Equals("") Or estadoEmpresa.Equals("") Or empresaMunicipio.Equals("") Or codigoPostal.Equals("") Or Not IsNumeric(codigoPostal) Or numero1.Equals("") Or Not IsNumeric(numero1) Or Not IsNumeric(numero2) Or numero2.Equals("") Or direcionWeb.Equals("") Or rfc.Equals("") Or correoElectronico.Equals("") Or cuentaBancaria.Equals("") Or Not IsNumeric(cuentaBancaria) Then
            lblErrorModificaciones.Text = "Si vas registrar informacion no puede dejar campos vacios o ingresar datos invalidos!"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Si vas registrar informacion no puede dejar campos vacios o ingresar datos invalidos!');</script>", False)
        Else
            'lblErrorModificaciones.Text = "Vamos bien"
            validacion = ProovedoresAD.RegistrarProovedor(nombreEmpreas, nombreTitular, direcionEmpresa, colonia, estadoEmpresa, empresaMunicipio, codigoPostal, numero1, numero2, direcionWeb, correoElectronico, rfc, cuentaBancaria)
            If validacion Then
                lblErrorModificaciones.Text = "Actualizacion Correcta"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta de Regiones');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/Proovedores.aspx")
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La informacion es incorrecta!');</script>", False)
            End If
        End If
    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub


End Class