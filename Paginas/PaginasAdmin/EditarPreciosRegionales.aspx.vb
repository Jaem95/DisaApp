﻿Imports System
Imports System.Globalization

Public Class EditarPreciosRegionales
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProductosAD As ProductosAD = New ProductosAD()
    Dim EditarPrecios As EditarPreciosAD = New EditarPreciosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""
    Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionProducto As New List(Of String)
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Accion = Request.QueryString("Accion")

        If IsPostBack Then Return

        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)




            End If

            If Not IsPostBack Then
                ds = ADGeneral.ConsultarRegiones()
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlRegion.DataSource = ds.Tables("Region").DefaultView
                    ddlRegion.DataValueField = "id"
                    ddlRegion.DataTextField = "nombre"
                    ddlRegion.DataBind()

                End If
            End If



            If Not Accion Is Nothing Then

                txtCodigoBarras.Text = Request.QueryString("Accion").ToString()
                informacionProducto = ADGeneral.ConsultarInformacionProducto(Request.QueryString("Accion").ToString())
                txtNombreProducto.Text = informacionProducto(0)
                txtDescProducto.Text = informacionProducto(1)
                txtPresentacionProducto.Text = informacionProducto(2)
                txtCantidadPorCaja.Text = informacionProducto(3)
                txtClasificacion.Text = informacionProducto(6)
                txtCodigoBarras.Text = Request.QueryString("Accion").ToString()
                imagenProducto.ImageUrl = informacionProducto(9)
                txtIDClas.Text = informacionProducto(11)

            End If


        End If
        BuscarPrecioRegion(ddlRegion.SelectedValue)
    End Sub

    Private Sub BuscarPrecioRegion(selectedValue As String)


        Dim precioPorRegion As New List(Of String)

        precioPorRegion = EditarPrecios.ConsultarPreciosPorRegion(Request.QueryString("Accion"), selectedValue)
        If precioPorRegion.Count() > 0 Then

            txtPrecioRI1.Text = precioPorRegion(0)
            txtPrecioRF1.Text = precioPorRegion(1)
            txtPrecio1.Text = precioPorRegion(2)

            txtPrecioRI2.Text = precioPorRegion(3)
            txtPrecioRF2.Text = precioPorRegion(4)
            txtPrecio2.Text = precioPorRegion(5)


            txtPrecioRI3.Text = precioPorRegion(6)
            txtPrecioRF3.Text = precioPorRegion(7)
            txtPrecio3.Text = precioPorRegion(8)
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Este  producto no tiene precio en esta region');</script>", False)
            txtPrecioRI1.Text = ""
            txtPrecioRF1.Text = ""
            txtPrecio1.Text = ""

            txtPrecioRI2.Text = ""
            txtPrecioRF2.Text = ""
            txtPrecio2.Text = ""


            txtPrecioRI3.Text = ""
            txtPrecioRF3.Text = ""
            txtPrecio3.Text = ""
        End If

    End Sub

    Private Sub BuscarPrecioGeneral()
        Dim precioPorRegion As New List(Of String)

        precioPorRegion = EditarPrecios.ConsultarPreciosGenerales(Request.QueryString("Accion"))
        If precioPorRegion.Count() > 0 Then

            txtPrecioRI1.Text = precioPorRegion(0)
            txtPrecioRF1.Text = precioPorRegion(1)
            txtPrecio1.Text = precioPorRegion(2)

            txtPrecioRI2.Text = precioPorRegion(3)
            txtPrecioRF2.Text = precioPorRegion(4)
            txtPrecio2.Text = precioPorRegion(5)


            txtPrecioRI3.Text = precioPorRegion(6)
            txtPrecioRF3.Text = precioPorRegion(7)
            txtPrecio3.Text = precioPorRegion(8)
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Este  producto no tiene precio general');</script>", False)
            txtPrecioRI1.Text = ""
            txtPrecioRF1.Text = ""
            txtPrecio1.Text = ""

            txtPrecioRI2.Text = ""
            txtPrecioRF2.Text = ""
            txtPrecio2.Text = ""


            txtPrecioRI3.Text = ""
            txtPrecioRF3.Text = ""
            txtPrecio3.Text = ""
        End If

    End Sub

    Public Sub PonerInformacionEnCampos(informacion As List(Of String), codigoBarras As String)
        txtNombreProducto.Text = informacion(0)
        txtDescProducto.Text = informacion(1)
        txtPresentacionProducto.Text = informacion(2)
        txtCantidadPorCaja.Text = informacion(3)
        txtClasificacion.Text = informacion(6)
        If codigoBarras.Equals("") Then
            txtCodigoBarras.Text = Request.QueryString("Accion").ToString()
        Else
            txtCodigoBarras.Text = codigoBarras
        End If

        imagenProducto.ImageUrl = informacion(9)

    End Sub


    Protected Sub rbtnPrecioGeneral_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim precioPorRegion As New List(Of String)

        If rbtnPrecio.SelectedValue = "PrecioGeneral" Then
            rbtnPrecio.Items(1).Selected = False
            'Traigo lso precios generales del producto
            BuscarPrecioGeneral()
            txtPrecioRI1.Enabled = False

            txtPrecioRF1.Enabled = False
            txtPrecio1.Enabled = False

            txtPrecioRI2.Enabled = False
            txtPrecioRF2.Enabled = False

            txtPrecio2.Enabled = False



            txtPrecioRI3.Enabled = False

            txtPrecioRF3.Enabled = False
            txtPrecio3.Enabled = False

        ElseIf rbtnPrecio.SelectedValue = "PrecioRegional" Then
            rbtnPrecio.Items(0).Selected = False
            BuscarPrecioRegion(ddlRegion.SelectedValue)
            txtPrecioRI1.Enabled = True
            txtPrecioRF1.Enabled = True
            txtPrecio1.Enabled = True
            txtPrecioRI2.Enabled = True
            txtPrecioRF2.Enabled = True
            txtPrecio2.Enabled = True
            txtPrecioRI3.Enabled = True
            txtPrecioRF3.Enabled = True
            txtPrecio3.Enabled = True
        End If
    End Sub


    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return

        If Not IsNumeric(txtPrecio1.Text) Or Not IsNumeric(txtPrecio2.Text) Or Not IsNumeric(txtPrecio3.Text) Or Not IsNumeric(txtPrecioRI1.Text) Or Not IsNumeric(txtPrecioRI2.Text) Or Not IsNumeric(txtPrecioRI3.Text) Or Not IsNumeric(txtPrecioRF1.Text) Or Not IsNumeric(txtPrecioRF2.Text) Or Not IsNumeric(txtPrecioRF3.Text) Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo numeros en los rangos de precio!');</script>", False)
        Else
            If rbtnPrecio.SelectedValue = "" Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Selecciona el precio a aplicar!');</script>", False)
            Else
                tipoPrecio = rbtnPrecio.SelectedValue()
                If EditarPrecios.BorraPreciosViejos(Request.QueryString("Accion").ToString(), ddlRegion.SelectedValue(), txtIDClas.Text) Then
                    If EditarPrecios.RegistrarPrecios(Request.QueryString("Accion").ToString(), txtPrecio1.Text, txtPrecioRI1.Text(), txtPrecioRF1.Text(), ddlRegion.SelectedValue()) Then
                        If EditarPrecios.RegistrarPrecios(Request.QueryString("Accion").ToString(), txtPrecio2.Text, txtPrecioRI2.Text(), txtPrecioRF2.Text(), ddlRegion.SelectedValue()) Then
                            If EditarPrecios.RegistrarPrecios(Request.QueryString("Accion").ToString(), txtPrecio3.Text, txtPrecioRI3.Text(), txtPrecioRF3.Text(), ddlRegion.SelectedValue()) Then
                                If EditarPrecios.ActualizarBanderaPrecio(Request.QueryString("Accion").ToString(), tipoPrecio, ddlRegion.SelectedValue()) Then
                                    ''VER PORQUE LA TABLA DE PRECIOS TIENE REGION
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('REGISTRO DE PRECIO REGIONAL PARA  EL PRODUCTO " & Request.QueryString("Accion").ToString() & " EN LA REGION: " + ddlRegion.SelectedItem.ToString + "');window.location='PreciosGenerales.aspx';</script>", False)
                                Else
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                                End If
                            Else
                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                            End If
                        Else
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)

                        End If
                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                    End If
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                End If

            End If

        End If

        txtNombreProducto.Attributes.Add("value", txtNombreProducto.Text())


    End Sub

    Protected Sub ddlRegion_SelectedIndexChanged(sender As Object, e As EventArgs)
        rbtnPrecio.SelectedValue = Nothing
        txtPrecioRI1.Enabled = True
        txtPrecioRF1.Enabled = True
        txtPrecio1.Enabled = True
        txtPrecioRI2.Enabled = True
        txtPrecioRF2.Enabled = True
        txtPrecio2.Enabled = True
        txtPrecioRI3.Enabled = True
        txtPrecioRF3.Enabled = True
        txtPrecio3.Enabled = True
        BuscarPrecioRegion(ddlRegion.SelectedValue)

    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class


