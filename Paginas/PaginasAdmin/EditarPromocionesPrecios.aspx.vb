﻿Imports System
Imports System.Globalization
Imports System.Data
Imports System.Data.SqlClient

Public Class EditarPromocionesPrecios
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim PromocionesAD As PromocionesAD = New PromocionesAD()
    Dim ProductosAD As ADGeneral = New ADGeneral()
    Dim EditarPrecios As EditarPreciosAD = New EditarPreciosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionCategoria As New List(Of String)
        Dim informacionProducto As New List(Of String)
        Dim informacionPromocion As New List(Of String)
        Dim precioProducto As New List(Of String)


        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Dim CB As String = Request.QueryString("CB").ToString()
        Dim idPromocion As String = Request.QueryString("ID").ToString()


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)


                If Not CB Is Nothing Then
                    'Busco la informacion del producto 
                    informacionProducto = ProductosAD.ConsultarInformacionProducto(Request.QueryString("CB").ToString())
                    If informacionProducto.Count() > 0 Then
                        txtCodigoDisa.Text = informacionProducto(10)
                        txtProducto.Text = informacionProducto(0)
                        txtDescripcion.Text = informacionProducto(1)
                        txtPresentacion.Text = informacionProducto(2)
                        txtCantidadCaja.Text = informacionProducto(3)
                        imagenCategoria.ImageUrl = informacionProducto(9)

                        If Not idPromocion Is Nothing Then
                            informacionPromocion = PromocionesAD.ConsultarInformacionPromo(idPromocion, "Descuento")
                            If informacionPromocion.Count > 0 Then
                                txtLugar.Text = informacionPromocion(0)
                                txtFechaInicial.Text = informacionPromocion(1)
                                txtFechaFinal.Text = informacionPromocion(2)
                                txtPorcentajeDescuento.Text = informacionPromocion(3) + "%"
                                txtTipoPromocion.Text = "Descuento"
                                If informacionPromocion(0).Equals("General") Then
                                    Dim script As String = "mostrarGeneral();"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)

                                    precioProducto = EditarPrecios.ConsultarPreciosGenerales(CB)

                                    If precioProducto.Count > 0 Then

                                        lblR1.Text = precioProducto(0) & "-" & precioProducto(1)
                                        lblR2.Text = precioProducto(3) & "-" & precioProducto(4)
                                        lblR3.Text = precioProducto(6) & "-" & precioProducto(7)




                                        lblPR1.Text = precioProducto(2)
                                        lblPR2.Text = precioProducto(5)
                                        lblPR3.Text = precioProducto(8)





                                    End If

                                ElseIf informacionPromocion(0).Equals("Regional") Then
                                    Dim script As String = "mostrarRegional();"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
                                    precioProducto = EditarPrecios.ConsultarPreciosPorRegion(CB, informacionPromocion(4))
                                    If precioProducto.Count > 0 Then



                                        lblRango1Regional.Text = precioProducto(0) & "-" & precioProducto(1)
                                        lblRango2Regional.Text = precioProducto(3) & "-" & precioProducto(4)
                                        lblRango3Regional.Text = precioProducto(6) & "-" & precioProducto(7)


                                        lblPrecio1Regional.Text = precioProducto(2)
                                        lblPrecio2Regional.Text = precioProducto(5)
                                        lblPrecio3Regional.Text = precioProducto(8)



                                    End If

                                End If
                            End If
                        End If

                    End If


                End If

            End If


        End If


    End Sub


    Protected Sub btnCalcularPrecioGeneral_Click(sender As Object, e As EventArgs) Handles btnCalcularPrecioGeneral.Click
        Dim resultado As Double = 0
        Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat

        If txtDescuento.Text.Equals("") Then
            lblErrorPrecioGeneral.Text = "Ingresa un monto valido"
        Else
            resultado = lblPR1.Text - (lblPR1.Text() * (txtDescuento.Text / 100))
            lblPrecioCalculado1.Text = resultado.ToString("C", nfi)

            resultado = lblPR2.Text - (lblPR2.Text() * (txtDescuento.Text / 100))
            lblPrecioCalculado2.Text = resultado.ToString("C", nfi)

            resultado = lblPR3.Text - (lblPR3.Text() * (txtDescuento.Text / 100))
            lblPrecioCalculado3.Text = resultado.ToString("C", nfi)


        End If


    End Sub

    Protected Sub btnCalcularPrecioRegional_Click(sender As Object, e As EventArgs) Handles btnCalcularPrecioRegional.Click
        Dim resultado As Double = 0
        Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat

        If txtDescuentoRegional.Text.Equals("") Then
            lblModalErrorRegional.Text = "Ingresa un Monto Valido"
        Else
            resultado = lblPrecio1Regional.Text - (lblPrecio1Regional.Text() * (txtDescuentoRegional.Text / 100))
            lblPrecioRegionalCalculado.Text = resultado.ToString("C", nfi)

            resultado = lblPrecio2Regional.Text - (lblPrecio2Regional.Text() * (txtDescuentoRegional.Text / 100))
            lblPrecioRegionalCalculado2.Text = resultado.ToString("C", nfi)

            resultado = lblPrecio3Regional.Text - (lblPrecio3Regional.Text() * (txtDescuentoRegional.Text / 100))
            lblPrecioRegionalCalculado3.Text = resultado.ToString("C", nfi)
        End If





    End Sub

    Protected Sub btnActualizarPromo_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return
        lblErrorPrecioGeneral.Text = ""
        Dim idPromo = Request.QueryString("ID").ToString()
        Dim codigoBarras = Request.QueryString("CB").ToString()
        Dim montoAplicar As String = txtDescuento.Text()
        Dim fechaNueva As String = calendarioFechaTermino.SelectedDate.ToShortDateString
        Dim fechaAntigua As String = txtFechaFinal.Text
        Dim script As String = "actualizar();"

        If montoAplicar.Equals("") Then
            lblErrorPrecioGeneral.Text = "Ingresa un Monto Valido"
        Else
            If fechaNueva.Equals("01/01/0001") Or fechaNueva.Equals("1/1/0001") Then
                'Registro Sin Actualizacion de Fecha
                If PromocionesAD.ActualizarPromo(idPromo, codigoBarras, fechaAntigua, montoAplicar) Then
                    lblErrorPrecioGeneral.Text = "La promocion se actualizo con este monto:" + montoAplicar
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + "');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
                End If
            Else
                If Convert.ToDateTime(fechaNueva) >= Convert.ToDateTime(fechaAntigua) Then
                    'Actulizare con Fecha
                    If PromocionesAD.ActualizarPromo(idPromo, codigoBarras, fechaNueva, montoAplicar) Then
                        lblErrorPrecioGeneral.Text = "La promocion se actualizo con este monto:" + montoAplicar + " y nueva fecha:" + fechaNueva
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + " y nueva fecha:" + fechaNueva + "');</script>", False)
                        Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
                    End If
                Else
                    lblErrorPrecioGeneral.Text = "La nueva fecha de termino debe de ser mayor a ya la registrada, la fecha seleccionada es = " + fechaNueva + ""
                End If
            End If

        End If





    End Sub
    Protected Sub btnActualizarPromoRegional_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return
        lblModalErrorRegional.Text = ""
        Dim idPromo = Request.QueryString("ID").ToString()
        Dim codigoBarras = Request.QueryString("CB").ToString()
        Dim montoAplicar As String = txtDescuentoRegional.Text()
        Dim fechaNueva As String = calendarioFechaTerminoRegional.SelectedDate.ToShortDateString
        Dim fechaAntigua As String = txtFechaFinal.Text
        Dim script As String = "actualizar();"

        If montoAplicar.Equals("") Then
            lblModalErrorRegional.Text = "Ingresa un Monto Valido"
        Else
            If fechaNueva.Equals("01/01/0001") Or fechaNueva.Equals("1/1/0001") Then
                'Registro Sin Actualizacion de Fecha
                If PromocionesAD.ActualizarPromo(idPromo, codigoBarras, fechaAntigua, montoAplicar) Then
                    lblModalErrorRegional.Text = "La promocion se actualizo con este monto:" + montoAplicar
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + "');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
                End If
            Else
                If Convert.ToDateTime(fechaNueva) >= Convert.ToDateTime(fechaAntigua) Then
                    'Actulizare con Fecha
                    If PromocionesAD.ActualizarPromo(idPromo, codigoBarras, fechaNueva, montoAplicar) Then
                        lblModalErrorRegional.Text = "La promocion se actualizo con este monto:" + montoAplicar + " y nueva fecha:" + fechaNueva
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + " y nueva fecha:" + fechaNueva + "');</script>", False)
                        Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
                    End If
                Else
                    lblModalErrorRegional.Text = "La nueva fecha de termino debe de ser mayor a ya la registrada, la fecha seleccionada es = " + fechaNueva + ""
                End If
            End If

        End If

    End Sub



    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub

    Protected Sub btnEliminarPromocion_Click(sender As Object, e As EventArgs)
        Dim validacion As Boolean
        validacion = PromocionesAD.EliminarPromocionPrecio(Request.QueryString("ID").ToString())
        If validacion Then
            Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Eliminacion incorrecta!');</script>", False)

        End If
    End Sub
End Class