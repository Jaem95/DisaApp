﻿Imports System
Imports System.Globalization
Imports System.Data
Imports System.Data.SqlClient
'Rama de devJaem
Public Class EditarPromocionesProductos
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim PromocionesAD As PromocionesAD = New PromocionesAD()
    Dim ProductosAD As ADGeneral = New ADGeneral()
    Dim EditarPrecios As EditarPreciosAD = New EditarPreciosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionCategoria As New List(Of String)
        Dim informacionProducto As New List(Of String)
        Dim informacionPromocion As New List(Of String)
        Dim precioProducto As New List(Of String)


        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Dim CB As String = Request.QueryString("CB").ToString()
        Dim idPromocion As String = Request.QueryString("ID").ToString()


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)


                If Not CB Is Nothing Then
                    'Busco la informacion del producto 
                    informacionProducto = ProductosAD.ConsultarInformacionProducto(Request.QueryString("CB").ToString())
                    If informacionProducto.Count() > 0 Then
                        txtCodigoDisa.Text = informacionProducto(10)
                        txtProducto.Text = informacionProducto(0)
                        txtDescripcion.Text = informacionProducto(1)
                        txtPresentacion.Text = informacionProducto(2)
                        txtCantidadCaja.Text = informacionProducto(3)
                        imagenCategoria.ImageUrl = informacionProducto(9)

                        If Not idPromocion Is Nothing Then
                            informacionPromocion = PromocionesAD.ConsultarInformacionPromo(idPromocion, "Producto")

                            If informacionPromocion.Count > 0 Then
                                txtLugar.Text = informacionPromocion(0)
                                txtFechaInicial.Text = informacionPromocion(1)
                                txtFechaFinal.Text = informacionPromocion(2)
                                txtPorcentajeDescuento.Text = informacionPromocion(3)
                                txtTipoPromocion.Text = "Regalo de Producto"

                                If informacionPromocion(0).Equals("General") Then
                                    Dim script As String = "mostrarGeneral();"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
                                    txtCantidadMinimaGeneral.Text = informacionPromocion(4)
                                    txtCantidadRegaloGeneral.Text = informacionPromocion(5)
                                    ds = PromocionesAD.ConsultarProductos

                                    If ds.Tables(0).Rows.Count > 0 Then
                                        ddlProductoRegaloGeneral.DataSource = ds.Tables("Productos").DefaultView
                                        ddlProductoRegaloGeneral.DataValueField = "codigo_barrras"
                                        ddlProductoRegaloGeneral.DataTextField = "nombre"
                                        ddlProductoRegaloGeneral.DataBind()
                                        ddlProductoRegaloGeneral.SelectedValue = informacionPromocion(3)


                                    End If







                                ElseIf informacionPromocion(0).Equals("Regional") Then
                                    Dim script As String = "mostrarRegional();"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
                                    txtCantidadMinimaRegional.Text = informacionPromocion(4)
                                    txtCantidadRegalo.Text = informacionPromocion(5)

                                    ds = PromocionesAD.ConsultarProductos
                                    If ds.Tables(0).Rows.Count > 0 Then
                                        ddlproductoRegaloRegional.DataSource = ds.Tables("Productos").DefaultView
                                        ddlproductoRegaloRegional.DataValueField = "codigo_barrras"
                                        ddlproductoRegaloRegional.DataTextField = "nombre"
                                        ddlproductoRegaloRegional.DataBind()
                                        ddlproductoRegaloRegional.SelectedValue = informacionPromocion(3)


                                    End If

                                    ds = ADGeneral.ConsultarRegiones()
                                    If ds.Tables(0).Rows.Count > 0 Then
                                        ddlRegionesRegalo.DataSource = ds.Tables("Region").DefaultView
                                        ddlRegionesRegalo.DataValueField = "id"
                                        ddlRegionesRegalo.DataTextField = "nombre"
                                        ddlRegionesRegalo.DataBind()
                                        ddlRegionesRegalo.SelectedValue = informacionPromocion(6)



                                    End If



                                End If
                            End If
                        End If

                    End If


                End If

            End If


        End If


    End Sub

    Protected Sub btnRegistrarPromoRegaloGeneral_Click(sender As Object, e As EventArgs)
        Dim cbProductoRegalo As String = ddlProductoRegaloGeneral.SelectedValue()
        Dim localidad As String = "1000001"
        Dim fechaTermino As String = calendarioFinalPromoRegaloGeneral.SelectedDate.ToShortDateString
        Dim compraMinima As String = txtCantidadMinimaGeneral.Text
        Dim cantidadRegal As String = txtCantidadRegaloGeneral.Text

        If compraMinima.Equals("") Or cantidadRegal.Equals("") Or Not IsNumeric(compraMinima) Or Not IsNumeric(cantidadRegal) Then
            lblRegaloGeneral.Text = "Ingresa Montos Validos"
        Else
            If fechaTermino.Equals("01/01/0001") Or fechaTermino.Equals("1/1/0001") Then
                'Registro Sin Actualizacion de Fecha
                If PromocionesAD.ActualizarPromoRegalo(Request.QueryString("ID").ToString(), cbProductoRegalo, localidad, txtFechaFinal.Text, compraMinima, cantidadRegal) Then
                    lblRegaloGeneral.Text = "La promocion se actualizo "
                    'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + "');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
                End If
            Else
                If Convert.ToDateTime(DateTime.Parse(fechaTermino).ToString("MM-dd-yyyy")) >= Convert.ToDateTime(txtFechaFinal.Text) Then
                    'Actulizare con Fecha
                    If PromocionesAD.ActualizarPromoRegalo(Request.QueryString("ID").ToString(), cbProductoRegalo, localidad, fechaTermino, compraMinima, cantidadRegal) Then
                        lblRegaloGeneral.Text = "La promocion se actualizo "
                        'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + "');</script>", False)
                        Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
                    End If
                Else
                    lblRegaloGeneral.Text = "La nueva fecha debe de ser mayor a la ya seleccionada, la fecha seleccionada es = " + fechaTermino + ""



                End If

            End If


            End If
    End Sub

    Protected Sub btnRegistrarPromoRegaloRegional_Click(sender As Object, e As EventArgs)
        Dim cbProductoRegalo As String = ddlproductoRegaloRegional.SelectedValue()
        Dim localidad As String = ddlRegionesRegalo.SelectedValue
        Dim fechaTermino As String = calendarioFinalRegaloRegional.SelectedDate.ToShortDateString
        Dim compraMinima As String = txtCantidadMinimaRegional.Text
        Dim cantidadRegal As String = txtCantidadRegalo.Text

        If compraMinima.Equals("") Or cantidadRegal.Equals("") Or Not IsNumeric(compraMinima) Or Not IsNumeric(cantidadRegal) Then
            lblRegaloRegional.Text = "Ingresa Montos Validos"
        Else
            If fechaTermino.Equals("01/01/0001") Or fechaTermino.Equals("1/1/0001") Then
                'Registro Sin Actualizacion de Fecha
                If PromocionesAD.ActualizarPromoRegalo(Request.QueryString("ID").ToString(), cbProductoRegalo, localidad, txtFechaFinal.Text, compraMinima, cantidadRegal) Then
                    lblRegaloRegional.Text = "La promocion se actualizo "
                    'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + "');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La region que seleccionaste ya tiene una promocion con este producto');</script>", False)
                End If
            Else
                If Convert.ToDateTime(DateTime.Parse(fechaTermino).ToString("MM-dd-yyyy")) >= Convert.ToDateTime(txtFechaFinal.Text) Then
                    'Actulizare con Fecha
                    If PromocionesAD.ActualizarPromoRegalo(Request.QueryString("ID").ToString(), cbProductoRegalo, localidad, fechaTermino, compraMinima, cantidadRegal) Then
                        lblRegaloRegional.Text = "La promocion se actualizo "
                        'ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + montoAplicar + "');</script>", False)
                        Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La region que seleccionaste ya tiene una promocion con este producto');</script>", False)

                    End If
                Else
                    lblRegaloRegional.Text = "La nueva fecha debe de ser mayor a la ya seleccionada, la fecha seleccionada es = " + fechaTermino + ""



                End If

            End If


        End If
    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub

    Protected Sub btnEliminarPromocion_Click(sender As Object, e As EventArgs)
        Dim validacion As Boolean
        validacion = PromocionesAD.EliminaPromocionProducto(Request.QueryString("ID").ToString())
        If validacion Then
            Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Eliminacion incorrecta!');</script>", False)

        End If
    End Sub
End Class