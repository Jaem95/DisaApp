﻿Imports System
Imports System.Globalization
Imports System.Data
Imports System.Data.SqlClient
Public Class EditarEquivalencia
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProductosAD As ADGeneral = New ADGeneral()
    Dim ADPuntos As DisaPuntosAD = New DisaPuntosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionProducto As New List(Of String)
        Dim informacionEquivalencia As New List(Of String)

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Dim idEquivalencia As String = Request.QueryString("ID").ToString()


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)


                If Not ID Is Nothing Then
                    informacionEquivalencia = ADPuntos.ConsultarProducto(idEquivalencia)
                    informacionProducto = ProductosAD.ConsultarInformacionProducto(informacionEquivalencia(0))
                    If informacionProducto.Count() > 0 Then
                        txtCodigoDisa.Text = informacionProducto(10)
                        txtProducto.Text = informacionProducto(0)
                        txtDescripcion.Text = informacionProducto(1)
                        txtPresentacion.Text = informacionProducto(2)
                        txtCantidadCaja.Text = informacionProducto(3)
                        imagenCategoria.ImageUrl = informacionProducto(9)
                        txtEquivelencia.Text = informacionEquivalencia(1)
                    End If


                End If

            End If


        End If


    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs)
        Dim nuevaEquivelencia As String = txtEquivelencia.Text
        Dim validacion As Boolean = False
        If nuevaEquivelencia.Equals("") Or Not IsNumeric(nuevaEquivelencia) Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Ingresa un parametro valido');</script>", False)
        Else
            validacion = ADPuntos.ActualizarEquivalencia(Request.QueryString("ID").ToString(), nuevaEquivelencia)
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/Puntos.aspx")
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error');</script>", False)
            End If
        End If
    End Sub

    Protected Sub btnEliminarEquivalencia_Click(sender As Object, e As EventArgs)
        Dim nuevaEquivelencia As String = txtEquivelencia.Text
        Dim validacion As Boolean = False
        If nuevaEquivelencia.Equals("") Or Not IsNumeric(nuevaEquivelencia) Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Ingresa un parametro valido');</script>", False)
        Else
            validacion = ADPuntos.EliminarEquivalencia(Request.QueryString("ID").ToString())
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/Puntos.aspx")
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error');</script>", False)
            End If
        End If
    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class