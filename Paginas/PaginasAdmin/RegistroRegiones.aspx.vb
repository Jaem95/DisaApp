﻿Public Class RegistroRegiones
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim RegistroRegionesAD As RegistroRegionesAD = New RegistroRegionesAD()
    Dim EditarPrecios As EditarPreciosAD = New EditarPreciosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)

        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)
            lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            imageUsuario1.ImageUrl = informacionUsuario(13)
            imageUsuario2.ImageUrl = informacionUsuario(13)
            imageUsuario3.ImageUrl = informacionUsuario(13)

            ds = ADGeneral.ConsultarEstados()

            If ds.Tables(0).Rows.Count > 0 Then
                ddlEstadosDeMex.DataSource = ds.Tables("Estados").DefaultView
                ddlEstadosDeMex.DataValueField = "id_estado"
                ddlEstadosDeMex.DataTextField = "estado"
                ddlEstadosDeMex.DataBind()

            End If
        End If
    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return

        lblErrorModificaciones.Text = ""
        Dim validacion As Boolean = False
        Dim idGenerado As Integer = 0

        Dim path As String = Server.MapPath("~/ImagenesRegiones/")
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""


        If FileUpload1.HasFile Then
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                End If
            Next
            If fileOK Then

                If txtRFC.Text.Equals("") Or txtNomrbreRegion.Text.Equals("") Or txtDireccion.Text.Equals("") Or txtColonia.Text.Equals("") Or txtMunicipio.Text.Equals("") Or txtCodigoPostal.Text.Equals("") Or txtNumeroTelefonico.Text.Equals("") Or txtCodigoGerente.Text.Equals("") Or txtNombreDelGerente.Text.Equals("") Or txtSuperficeBodega.Text.Equals("") Or txtCapacidadCajas.Text.Equals("") Then
                    lblErrorModificaciones.Text = "En el Registro no puedes dejar campos vacios!"
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios!');</script>", False)
                Else
                    idGenerado = RegistroRegionesAD.RegistrarRegion(txtRFC.Text(), txtNomrbreRegion.Text(), txtDireccion.Text(), txtColonia.Text(), txtMunicipio.Text(), ddlEstadosDeMex.SelectedValue(), txtCodigoPostal.Text(), txtNumeroTelefonico.Text(), txtCodigoGerente.Text(), txtNombreDelGerente.Text(), txtSuperficeBodega.Text(), txtCapacidadCajas.Text())
                    If idGenerado.ToString() IsNot "" And idGenerado > 0 Then
                        If RegistroRegionesAD.CrearTablas(idGenerado.ToString.Trim()) Then
                            Try
                                nombre = FileUpload1.FileName()
                                ruta = "~/ImagenesRegiones/" & nombre

                                If RegistroRegionesAD.SubirFoto(idGenerado, ruta) Then
                                    FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)

                                    lblErrorModificaciones.Text = "ALTA DE REGION CORRECTA!!"
                                    Dim direc As String = "www.google.com.mx"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Alta de Region correcta, el ID que se genero fue: " & idGenerado & "');</script>", False)
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script  type='text/javascript'> window.location=" + direc + "", False)

                                Else

                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en creacion de Regiones');</script>", False)

                                End If

                            Catch ex As Exception
                                lblErrorModificaciones.Text = "Error en el registro de region!"
                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registo!');</script>", False)

                            End Try
                        Else
                            lblErrorModificaciones.Text = "Error en el registro de regiones!"
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro!,Las tablas no han sido creadas!');</script>", False)

                        End If
                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro,El usuario DISA no existe');</script>", False)
                    End If
                End If

            Else
                lblFoto.Text = "Solo fotos!."
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo imagenes!');</script>", False)


            End If
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Tienes que Seleccionar una foto!!');</script>", False)
            lblErrorModificaciones.Text = "Tienes que seleccionar una foto!"


        End If

    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub

End Class