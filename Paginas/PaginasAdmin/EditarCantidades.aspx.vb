﻿Imports System.Globalization

Public Class EditarCantidades
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProductosAD As ProductosAD = New ProductosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionProducto As New List(Of String)

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Accion = Request.QueryString("Accion")


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)


                If Accion Is Nothing Then

                Else
                    txtCodigoDisa.Text = Request.QueryString("Accion").ToString()
                    informacionProducto = ADGeneral.ConsultarInformacionProducto(Request.QueryString("Accion").ToString())
                    PonerInformacionEnCampos(informacionProducto, "")
                    labelUbicacion.Text = "La informacion seria modificada en el inventario de la Region: " & Request.QueryString("Region").ToString()
                    txtCantidadLimiteEditar.Text = Request.QueryString("CM").ToString()


                End If

            End If


        End If
    End Sub

    Public Sub PonerInformacionEnCampos(informacion As List(Of String), codigoDisa As String)
        txtNombreProducto.Text = informacion(0)
        txtDescProducto.Text = informacion(1)
        txtPresentacionProducto.Text = informacion(2)
        txtCantidadPorCaja.Text = informacion(3)
        txtClasificacion.Text = informacion(6)
        If codigoDisa.Equals("") Then
            txtCodigoDisa.Text = Request.QueryString("Accion").ToString()
        Else
            txtCodigoDisa.Text = codigoDisa
        End If

        imagenProducto.ImageUrl = informacion(9)

    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        Dim validacion As Boolean = False
        Dim bln As Boolean = True
        Dim fecha As String = ""
        Dim dateChek As Boolean = False

        If txtCantidadLimiteEditar.Text.Equals("") Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios');</script>", False)
        Else
            fecha = CalendarioCaducidadLimite.SelectedDate.ToShortDateString
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Seleccione una fecha," + fecha + "  no es una entrada valida.');</script>", False)
            If fecha.Equals("01/01/0001") Or fecha.Equals("1/1/0001") Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Seleccione una fecha," + fecha + "  no es una entrada valida.');</script>", False)
            Else
                validacion = ProductosAD.ActualizarCaducidadCantidadLimite(Request.QueryString("Accion").ToString(), txtCantidadLimiteEditar.Text(), DateTime.Parse(fecha).ToString("yyyy-MM-dd"), Request.QueryString("Region").ToString())
                If validacion Then
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/Caducidades.aspx")
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Incorrecta');</script>", False)
                End If
            End If





        End If

    End Sub
    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class