﻿Public Class Puntos
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ADPuntos As DisaPuntosAD = New DisaPuntosAD()
    Dim PromocionesAD As PromocionesAD = New PromocionesAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ds As New Data.DataSet
        Dim nuevaUbicacion As String = ""
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)

        If IsPostBack Then Return

        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                ds = ADPuntos.ConsultarDisaPuntos()

                If ds.Tables(0).Rows.Count > 0 Then
                    Tabla1.DataSource = ds.Tables("tabla")
                    Tabla1.DataBind()
                    lblEquivalenciaXPCompraActual.Text = ADPuntos.ConsultarEquivalenciaActual()


                Else
                    Tabla1.DataSourceID = Nothing
                    Tabla1.DataSource = Nothing
                    Tabla1.DataBind()
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No hay equivalencia de DISA Puntos aun');</script>", False)




                End If

                ds = PromocionesAD.ConsultarProductos

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlProductoParaPuntos.DataSource = ds.Tables("Productos").DefaultView
                    ddlProductoParaPuntos.DataValueField = "codigo_barrras"
                    ddlProductoParaPuntos.DataTextField = "nombre"
                    ddlProductoParaPuntos.DataBind()


                End If


            End If
        End If


    End Sub



    Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Tabla1.SelectedIndexChanging
        Dim Indice As Integer = e.NewSelectedIndex
        Dim idEquivalencia As String = Tabla1.Rows(Indice).Cells(0).Text
        Response.Redirect("/Paginas/PaginasAdmin/EditarEquivalencia.aspx?ID=" & idEquivalencia.Trim())

    End Sub

    Protected Sub btnRegistrarEquivalencia_Click(sender As Object, e As EventArgs)
        Dim equivalenciaPuntos As String = txtDisaPuntosEquivalantes.Text
        Dim validacion As Boolean = False

        If equivalenciaPuntos.Equals("") Or Not IsNumeric(equivalenciaPuntos) Then
            lblErrorModal.Text = "Ingresa un monto valido"
        Else
            validacion = ADPuntos.CrearEquvialencia(ddlProductoParaPuntos.SelectedValue, equivalenciaPuntos)
            If validacion Then
                Response.Redirect("/Paginas/PaginasAdmin/Puntos.aspx")
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error,un producto solo peude tener una equivalencia');</script>", False)
            End If
        End If
    End Sub

    Protected Sub btnCambioXCompra_Click(sender As Object, e As EventArgs)
        Dim disaPuntos As String = txtCantidadDisaPuntos.Text
        Dim monto As String = txtMontoDeCompra.Text
        Dim validacion As String = False

        If disaPuntos.Equals("") Or monto.Equals("") Or Not IsNumeric(disaPuntos) Or Not IsNumeric(monto) Then
            lblErrorModalCambioDeCompra.Text = "Ingrese  informacion valida"
        Else
            validacion = ADPuntos.EquivalenciaPrecio(disaPuntos, monto)
            If validacion Then
                lblErrorModalCambioDeCompra.Text = "Actualizacion Correcta"
                Response.Redirect("/Paginas/PaginasAdmin/Puntos.aspx")

            Else

                lblErrorModalCambioDeCompra.Text = "ERRORR!"
            End If
        End If

    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class