﻿Public Class EditarRegiones
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim RegionesAD As RegionesAD = New RegionesAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionProducto As New List(Of String)

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Accion = Request.QueryString("Accion")


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                ds = ADGeneral.ConsultarEstados()

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlEstadosDeMex.DataSource = ds.Tables("Estados").DefaultView
                    ddlEstadosDeMex.DataValueField = "id_estado"
                    ddlEstadosDeMex.DataTextField = "estado"
                    ddlEstadosDeMex.DataBind()

                End If


                If Accion Is Nothing Then
                    lblError.Text = ""
                Else
                    txtCodigoRegion.Text = Request.QueryString("Accion").ToString()
                    informacionProducto = ADGeneral.ConsultarInformacionRegiones(Request.QueryString("Accion").ToString())
                    PonerInformacionEnCampos(informacionProducto, "")


                End If

            End If


        End If

    End Sub



    Public Sub PonerInformacionEnCampos(informacion As List(Of String), codigoRegion As String)
        txtNomrbreRegion.Text = informacion(0)
        ddlEstadosDeMex.SelectedValue = informacion(1)
        txtCodigoGerente.Text = informacion(2)
        txtNombreDelGerente.Text = informacion(3)
        txtRFC.Text = informacion(4)
        txtDireccion.Text = informacion(5)
        txtColonia.Text = informacion(6)
        txtMunicipio.Text = informacion(7)
        txtCodigoPostal.Text = informacion(8)
        txtSuperficeBodega.Text = informacion(9)
        txtNumeroTelefonico.Text = informacion(10)
        txtCapacidadCajas.Text = informacion(11)

        If codigoRegion.Equals("") Then
            txtCodigoRegion.Text = Request.QueryString("Accion").ToString()
        Else
            txtCodigoRegion.Text = codigoRegion
        End If

    End Sub

    Protected Sub btnBuscarRegionEspecifica_Click(sender As Object, e As EventArgs) Handles btnBuscarRegionEspecifica.Click
        lblError.Text = ""
        lblErrorModificaciones.Text = ""

        Dim codigoRegion As String = txtBuscarRegion.Text()
        Dim informacionRegion As New List(Of String)

        If codigoRegion.Equals("") Then
            lblError.Text = "Codigo De Region!"
        Else
            informacionRegion = ADGeneral.ConsultarInformacionRegiones(codigoRegion)

            If informacionRegion.Count > 0 Then
                PonerInformacionEnCampos(informacionRegion, codigoRegion)
            Else
                lblError.Text = "Codigo Inexistente"

            End If
        End If


    End Sub


    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click

        Dim confirmValue As String = Request.Form("confirm_value")

        If confirmValue = "No" Then Return


        lblError.Text = ""
        lblErrorModificaciones.Text = ""
        Dim ID As String = txtCodigoRegion.Text()

        Dim validacion As Boolean = False
        If txtRFC.Text.Equals("") Or txtNomrbreRegion.Text.Equals("") Or txtDireccion.Text.Equals("") Or txtColonia.Text.Equals("") Or txtMunicipio.Text.Equals("") Or txtCodigoPostal.Text.Equals("") Or txtNumeroTelefonico.Text.Equals("") Or txtCodigoGerente.Text.Equals("") Or txtNombreDelGerente.Text.Equals("") Or txtSuperficeBodega.Text.Equals("") Or txtCapacidadCajas.Text.Equals("") Then
            lblErrorModificaciones.Text = "Si vas actualizar informacion no puede dejar campos vacios!"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Si vas actualizar informacion no puede dejar campos vacios!');</script>", False)
        Else
            'lblErrorModificaciones.Text = "Vamos bien"
            validacion = RegionesAD.ActualizarInformacionRegion(txtRFC.Text(), txtNomrbreRegion.Text(), txtDireccion.Text(), txtColonia.Text(), txtMunicipio.Text(), txtCodigoPostal.Text(), txtNumeroTelefonico.Text(), txtCodigoGerente.Text(), txtNombreDelGerente.Text(), txtSuperficeBodega.Text(), txtCapacidadCajas.Text(), ID, ddlEstadosDeMex.SelectedValue())
            If validacion Then
                lblErrorModificaciones.Text = "Actualizacion Correcta"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actualizacion Correcta de Regiones');</script>", False)
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La informacion es incorrecta o el Usuario: " & txtCodigoGerente.Text() & " no existe!');</script>", False)
            End If
        End If
    End Sub

    Protected Sub btnCambiarFotoRegion_Click(sender As Object, e As EventArgs) Handles btnCambiarFotoRegion.Click
        lblError.Text = ""
        lblErrorModificaciones.Text = ""

        Dim path As String = Server.MapPath("~/ImagenesRegiones/")
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""
        Dim codigoRegion As String = txtCodigoRegion.Text()

        If FileUpload1.HasFile Then
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                End If
            Next
            If fileOK Then
                Try
                    nombre = FileUpload1.FileName()
                    ruta = "~/ImagenesRegiones/" & nombre

                    If RegionesAD.ActualizarURLFoto(codigoRegion, ruta) Then
                        FileUpload1.PostedFile.SaveAs(path &
                        FileUpload1.FileName)
                        lblFoto.Text = "Foto Actualizada!!"
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Foto Actualizada');</script>", False)


                    Else
                        lblFoto.Text = "Error en la actualizacion!"

                    End If



                Catch ex As Exception
                    lblFoto.Text = "No se pudo subir el archivo!."
                End Try
            Else
                lblFoto.Text = "Solo fotos!."
            End If
        End If
    End Sub


    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub

    Protected Sub btnEliminarRegion_Click(sender As Object, e As EventArgs)
        Dim validacion As Boolean = False
        validacion = RegionesAD.EliminarRegion(Request.QueryString("Accion").ToString())
        If validacion Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Foto Actualizada');</script>", False)
            Response.Redirect("/Paginas/PaginasAdmin/Regiones.aspx")
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Eliminacion Incorrecta');</script>", False)
        End If
    End Sub
End Class