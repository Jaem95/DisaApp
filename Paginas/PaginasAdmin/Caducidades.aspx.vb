﻿Public Class Caducidades
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProductosAD As ProductosAD = New ProductosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ds As New Data.DataSet
        Dim nuevaUbicacion As String = ""
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)

        ' If IsPostBack Then Return
        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)




                If DropDownList1.SelectedValue.Equals("CodigoDisa") Then
                    ds = ProductosAD.ConsultarProductoEnInventario(ddlRegion.SelectedValue(), "codigoDisa")
                    nuevaUbicacion = ddlRegion.SelectedValue()
                ElseIf DropDownList1.SelectedValue.Equals("NombreProducto") Then
                    ds = ProductosAD.ConsultarProductoEnInventario(ddlRegion.SelectedValue(), "nombre")
                    nuevaUbicacion = ddlRegion.SelectedValue()
                ElseIf DropDownList1.SelectedValue.Equals("Clasificacion") Then
                    ds = ProductosAD.ConsultarProductoEnInventario(ddlRegion.SelectedValue(), "clasificacion")
                    nuevaUbicacion = ddlRegion.SelectedValue()
                ElseIf DropDownList1.SelectedValue.Equals("CodigoBarras") Then
                    ds = ProductosAD.ConsultarProductoEnInventario(ddlRegion.SelectedValue(), "codigo_barrras")
                    nuevaUbicacion = ddlRegion.SelectedValue()
                ElseIf ddlRegion.SelectedValue.Equals("") Then
                    ds = ProductosAD.ConsultarProductoEnInventario(ubicacion, "codigoDisa")
                    nuevaUbicacion = ubicacion
                Else
                    ds = ProductosAD.ConsultarProductoEnInventario(ddlRegion.SelectedValue(), "codigoDisa")
                    nuevaUbicacion = ddlRegion.SelectedValue()
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    Tabla1.DataSource = ds.Tables("tabla")
                    Tabla1.DataBind()
                    lblError.Text = ""

                Else
                    Tabla1.DataSourceID = Nothing
                    Tabla1.DataSource = Nothing
                    Tabla1.DataBind()
                    lblError.Text = "No hay productos"

                End If

                ds = ADGeneral.ConsultarRegiones()

                If ds.Tables(0).Rows.Count > 0 Then
                    ddlRegion.DataSource = ds.Tables("Region").DefaultView
                    ddlRegion.DataValueField = "id"
                    ddlRegion.DataTextField = "nombre"
                    ddlRegion.DataBind()
                    ddlRegion.SelectedValue = nuevaUbicacion

                End If

            End If

        End If

    End Sub


    Protected Sub btnBuscarProducto_Click(sender As Object, e As EventArgs) Handles btnBuscarProducto.Click
        lblError.Text = ""
        Dim codigoBarras As String = txtBuscarProducto.Text()
        Dim ds As New Data.DataSet

        If codigoBarras.Equals("") Then
            lblError.Text = "Ingresa algun dato valido"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Ingresa algun dato valido');</script>", False)
        Else
            ds = ProductosAD.ConsultarProductoEnInventarioEspecifico(ddlRegion.SelectedValue(), codigoBarras)
            If ds.Tables(0).Rows.Count > 0 Then
                Tabla1.DataSource = ds.Tables("tabla")
                Tabla1.DataBind()

            Else
                lblError.Text = "No hay productos"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No hay productos con el codigo de barras " & codigoBarras & "');</script>", False)

            End If

        End If

    End Sub

    Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Tabla1.SelectedIndexChanging
        Dim Indice As Integer = e.NewSelectedIndex
        Dim codigoDisa As String = Tabla1.Rows(Indice).Cells(1).Text
        Dim cantidadMinima As String = Tabla1.Rows(Indice).Cells(5).Text
        Response.Redirect("/Paginas/PaginasAdmin/EditarCantidades.aspx?Accion=" & codigoDisa.Trim() & "&Region=" & ddlRegion.SelectedValue() & "&CM=" + cantidadMinima + "")

    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub



End Class