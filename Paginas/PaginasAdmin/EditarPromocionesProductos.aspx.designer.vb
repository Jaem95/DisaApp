﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class EditarPromocionesProductos
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''imageUsuario3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imageUsuario3 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''lblNombre2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNombre2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''imageUsuario2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imageUsuario2 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''lblNombre3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNombre3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''btnCerrarSesion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCerrarSesion As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''imageUsuario1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imageUsuario1 As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''lblNombre4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNombre4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtCodigoDisa control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCodigoDisa As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtProducto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProducto As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtDescripcion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDescripcion As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPresentacion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPresentacion As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtCantidadCaja control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCantidadCaja As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtPorcentajeDescuento control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPorcentajeDescuento As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtFechaInicial control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaInicial As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtFechaFinal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFechaFinal As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtTipoPromocion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTipoPromocion As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtLugar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLugar As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''imagenCategoria control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imagenCategoria As Global.System.Web.UI.WebControls.Image
    
    '''<summary>
    '''btnEliminarPromocion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEliminarPromocion As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''txtCantidadMinimaGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCantidadMinimaGeneral As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''calendarioFinalPromoRegaloGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calendarioFinalPromoRegaloGeneral As Global.System.Web.UI.WebControls.Calendar
    
    '''<summary>
    '''lblRegaloGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegaloGeneral As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlProductoRegaloGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProductoRegaloGeneral As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtCantidadRegaloGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCantidadRegaloGeneral As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnRegistrarPromoRegaloGeneral control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRegistrarPromoRegaloGeneral As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''txtCantidadMinimaRegional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCantidadMinimaRegional As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlRegionesRegalo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlRegionesRegalo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''calendarioFinalRegaloRegional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calendarioFinalRegaloRegional As Global.System.Web.UI.WebControls.Calendar
    
    '''<summary>
    '''lblRegaloRegional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegaloRegional As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlproductoRegaloRegional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlproductoRegaloRegional As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtCantidadRegalo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCantidadRegalo As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''btnRegistrarPromoRegaloRegional control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRegistrarPromoRegaloRegional As Global.System.Web.UI.WebControls.Button
End Class
