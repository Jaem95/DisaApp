﻿Public Class ReporteGeneralProductos
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProductosAD As ReportesAD = New ReportesAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                ds = ProductosAD.ReporteProductosAdminGeneral(ddlPeriodo.SelectedValue)
                If ds.Tables(0).Rows.Count > 0 Then
                    Tabla1.DataSource = ds.Tables("tabla")
                    Tabla1.DataBind()

                Else
                    lblError.Text = "No hay Productos"

                End If

            End If

        End If



    End Sub
    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub

    Protected Sub btnBuscarProducto_Click(sender As Object, e As EventArgs)
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        ds = ProductosAD.ReporteProductosAdminButtonGeneral(ddlPeriodo.SelectedValue, txtBuscarProducto.Text)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay Productos"

        End If

    End Sub

    Protected Sub ddlPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        ds = ProductosAD.ReporteProductosAdminGeneral(ddlPeriodo.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            Tabla1.DataSource = ds.Tables("tabla")
            Tabla1.DataBind()

        Else
            lblError.Text = "No hay Productos"

        End If
    End Sub

    Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles Tabla1.SelectedIndexChanging
        Dim Indice As Integer = e.NewSelectedIndex
        Dim codigoDisa As String = Tabla1.Rows(Indice).Cells(1).Text
        Response.Redirect("/Paginas/PaginasAdmin/ReporteProductoEspecifico.aspx?cd=" & codigoDisa.Trim())

    End Sub
End Class