﻿Public Class ReporteVentasEspecificas
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ReportesAD As ReportesAD = New ReportesAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim informacionEmpleado As New List(Of String)
    Dim cd As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        cd = Request.QueryString("cd")


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)

                informacionEmpleado = ReportesAD.ReporteVentasRegionalesEspecificas(ubicacion, ddlPeriodo.SelectedValue, cd)
                txtClaveEmpleado.Text = informacionEmpleado(0)
                txtNombreGerente.Text = informacionEmpleado(1)
                txtRegion.Text = informacionEmpleado(2)

                txtPedidosRealizados.Text = informacionEmpleado(3)
                txtTotalDevoluciones.Text = informacionEmpleado(4)
                txtVentaTotal.Text = informacionEmpleado(5)
                txtVentaPromedios.Text = informacionEmpleado(6)

                'If DropDownList1.SelectedValue.Equals("CodigoDisa") Then
                '    ds = ProductosAD.ConsultarProductos("codigoDisa")
                'ElseIf DropDownList1.SelectedValue.Equals("NombreProducto") Then
                '    ds = ProductosAD.ConsultarProductos("nombre")
                'ElseIf DropDownList1.SelectedValue.Equals("Clasificacion") Then
                '    ds = ProductosAD.ConsultarProductos("clas")
                'ElseIf DropDownList1.SelectedValue.Equals("CodigoBarras") Then
                '    ds = ProductosAD.ConsultarProductos("codigo_barrras")
                'Else
                '    ds = ProductosAD.ConsultarProductos("prd.id")
                'End If

                'If ds.Tables(0).Rows.Count > 0 Then
                '    'Tabla1.DataSource = ds.Tables("tabla")
                '    'Tabla1.DataBind()

                'Else
                '    lblError.Text = "No hay productos"

                'End If

            End If

        End If



    End Sub
    Protected Sub ddlPeriodo_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ds As New Data.DataSet
        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        cd = Request.QueryString("cd")

        informacionEmpleado = ReportesAD.ReporteVentasRegionalesEspecificas(ubicacion, ddlPeriodo.SelectedValue, cd)
        txtClaveEmpleado.Text = informacionEmpleado(0)
        txtNombreGerente.Text = informacionEmpleado(1)
        txtRegion.Text = informacionEmpleado(2)

        txtPedidosRealizados.Text = informacionEmpleado(3)
        txtTotalDevoluciones.Text = informacionEmpleado(4)
        txtVentaTotal.Text = informacionEmpleado(5)
        txtVentaPromedios.Text = informacionEmpleado(6)

    End Sub
    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class