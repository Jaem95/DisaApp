<!DOCTYPE HTML>
<?php 
session_start();
if($_SESSION['pin']==""){
  echo '<script language = javascript>        
      self.location = "login.php"
    </script>';
}
else{
  if($_SESSION['st_eve']=="paid") {
    echo '<script language = javascript>        
      alert("You are alive yet");
      self.location = "home.php";
    </script>';
  }

}
?>
<html lang="es-MX">
<head>
  <title>My Weddapp</title>
  
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    
  <link rel="stylesheet" href="css/bootstrap.css" />
  <link rel="stylesheet" href="css/font-awesome.min.css" />
  <link rel="stylesheet" href="css/linea-icon.css" />
  <link rel="stylesheet" href="css/fancy-buttons.css" />
  <link rel="stylesheet" type="text/css" href="css/css.css">
  
  <!--=== Google Fonts ===-->
  <link href='http://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,400' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Raleway:600,400,300' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <!--=== Other CSS files ===-->
  <link rel="stylesheet" href="css/animate.css" />
  <link rel="stylesheet" href="css/jquery.vegas.css" />
  <link rel="stylesheet" href="css/baraja.css" />
  <link rel="stylesheet" href="css/jquery.bxslider.css" />
  
  <!--=== Main Stylesheets ===-->
  <link rel="stylesheet" href="css/style.css" />
  <link rel="stylesheet" href="css/responsive.css" />
  
  <!--=== Color Scheme, three colors are available red.css, orange.css and gray.css ===-->
  <link rel="stylesheet" id="scheme-source" href="css/schemes/red.css" />
  
  <!--=== Internet explorer fix ===-->
  <!-- [if lt IE 9]>
    <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif] -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>
  <script type="text/javascript" src="pago.js"></script>
  

  <script type="text/javascript">
    // Conekta Public Key
    Conekta.setPublishableKey('key_eELaoszV7kt8tCfWj1xcPoA');
    // ...
  </script>
  
</head>
<body >
  <!--=== Preloader section starts ===-->
  <section id="preloader">
    <div class="loading-circle fa-spin"></div>
  </section>
  <!--=== Preloader section Ends ===-->
  
  <!--=== Header section Starts ===-->
  <div id="header" class="header-section">
            
    <!-- sticky-bar Starts-->
    <div class="sticky-bar-wrap">
      <div class="sticky-section">
        <div id="topbar-hold" class="nav-hold container">
          <nav class="navbar" role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <!--=== Site Name ===-->
              <a class="site-name navbar-brand" href="home.php">
                            	<img style="position:absolute; margin-top:2%;" width="20%" height="auto" src="images/logo.png"/>
							</a>
            </div>
            
            <!-- Main Navigation menu Starts -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="dresscode.php">Dresscode</a></li>
								<li><a href="location.php">Location</a></li>
								<li><a href="guest.php">Guest List</a></li>
								<li><a href="hotel.php">Hotels</a></li>
								<li><a href="invitation.php">Invitation</a></li>
								<li><a href="gallery.php">Photo Albums</a></li>
                                <li><a href="gift.php">Gift List</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="logout.php">Log out</a></li>
              </ul>
            </div>
            <!-- Main Navigation menu ends-->
          </nav>
        </div>
      </div>
            <div class="social-icons" style="text-align:right; margin-right:2%;">
        <ul>
          <li style="cursor:pointer; color:rgba(255,0,0,1);" onClick="changeRed();"><i class="fa fa-square"></i></li>
          <li style="cursor:pointer; color:rgba(255,153,51,1);" onClick="changeOrange();"><i class="fa fa-square"></i></li>
                    <li style="cursor:pointer; color:#FA69BE;" onClick="changeRose();"><i class="fa fa-square"></i></li>
          <li style="cursor:pointer; color:rgba(102,102,102,1);" onClick="changeGray();"><i class="fa fa-square"></i></li>
                    <li style="cursor:pointer; color:#ED1FE6;" onClick="changePurple();"><i class="fa fa-square"></i></li>
        </ul>
      </div>
    </div>
    <!-- sticky-bar Ends-->
    <!--=== Header section Ends ===-->  
    <div id="section-home" class="home-section-wrap center">
      <div class="section-overlay"></div>
      <div class="container home">
        <div class="row">
          
          <h1 class="well-come" style="margin-top:100px; font-size:30px;">Liven up your wedding</h1>
          
            <div class="row">
            <div class="col-md-6 col-sm-6 single-pricing-wrap center animated" data-animation="bounceInLeft" data-animation-delay="700">
              <div class="single-pricing best-pricing"> <!-- this is best-pricing --> 
              
                <div class="pricing-head">
                  <h4 class="pricing-heading color-scheme">Payment method</h4>
                  <div class="col-md-8 col-md-offset-2" style="margin-top:4%;">
                      <img src="images/american1.png" style="width:20%;"  >
                      <img style="background-color:#fff; width:20%;" src="images/visa1.png">
                      <img src="images/master1.png" style="width:20%;">
                    </div>
                </div>
                <div class="package-features">
                  <div class="row">
                    <form action="trans.php" method="POST" id="card-form" role="form" class="col-md-8 col-md-offset-2">
                    <span class="card-errors"></span></br>
                    <div class="form-group ">
                      
                        <label style="color:#000000;"><span>Cardholder Name</span></label>
                        <input type="text" data-conekta="card[name]" placeholder="Enter your name" class="form-control "/>
                      
                    </div>
                    <div class="form-group ">
                      
                        <label style="color:#000000;"><span>Card Number</span></label>
                        <input type="text" data-conekta="card[number]" placeholder="Enter your card number" class="form-control "/>
                      </label>
                    </div>
                    <div class="col-md-4">
                        <label style="color:#000000;">
                        <span>CVC </span>
                      </label>
                    </div>
                    <div clas="col-md-8">
                        <label style="color:#000000;">
                        <span>Expiration Date (MM/YYYY)</span>
                      </label>
                    </div>
                    
                    <div class="form-group col-md-4">
                        <input type="text" data-conekta="card[cvc]" placeholder="CVC" class="form-control "/>
                      
                    </div>
                    
                    <div class="form-group col-md-4">

                        <input type="text" data-conekta="card[exp_month]" placeholder="MM" class="form-control "/>
                      
                    </div>
                    <div class="form-group col-md-4">                     
                        <input type="text" data-conekta="card[exp_year]" placeholder="YYYY" class="form-control"/>
                        
                    </div>
                    <button type="submit" class="fancy-button button-line btn-col zoom">¡Pay now!
                        <span class="icon">
                        <i class="fa fa-thumbs-o-up"></i>
                      </span>
                    </button>
                  </form>
            </div>
                </div>
                
                
                
              </div>
            </div>        
        <!-- Single Price Starts -->
           <div class="col-md-6 col-sm-6 single-pricing-wrap center animated" data-animation="bounceInLeft" data-animation-delay="700">
                  <div class="single-pricing best-pricing"> <!-- this is best-pricing --> 
                  
                    <div class="pricing-head">
                      <h4 class="pricing-heading color-scheme">Ultra</h4>
                      <div class="price" style="margin-bottom: 18px;">
                        <h3>
                          <span class="dollar">$</span>
                          3499.00 MXN /  <span class="dollar">$</span> 200 USD aprox
                          <span class="month">/event</span>
                        </h3>
                      </div>
                    </div>
                    
                    <ul class="package-features">
                      <li><span class="color-scheme fa fa-check"></span>Web panel access</li>
                      <li><span class="color-scheme fa fa-check"></span>Couple App acess</li>
                      <li><span class="color-scheme fa fa-check"></span>Guest App access</li>
                      <li><span class="color-scheme fa fa-check"></span>Manage your Guest list</li>
                      <li><span class="color-scheme fa fa-check"></span>Personal and Downloadable Photo Album</li>
                      
                    </ul>
                    
                  </div>
          </div>
        <!-- Single Price Ends -->
        </div>
            <!--Features container Ends -->
            
                    
        </div>
      </div>
    </div>




<br>
    <br>

</div>
    <!--=== Services section Starts ===-->
  
  <script type="text/javascript">
  jQuery(function($) {
  $("#card-form").submit(function(event) {
    var $form;
    $form = $(this);

    /* Previene hacer submit más de una vez */
    $form.find("button").prop("disabled", true);
    Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);

    /* Previene que la información de la forma sea enviada al servidor */
    return false;
  });
});

var conektaSuccessResponseHandler;
conektaSuccessResponseHandler = function(token) {
  var $form;
  $form = $("#card-form");

  
/* Inserta el token_id en la forma para que se envíe al servidor */

  $form.append($("<input type=\"hidden\" name=\"conektaTokenId\" />").val(token.id));

  
/* and submit */

  $form.get(0).submit();
};

var conektaErrorResponseHandler;
conektaErrorResponseHandler = function(response) {
  var $form;
  $form = $("#card-form");

  
/* Muestra los errores en la forma */
alert(response.message);
 // $form.find(".card-errors").text(response.message);
  $form.find("button").prop("disabled", false);
};


</script>
  
  <!--=== Download section Starts ===-->
  <section id="section-download" style="background-color:#FFF;" class="download-wrap">
    
    <div class="container download center">
          <div class="row">
              <div class="col-md-10 col-md-offset-1 center section-title">
            <h3 style="color:rgba(51,51,51,1)">Download</h3>
        </div>
            </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="col-md-10 col-md-offset-1 center section-title">
            <h3 style="color:rgba(51,51,51,1)">Couple App</h3>
          </div>
          <div class="download-buttons clearfix"> <!-- Download Buttons -->
            <a class="fancy-button button-line no-text btn-col large zoom" href="#" title="Download from App store">
              <span class="icon">
              <i class="fa fa-apple"></i>
              </span>
            </a>
            <a class="fancy-button button-line btn-col no-text large zoom" href="#" title="Download from App store">
              <span class="icon">
              <i class="fa fa-android"></i>
              </span>
            </a>
            <a class="fancy-button button-line btn-col no-text large zoom" href="#" title="Download from App store">
              <span class="icon">
              <i class="fa fa-windows"></i>
              </span>
            </a>
          </div>
        </div>
                <div class="col-lg-6">
          <div class="col-md-10 col-md-offset-1 center section-title">
            <h3 style="color:rgba(51,51,51,1)">Guest App</h3>
          </div>
          <div class="download-buttons clearfix"> <!-- Download Buttons -->
            <a class="fancy-button button-line no-text btn-col large zoom" href="#" title="Download from App store">
              <span class="icon">
              <i class="fa fa-apple"></i>
              </span>
            </a>
            <a class="fancy-button button-line btn-col no-text large zoom" href="#" title="Download from App store">
              <span class="icon">
              <i class="fa fa-android"></i>
              </span>
            </a>
            <a class="fancy-button button-line btn-col no-text large zoom" href="#" title="Download from App store">
              <span class="icon">
              <i class="fa fa-windows"></i>
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=== Download section Ends ===-->
  
  <!--=== Footer section Starts ===-->
  <div id="section-footer" class="footer-wrap">
    <div class="container footer center">
      <div class="row">
        <div class="col-lg-12">
          <h4 class="footer-title"><!-- Footer Title -->
            <a href="#"><img width="30%" height="auto" src="images/logo.png"/></a>
          </h4>
          
          <!-- Social Links -->
          <div class="social-icons">
            <ul>
              <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
              <li><a href="#"><i class="fa fa-flickr"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
            </ul>
          </div>
          
          <p class="copyright">GYO Solutions All rights reserved &copy; 2015</p>
        </div>
      </div>
    </div>
  </div>
  <!--=== Footer section Ends ===-->
  
<!--==== Js files ====-->
<!--==== Essential files ====-->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="js/modernizr.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

<!--==== Slider and Card style plugin ====-->
<script type="text/javascript" src="js/jquery.baraja.js"></script>
<script type="text/javascript" src="js/jquery.vegas.min.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>

<!--==== MailChimp Widget plugin ====-->
<script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>

<!--==== Scroll and navigation plugins ====-->
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/jquery.nav.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!--==== Custom Script files ====-->
<script type="text/javascript" src="js/custom.js"></script>

<script type="text/javascript" src="js/main.js"></script>

<script>
function changeOrange()
{
  $("#scheme-source").attr("href", "css/schemes/orange.css");
}
function changeRed()
{
  $("#scheme-source").attr("href", "css/schemes/red.css");
}
function changeGray()
{
  $("#scheme-source").attr("href", "css/schemes/gray.css");
}
function changePurple()
{
  $("#scheme-source").attr("href", "css/schemes/purple.css");
}
function changeRose()
{
  $("#scheme-source").attr("href", "css/schemes/rose.css");
}
</script>

</body>
</html>