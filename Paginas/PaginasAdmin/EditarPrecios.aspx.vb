﻿Public Class EditarPrecios
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim ProductosAD As ProductosAD = New ProductosAD()
    Dim EditarPrecios As EditarPreciosAD = New EditarPreciosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ds As New Data.DataSet
        Dim informacionProducto As New List(Of String)
        Dim precioProducto As New List(Of String)


        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Accion = Request.QueryString("Accion")



        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)


                If Accion Is Nothing Then

                Else
                    txtCodigoBarras.Text = Request.QueryString("Accion").ToString()
                    informacionProducto = ADGeneral.ConsultarInformacionProducto(Request.QueryString("Accion").ToString())
                    PonerInformacionEnCampos(informacionProducto, "")

                    If IsPostBack Then Return

                    precioProducto = EditarPrecios.ConsultarPreciosGenerales(Request.QueryString("Accion").ToString())

                    If precioProducto.Count() > 0 Then

                        txtPrecioRI1.Text = precioProducto(0)
                        txtPrecioRF1.Text = precioProducto(1)
                        txtPrecio1.Text = precioProducto(2)

                        txtPrecioRI2.Text = precioProducto(3)
                        txtPrecioRF2.Text = precioProducto(4)
                        txtPrecio2.Text = precioProducto(5)


                        txtPrecioRI3.Text = precioProducto(6)
                        txtPrecioRF3.Text = precioProducto(7)
                        txtPrecio3.Text = precioProducto(8)

                        ' rbtnPrecio.SelectedValue = EditarPrecios.ConsultarTipoPrecioActual(Request.QueryString("Accion").ToString())
                    End If


                End If

            End If


        End If

    End Sub

    Public Sub PonerInformacionEnCampos(informacion As List(Of String), codigoBarrasProducto As String)
        txtNombreProducto.Text = informacion(0)
        txtDescProducto.Text = informacion(1)
        txtPresentacionProducto.Text = informacion(2)
        txtCantidadPorCaja.Text = informacion(3)
        txtClasificacion.Text = informacion(6)
        If codigoBarrasProducto.Equals("") Then
            txtCodigoBarras.Text = Request.QueryString("Accion").ToString()
        Else
            txtCodigoBarras.Text = codigoBarrasProducto
        End If

        imagenProducto.ImageUrl = informacion(9)

    End Sub

    'Protected Sub rbtnPrecioGeneral_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    If rbtnPrecio.SelectedValue = "PrecioGeneral" Then
    '        rbtnPrecio.Items(1).Selected = False

    '    ElseIf rbtnPrecio.SelectedValue = "PrecioRegional" Then
    '        rbtnPrecio.Items(0).Selected = False
    '    End If
    'End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return

        If Not IsNumeric(txtPrecio1.Text) Or Not IsNumeric(txtPrecio2.Text) Or Not IsNumeric(txtPrecio3.Text) Or Not IsNumeric(txtPrecioRI1.Text) Or Not IsNumeric(txtPrecioRI2.Text) Or Not IsNumeric(txtPrecioRI3.Text) Or Not IsNumeric(txtPrecioRF1.Text) Or Not IsNumeric(txtPrecioRF2.Text) Or Not IsNumeric(txtPrecioRF3.Text) Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo numeros en los rangos de precio!');</script>", False)
        Else
            If EditarPrecios.BorraPreciosViejos(Request.QueryString("Accion").ToString(), "", "") Then
                If EditarPrecios.RegistrarPrecios(Request.QueryString("Accion").ToString(), txtPrecio1.Text, txtPrecioRI1.Text(), txtPrecioRF1.Text(), "") Then
                    If EditarPrecios.RegistrarPrecios(Request.QueryString("Accion").ToString(), txtPrecio2.Text, txtPrecioRI2.Text(), txtPrecioRF2.Text(), "") Then
                        If EditarPrecios.RegistrarPrecios(Request.QueryString("Accion").ToString(), txtPrecio3.Text, txtPrecioRI3.Text(), txtPrecioRF3.Text(), "") Then
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('REGISTRO DE PRECIO GENERAL PARA  EL PRODUCTO " & Request.QueryString("Accion").ToString() & "');window.location='PreciosGenerales.aspx'</script>", False)
                            'If EditarPrecios.ActualizarBanderaPrecio(Request.QueryString("Accion").ToString(), tipoPrecio, ubicacion) Then
                            '    ''VER PORQUE LA TABLA DE PRECIOS TIENE REGION
                            '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('REGISTRO DE PRECIO GENERAL PARA  EL PRODUCTO " & Request.QueryString("Accion").ToString() & "');</script>", False)
                            'Else
                            '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                            'End If
                        Else
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                        End If
                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)

                    End If
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro');</script>", False)
                End If
            End If

            'If rbtnPrecio.SelectedValue = "" Then
            '    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Selecciona el precio a aplicar!');</script>", False)
            'Else
            '    ' tipoPrecio = rbtnPrecio.SelectedValue()
            'End If

        End If


    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class