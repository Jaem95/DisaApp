﻿Public Class RegistrarProducto
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim RegistroProd As RegistroProdAD = New RegistroProdAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        If IsPostBack Then
            If ddlTipoDePieza.SelectedValue.Equals("5") Then
                txtCodigoBarrasCaja.Enabled = False


            End If
            Return
        End If

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)

        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)
            lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
            imageUsuario1.ImageUrl = informacionUsuario(13)
            imageUsuario2.ImageUrl = informacionUsuario(13)
            imageUsuario3.ImageUrl = informacionUsuario(13)

            ds = ADGeneral.ConsultarClasificacion

            If ds.Tables(0).Rows.Count > 0 Then
                ddlClasificacion.DataSource = ds.Tables("ClasificacionProductos").DefaultView
                ddlClasificacion.DataValueField = "id"
                ddlClasificacion.DataTextField = "nombre"
                ddlClasificacion.DataBind()

            End If

            ds = ADGeneral.ConsultarTipoDePieza

            If ds.Tables(0).Rows.Count > 0 Then
                ddlTipoDePieza.DataSource = ds.Tables("TipoPieza").DefaultView
                ddlTipoDePieza.DataValueField = "id"
                ddlTipoDePieza.DataTextField = "tipo"
                ddlTipoDePieza.DataBind()

            End If

            ds = ADGeneral.ConsultarProovedor

            If ds.Tables(0).Rows.Count > 0 Then
                ddlProovedor.DataSource = ds.Tables("Proovedor").DefaultView
                ddlProovedor.DataValueField = "id"
                ddlProovedor.DataTextField = "nombre"
                ddlProovedor.DataBind()

            End If




        End If
    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim script As String = ""

        If confirmValue = "No" Then Return

        lblErrorModificaciones.Text = ""
        Dim validacion As Boolean = False

        Dim path As String = Server.MapPath("~/ImagenesProductos/")
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""
        Dim codigoDisa As String = txtCodigoDisa.Text()

        If FileUpload1.HasFile Then
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                    Exit For
                End If
            Next
            If fileOK Then

                If txtCodigoBarrasCaja.Enabled = False Then
                    If txtNombreProducto.Text.Equals("") Or txtDescripcionProducto.Text.Equals("") Or txtPresentacion.Text.Equals("") Or txtCantidadPorCaja.Text.Equals("") Or txtCodigoBarrasPieza.Text.Equals("") Or txtCodigoDisa.Text.Equals("") Then
                        lblErrorModificaciones.Text = "En el Registro no puedes dejar campos vacios!"
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios!');</script>", False)
                    Else
                        lblErrorModificaciones.Text = "Vamos bien"
                        validacion = RegistroProd.RegistrarProducto(txtNombreProducto.Text(), txtDescripcionProducto.Text(), txtPresentacion.Text(), txtCantidadPorCaja.Text(), txtCodigoBarrasPieza.Text(), "", txtCodigoDisa.Text(), ddlProovedor.SelectedValue().ToString(), ddlClasificacion.SelectedValue.ToString(), ddlTipoDePieza.SelectedValue.ToString())
                        If validacion Then
                            Try
                                nombre = FileUpload1.FileName()
                                ruta = "~/ImagenesProductos/" & nombre

                                If RegistroProd.SubirFoto(txtCodigoDisa.Text(), ruta) Then
                                    FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)

                                    lblErrorModificaciones.Text = "ALTA DE PRODUCTOS CORRECTA!!"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Alta de productos correcta');</script>", False)
                                    Response.Redirect("/Paginas/PaginasAdmin/Productos.aspx")


                                End If

                            Catch ex As Exception
                                lblErrorModificaciones.Text = "Error en el registro de productos!"
                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registo!');</script>", False)

                            End Try
                        Else
                            lblErrorModificaciones.Text = "Error en el registro de productos! Codigo de Barras Duplicado!"
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro! El codigo de barras esta repetido!');</script>", False)

                        End If
                    End If
                Else
                    If txtNombreProducto.Text.Equals("") Or txtDescripcionProducto.Text.Equals("") Or txtPresentacion.Text.Equals("") Or txtCantidadPorCaja.Text.Equals("") Or txtCodigoBarrasPieza.Text.Equals("") Or txtCodigoBarrasCaja.Text.Equals("") Or txtCodigoDisa.Text.Equals("") Then
                        lblErrorModificaciones.Text = "En el Registro no puedes dejar campos vacios!"
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puedes dejar campos vacios!');</script>", False)
                    Else
                        lblErrorModificaciones.Text = "Vamos bien"
                        validacion = RegistroProd.RegistrarProducto(txtNombreProducto.Text(), txtDescripcionProducto.Text(), txtPresentacion.Text(), txtCantidadPorCaja.Text(), txtCodigoBarrasPieza.Text(), txtCodigoBarrasCaja.Text(), txtCodigoDisa.Text(), ddlProovedor.SelectedValue().ToString(), ddlClasificacion.SelectedValue.ToString(), ddlTipoDePieza.SelectedValue.ToString())
                        If validacion Then
                            Try
                                nombre = FileUpload1.FileName()
                                ruta = "~/ImagenesProductos/" & nombre

                                If RegistroProd.SubirFoto(txtCodigoDisa.Text(), ruta) Then
                                    FileUpload1.PostedFile.SaveAs(path & FileUpload1.FileName)

                                    lblErrorModificaciones.Text = "ALTA DE PRODUCTOS CORRECTA!!"
                                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Alta de productos correcta');</script>", False)
                                    Response.Redirect("/Paginas/PaginasAdmin/Productos.aspx")


                                End If

                            Catch ex As Exception
                                lblErrorModificaciones.Text = "Error en el registro de productos!"
                                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registo!');</script>", False)

                            End Try
                        Else
                            lblErrorModificaciones.Text = "Error en el registro de productos! Codigo de Barras Duplicado!"
                            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en el registro! El codigo de barras esta repetido!');</script>", False)

                        End If
                    End If
                End If



            Else
                lblFoto.Text = "Solo fotos!."
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo imagenes!');</script>", False)


            End If
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Tienes que Seleccionar una foto!!');</script>", False)
            lblErrorModificaciones.Text = "Tienes que seleccionar una foto!"


        End If

    End Sub


    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub

End Class