﻿Imports System
Imports System.Globalization
Imports System.Data
Imports System.Data.SqlClient

Public Class RegistroPromociones
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim PromocionesAD As PromocionesAD = New PromocionesAD()
    Dim ProductosAD As ADGeneral = New ADGeneral()
    Dim EditarPrecios As EditarPreciosAD = New EditarPreciosAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionCategoria As New List(Of String)
        Dim informacionPromocion As New List(Of String)
        Dim precioProducto As New List(Of String)


        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)
            End If

            ds = PromocionesAD.ConsultarProductos

            If ds.Tables(0).Rows.Count > 0 Then
                ddlProductos.DataSource = ds.Tables("Productos").DefaultView
                ddlProductos.DataValueField = "codigo_barrras"
                ddlProductos.DataTextField = "nombre"
                ddlProductos.DataBind()
                ddlProductos.SelectedValue = 0

            End If

            ds = ADGeneral.ConsultarRegiones()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlRegiones.DataSource = ds.Tables("Region").DefaultView
                ddlRegiones.DataValueField = "id"
                ddlRegiones.DataTextField = "nombre"
                ddlRegiones.DataBind()

            End If


        End If


    End Sub


    Protected Sub ddlProductos_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlTipoPromocion.Visible = True
        ddlTipoLugarPromocion.Visible = True

        ddlTipoLugarPromocion.SelectedValue = 0
        ddlTipoPromocion.SelectedValue = 0


        Dim informacionProducto As New List(Of String)
        informacionProducto = ADGeneral.ConsultarInformacionProducto(ddlProductos.SelectedValue)

        txtProducto.Text = ddlProductos.SelectedValue
        txtDescripcion.Text = informacionProducto(1)
        txtPresentacion.Text = informacionProducto(2)
        txtCantidadCaja.Text = informacionProducto(3)
        imagenCategoria.ImageUrl = informacionProducto(9)




    End Sub

    Protected Sub ddlTipoLugarPromocion_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim tipoPromo As String = ddlTipoPromocion.SelectedValue
        Dim lugarPromo As String = ddlTipoLugarPromocion.SelectedValue
        LimpiarCamposResultados()

        If tipoPromo.Equals("1") Then
            If lugarPromo.Equals("1") Then
                'Habilio boton PrecioGeneral
                CargarPreciosGeneral()
                Dim script As String = "mostrarPrecioGeneral();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
            ElseIf lugarPromo.Equals("2") Then
                CargarRegiones()
                'Hablito boton PregioRegioan
                Dim script As String = "mostrarPrecioRegional();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
            End If

        ElseIf tipoPromo.Equals("2") Then
            If lugarPromo.Equals("1") Then
                'Habilio boton RegaloGeneral
                CargarProductosParaRegaloGeneral()
                Dim script As String = "mostrarRegaloGeneral();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
            ElseIf lugarPromo.Equals("2") Then
                CargarProductosParaRegaloRegional()
                CargarRegionesParaRegaloRegional()
                'Hablito boton RegaloRegioan
                Dim script As String = "mostrarRegaloRegional();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)

            End If
        End If

    End Sub

    Protected Sub ddlTipoPromocion_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim tipoPromo As String = ddlTipoPromocion.SelectedValue
        Dim lugarPromo As String = ddlTipoLugarPromocion.SelectedValue

        LimpiarCamposResultados()

        If tipoPromo.Equals("1") Then
            If lugarPromo.Equals("1") Then
                'Habilio boton PrecioGeneraly cargo precios!
                CargarPreciosGeneral()
                Dim script As String = "mostrarPrecioGeneral();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
            ElseIf lugarPromo.Equals("2") Then
                CargarRegiones()
                'Hablito boton PregioRegioan
                Dim script As String = "mostrarPrecioRegional();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
            End If

        ElseIf tipoPromo.Equals("2") Then
            If lugarPromo.Equals("1") Then
                'Habilio boton RegaloGeneral
                CargarProductosParaRegaloGeneral()
                Dim script As String = "mostrarRegaloGeneral();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)
            ElseIf lugarPromo.Equals("2") Then
                'Hablito boton RegaloRegioan
                CargarProductosParaRegaloRegional()
                CargarRegionesParaRegaloRegional()
                Dim script As String = "mostrarRegaloRegional();"
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "show", script, True)

            End If
        End If
    End Sub

    Private Sub CargarProductosParaRegaloGeneral()
        Dim ds As DataSet
        ds = PromocionesAD.ConsultarProductos

        If ds.Tables(0).Rows.Count > 0 Then
            ddlProductoRegaloGeneral.DataSource = ds.Tables("Productos").DefaultView
            ddlProductoRegaloGeneral.DataValueField = "codigo_barrras"
            ddlProductoRegaloGeneral.DataTextField = "nombre"
            ddlProductoRegaloGeneral.DataBind()


        End If
    End Sub

    Private Sub CargarProductosParaRegaloRegional()
        Dim ds As DataSet
        ds = PromocionesAD.ConsultarProductos

        If ds.Tables(0).Rows.Count > 0 Then
            ddlproductoRegaloRegional.DataSource = ds.Tables("Productos").DefaultView
            ddlproductoRegaloRegional.DataValueField = "codigo_barrras"
            ddlproductoRegaloRegional.DataTextField = "nombre"
            ddlproductoRegaloRegional.DataBind()


        End If
    End Sub

    Private Sub CargarRegionesParaRegaloRegional()
        Dim ds As New Data.DataSet

        ds = ADGeneral.ConsultarRegiones()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRegionesRegalo.DataSource = ds.Tables("Region").DefaultView
            ddlRegionesRegalo.DataValueField = "id"
            ddlRegionesRegalo.DataTextField = "nombre"
            ddlRegionesRegalo.DataBind()


        End If
    End Sub

    Protected Sub ddlRegiones_SelectedIndexChanged(sender As Object, e As EventArgs)
        CargarPreciosXRegion()
    End Sub




    Private Sub LimpiarCamposResultados()
        lblPrecioCalculado1.Text = ""
        lblPrecioCalculado2.Text = ""
        lblPrecioCalculado3.Text = ""

        lblPrecioRegionalCalculado.Text = ""
        lblPrecioRegionalCalculado2.Text = ""
        lblPrecioRegionalCalculado3.Text = ""
    End Sub

    Private Sub CargarPreciosGeneral()
        Dim precioProducto As New List(Of String)
        precioProducto = EditarPrecios.ConsultarPreciosGenerales(ddlProductos.SelectedValue)
        Dim resultado As Double = 0
        Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat

        If Not precioProducto.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Este producto aun no tiene un precio general');</script>", False)
            lblR1.Text = ""
            lblR2.Text = ""
            lblR3.Text = ""

            lblPR1.Text = ""
            lblPR2.Text = ""
            lblPR3.Text = ""
        Else
            lblR1.Text = precioProducto(0) & "-" & precioProducto(1)
            lblR2.Text = precioProducto(3) & "-" & precioProducto(4)
            lblR3.Text = precioProducto(6) & "-" & precioProducto(7)

            lblPR1.Text = precioProducto(2)
            lblPR2.Text = precioProducto(5)
            lblPR3.Text = precioProducto(8)
        End If



    End Sub

    Private Sub CargarPreciosXRegion()
        Dim precioProducto As New List(Of String)
        precioProducto = EditarPrecios.ConsultarPreciosPorRegion(ddlProductos.SelectedValue, ddlRegiones.SelectedValue)
        Dim resultado As Double = 0
        Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat

        If Not precioProducto.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Este producto aun no tiene un precio general');</script>", False)
            lblRango1Regional.Text = ""
            lblRango2Regional.Text = ""
            lblRango3Regional.Text = ""

            lblPrecio1Regional.Text = ""
            lblPrecio2Regional.Text = ""
            lblPrecio3Regional.Text = ""
        Else
            lblRango1Regional.Text = precioProducto(0) & "-" & precioProducto(1)
            lblRango2Regional.Text = precioProducto(3) & "-" & precioProducto(4)
            lblRango3Regional.Text = precioProducto(6) & "-" & precioProducto(7)

            lblPrecio1Regional.Text = precioProducto(2)
            lblPrecio2Regional.Text = precioProducto(5)
            lblPrecio3Regional.Text = precioProducto(8)
        End If



    End Sub

    Private Sub CargarRegiones()
        Dim ds As New Data.DataSet

        ds = ADGeneral.ConsultarRegiones()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRegiones.DataSource = ds.Tables("Region").DefaultView
            ddlRegiones.DataValueField = "id"
            ddlRegiones.DataTextField = "nombre"
            ddlRegiones.DataBind()


        End If
    End Sub




    Protected Sub btnCalcularPrecioGeneral_Click(sender As Object, e As EventArgs) Handles btnCalcularPrecioGeneral.Click
        Dim resultado As Double = 0
        Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat


        resultado = lblPR1.Text - (lblPR1.Text() * (txtDescuento.Text / 100))
        lblPrecioCalculado1.Text = resultado.ToString("C", nfi)

        resultado = lblPR2.Text - (lblPR2.Text() * (txtDescuento.Text / 100))
        lblPrecioCalculado2.Text = resultado.ToString("C", nfi)

        resultado = lblPR3.Text - (lblPR3.Text() * (txtDescuento.Text / 100))
        lblPrecioCalculado3.Text = resultado.ToString("C", nfi)


    End Sub

    Protected Sub btnCalcularPrecioRegional_Click(sender As Object, e As EventArgs)
        Dim resultado As Double = 0
        Dim nfi As NumberFormatInfo = New CultureInfo("en-US", False).NumberFormat


        resultado = lblPrecio1Regional.Text - (lblPrecio1Regional.Text() * (txtDescuentoRegional.Text / 100))
        lblPrecioRegionalCalculado.Text = resultado.ToString("C", nfi)

        resultado = lblPrecio2Regional.Text - (lblPrecio2Regional.Text() * (txtDescuentoRegional.Text / 100))
        lblPrecioRegionalCalculado2.Text = resultado.ToString("C", nfi)

        resultado = lblPrecio3Regional.Text - (lblPrecio3Regional.Text() * (txtDescuentoRegional.Text / 100))
        lblPrecioRegionalCalculado3.Text = resultado.ToString("C", nfi)
    End Sub

    Protected Sub btnActualizarPromo_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return

        Dim cbProducto As String = ddlProductos.SelectedValue
        Dim descuento As String = txtDescuento.Text
        Dim localidad = "1000001"
        Dim fechaInicialPromo As String = fechaInicio.SelectedDate.ToShortDateString
        Dim fechaFinPromo As String = fechaFinal.SelectedDate.ToShortDateString
        Dim validacion As Boolean = False

        If fechaInicialPromo.Equals("01/01/0001") Or fechaFinPromo.Equals("01/01/0001") Then
            lblErrorPrecioGeneral.Text = "Debes de seleccionar  fecha de inicio y fin!"
        ElseIf Convert.ToDateTime(fechaInicialPromo) >= Convert.ToDateTime(fechaFinPromo) Then
            lblErrorPrecioGeneral.Text = "La fecha de inicio debe de ser menor que la de inicio"
        Else
            validacion = PromocionesAD.CrearPromocion(cbProducto, descuento, localidad, fechaInicialPromo, fechaFinPromo)
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + descuento + "');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
            Else
                lblErrorPrecioGeneral.Text = ("Probablemente este producto ya tiene una promocion ")
            End If


        End If

    End Sub



    Protected Sub btnActualizarPromoRegional_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return

        Dim cbProducto As String = ddlProductos.SelectedValue
        Dim descuento As String = txtDescuentoRegional.Text
        Dim localidad As String = ddlRegiones.SelectedValue
        Dim fechaInicialPromo As String = fechaInicialPromoRegional.SelectedDate.ToShortDateString
        Dim fechaFinPromo As String = fechaFinalPromoRegional.SelectedDate.ToShortDateString
        Dim validacion As Boolean = False

        If fechaInicialPromo.Equals("01/01/0001") Or fechaFinPromo.Equals("01/01/0001") Then
            lblModalErrorRegional.Text = "Debes de seleccionar  fecha de inicio y fin!"
        ElseIf Convert.ToDateTime(fechaInicialPromo) >= Convert.ToDateTime(fechaFinPromo) Then
            lblModalErrorRegional.Text = "La fecha de inicio debe de ser menor que la de inicio"
        Else
            validacion = PromocionesAD.CrearPromocion(cbProducto, descuento, localidad, fechaInicialPromo, fechaFinPromo)
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion se actualizo con este monto:" + descuento + "');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/PromocionesPrecios.aspx")
            Else
                lblModalErrorRegional.Text = ("Probablemente este producto ya tiene una promocion ")
            End If


        End If


    End Sub

    Protected Sub btnRegistrarPromoRegaloRegional_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return

        Dim cbProductoCompra As String = ddlProductos.SelectedValue
        Dim cbProductoRegalo As String = ddlproductoRegaloRegional.SelectedValue
        Dim region As String = ddlRegionesRegalo.SelectedValue
        Dim fechaInicialPromo As String = calendarioInicialRegaloRegional.SelectedDate.ToShortDateString
        Dim fechaFinPromo As String = calendarioFinalRegaloRegional.SelectedDate.ToShortDateString
        Dim compraMinima As String = txtCantidadMinimaRegional.Text
        Dim cantidadRegalo As String = txtCantidadRegalo.Text
        Dim validacion As Boolean = False

        If compraMinima.Equals("") Or cantidadRegalo.Equals("") Or Not IsNumeric(compraMinima) Or Not IsNumeric(cantidadRegalo) Then
            lblRegaloRegional.Text = "Ingrese datos validos"
        Else
            If fechaInicialPromo.Equals("01/01/0001") Or fechaFinPromo.Equals("01/01/0001") Then
                lblRegaloRegional.Text = "Debes de seleccionar  fecha de inicio y fin!"
            ElseIf Convert.ToDateTime(fechaInicialPromo) >= Convert.ToDateTime(fechaFinPromo) Then
                lblRegaloRegional.Text = "La fecha de inicio debe de ser menor que la de inicio"
            Else
                validacion = PromocionesAD.CrearPromocionRegalo(cbProductoCompra, cbProductoRegalo, region, fechaInicialPromo, fechaFinPromo, compraMinima, cantidadRegalo)
                If validacion Then
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion fue creada ');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
                Else
                    lblRegaloRegional.Text = "Probablemente este producto ya tiene una promocion"
                End If


            End If


        End If










    End Sub

    Protected Sub btnRegistrarPromoRegaloGeneral_Click(sender As Object, e As EventArgs)
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim tipoPrecio As String = ""

        If confirmValue = "No" Then Return

        Dim cbProductoCompra As String = ddlProductos.SelectedValue
        Dim cbProductoRegalo As String = ddlProductoRegaloGeneral.SelectedValue
        Dim fechaInicialPromo As String = calendarioInicialPromoRegaloGeneral.SelectedDate.ToShortDateString
        Dim fechaFinPromo As String = calendarioFinalPromoRegaloGeneral.SelectedDate.ToShortDateString
        Dim compraMinima As String = txtCantidadMinimaGeneral.Text
        Dim cantidadRegalo As String = txtCantidadRegaloGeneral.Text
        Dim validacion As Boolean = False

        If compraMinima.Equals("") Or cantidadRegalo.Equals("") Or Not IsNumeric(compraMinima) Or Not IsNumeric(cantidadRegalo) Then
            lblRegaloGeneral.Text = "Ingrese datos validos"
        Else
            If fechaInicialPromo.Equals("01/01/0001") Or fechaFinPromo.Equals("01/01/0001") Then
                lblRegaloGeneral.Text = "Debes de seleccionar  fecha de inicio y fin!"
            ElseIf Convert.ToDateTime(fechaInicialPromo) >= Convert.ToDateTime(fechaFinPromo) Then
                lblRegaloGeneral.Text = "La fecha de inicio debe de ser menor que la de inicio"
            Else
                validacion = PromocionesAD.CrearPromocionRegalo(cbProductoCompra, cbProductoRegalo, "1000001", fechaInicialPromo, fechaFinPromo, compraMinima, cantidadRegalo)
                If validacion Then
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('La promocion fue creada ');</script>", False)
                    Response.Redirect("/Paginas/PaginasAdmin/PromocionesProductos.aspx")
                Else
                    lblRegaloRegional.Text = "Probablemente este producto ya tiene una promocion"
                End If

            End If


        End If

    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
End Class