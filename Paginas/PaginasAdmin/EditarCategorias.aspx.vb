﻿Public Class EditarCategorias
    Inherits System.Web.UI.Page
    Dim ADGeneral As ADGeneral = New ADGeneral()
    Dim CategoriasAD As CategoriasAD = New CategoriasAD()
    Dim usuario As String
    Dim pwd As String
    Dim ubicacion As String
    Dim tipoUsuario As String
    Dim informacionUsuario As New List(Of String)
    Dim Accion As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        Dim informacionCategoria As New List(Of String)

        If IsPostBack Then Return

        usuario = CType((Session("usuario")), String)
        pwd = CType((Session("pwd")), String)
        tipoUsuario = CType((Session("tipoUsuario")), String)
        ubicacion = CType((Session("ubicacion")), String)
        Accion = Request.QueryString("Accion")


        If usuario Is Nothing Or pwd Is Nothing Or tipoUsuario Is Nothing Or ubicacion Is Nothing Then
            Response.Redirect("/Login.aspx")
        Else
            informacionUsuario = ADGeneral.ConsultarInformacion(usuario)

            If informacionUsuario.Count() > 0 Then

                lblNombre2.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre3.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                lblNombre4.Text = informacionUsuario(0) & " " & informacionUsuario(1)
                imageUsuario1.ImageUrl = informacionUsuario(13)
                imageUsuario2.ImageUrl = informacionUsuario(13)
                imageUsuario3.ImageUrl = informacionUsuario(13)


                If Not Accion Is Nothing Then
                    informacionCategoria = CategoriasAD.ConsultarInformacion(Request.QueryString("Accion").ToString())
                    If informacionCategoria.Count() > 0 Then
                        txtNombreCategoria.Text = informacionCategoria(0)
                        txtLetrasIniciales.Text = informacionCategoria(1)
                        txtDescCategoria.Text = informacionCategoria(2)
                        imagenCategoria.ImageUrl = "~/ImagenesCategorias/" & informacionCategoria(3) & ""
                    End If
                End If

            End If


        End If


    End Sub

    Protected Sub btnGuardarCambios_Click(sender As Object, e As EventArgs) Handles btnGuardarCambios.Click
        Dim validacion As Boolean = False

        If txtDescCategoria.Text.Equals("") Or txtLetrasIniciales.Text.Equals("") Or txtNombreCategoria.Text.Equals("") Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('No puede dejar campos vacios');</script>", False)
        Else
            validacion = CategoriasAD.ActualizarInfo(txtNombreCategoria.Text(), txtLetrasIniciales.Text(), txtDescCategoria.Text(), Request.QueryString("Accion").ToString())

            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Actulizacion Correcta!');</script>", False)
                Response.Redirect("/Paginas/PaginasAdmin/Categorias.aspx")
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error');</script>", False)

            End If

        End If
    End Sub

    Protected Sub btnCerrarSesion_Click(sender As Object, e As EventArgs)
        FormsAuthentication.SignOut()
        usuario = vbNull
        pwd = vbNull
        Session.Remove("usuario")
        Session.Remove("contra")
        Session.Abandon()
        Response.Redirect("/Login.aspx")
    End Sub
    Protected Sub btnSubirFoto_Click(sender As Object, e As EventArgs) Handles btnSubirFoto.Click
        Dim path As String = Server.MapPath("~/ImagenesCategorias/")
        Console.Write(path)
        Dim fileOK As Boolean = False
        Dim nombre As String = ""
        Dim ruta As String = ""

        If FileUpload1.HasFile Then
            Console.Write("Entre al if")
            Dim fileExtension As String
            fileExtension = System.IO.Path.
                GetExtension(FileUpload1.FileName).ToLower()
            Dim allowedExtensions As String() =
                {".jpg", ".jpeg", ".png", ".gif"}
            For i As Integer = 0 To allowedExtensions.Length - 1
                If fileExtension = allowedExtensions(i) Then
                    fileOK = True
                End If
                Console.Write("La extencion esta bien")
            Next
            If fileOK Then
                Try
                    nombre = FileUpload1.FileName()
                    ruta = "~/ImagenesCategorias/" & nombre
                    Console.Write(nombre & "_" & ruta)
                    If CategoriasAD.ActualizarURLCategoria(Request.QueryString("Accion").ToString(), nombre) Then
                        FileUpload1.PostedFile.SaveAs(path &
                        FileUpload1.FileName)
                        Console.Write(CType((Session("usuario")), String))
                        lblFotoPerfil.Text = "File uploaded!"
                        Response.Redirect("/Paginas/PaginasAdmin/EditarCategorias.aspx?Accion=" & (Request.QueryString("Accion").ToString() & ""))

                    Else
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error en la actualizacion');</script>", False)


                    End If



                Catch ex As Exception
                    Console.Write(ex.ToString())

                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Error " & ex.ToString() & "');</script>", False)

                End Try
            Else
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>alert('Solo archivos PNG o JPEG');</script>", False)

            End If
        End If
    End Sub
End Class