﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PromocionesProductos.aspx.vb" Inherits="DISA.PromocionesProductos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>DISA</title>  <link rel="shortcut icon" type="image/png" href="../../DISA.png"/>
 <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" >
    <form id="form1" runat="server"> <asp:ScriptManager runat="server"></asp:ScriptManager>
       
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/Paginas/Admin.aspx" class="logo">
                    <span class="logo-mini"><b>DISA</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>DISA</b> Managment</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <%--<img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">--%>
                                    <asp:Image ID="imageUsuario3" runat="server" class="user-image" />
                                    <span class="hidden-xs">
                                        <asp:Label ID="lblNombre2" runat="server" Text="Label"></asp:Label></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <%--<img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">--%>
                                        <asp:Image ID="imageUsuario2" runat="server" class="img-circle" />
                                        <p>
                                            <asp:Label ID="lblNombre3" runat="server" Text=""></asp:Label>

                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="/Paginas/PaginasComun/Perfil.aspx" class="btn btn-default btn-flat">Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            <asp:Button runat="server" CssClass="btn btn-default btn-flat" Text="Cerrar Sesion" ID="btnCerrarSesion"  OnClick="btnCerrarSesion_Click"/>
          
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
                     <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">

                            <asp:Image ID="imageUsuario1" runat="server" class="img-circle" />
                        </div>
                        <div class="pull-left info">
                            <p>
                                <asp:Label ID="lblNombre4" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                    </div>

                    <!-- Sidebar user panel -->

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menu Principal</li>
                     
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-cubes"></i>
                                <span>Productos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Productos.aspx"><i class="fa fa-circle-o"></i>Productos Generales</a></li>
                                <li><a href="/Paginas/PaginasAdmin/ModificarProductos.aspx"><i class="fa fa-circle-o"></i>Modificacion de Productos</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistrarProducto.aspx"><i class="fa fa-circle-o"></i>Alta de Productos</a></li>
                                <li><a href="/Paginas/PaginasAdmin/Caducidades.aspx"><i class="fa fa-circle-o"></i>Caducidades y Flujo de
                          <br />
                                    Inventarios</a></li>
                                <li><a href="/Paginas/PaginasAdmin/Categorias.aspx"><i class="fa fa-circle-o"></i>Categorias Generales</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroCategorias.aspx"><i class="fa fa-circle-o"></i>Alta de Categorias</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-dollar"></i>
                                <span>Precios</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/PreciosGenerales.aspx"><i class="fa fa-circle-o"></i>Edicion de Precios</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa  fa-balance-scale"></i>
                                <span>Promociones</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/RegistroPromociones.aspx"><i class="fa fa-circle-o"></i>Registro de Promociones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/PromocionesPrecios.aspx"><i class="fa fa-circle-o"></i>Promociones de Precios</a></li>
                                <li><a href="/Paginas/PaginasAdmin/PromocionesProductos.aspx"><i class="fa fa-circle-o"></i>Promociones de Productos</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa   fa-file-text"></i>
                                <span>Reportes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de Productos</a></li>
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de Ventas</a></li>
                                <li><a href="Subpages/Registro.aspx"><i class="fa fa-circle-o"></i>Reportes de DisaPuntos</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-map-marker"></i>
                                <span>Regiones</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Regiones.aspx"><i class="fa fa-circle-o"></i>Consulta de Regiones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroRegiones.aspx"><i class="fa fa-circle-o"></i>Alta de Regiones</a></li>
                                <li><a href="/Paginas/PaginasAdmin/EditarRegiones.aspx"><i class="fa fa-circle-o"></i>Modifacion de Regiones</a></li>
                            </ul>
                        </li>


                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-sheqel"></i>
                                <span>DISA-Puntos</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Puntos.aspx"><i class="fa fa-circle-o"></i>Edicion de DISA Puntos</a></li>
                            </ul>
                        </li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa    fa-group "></i>
                                <span>Fabricantes</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/Paginas/PaginasAdmin/Proovedores.aspx"><i class="fa fa-circle-o"></i>Consulta de Fabricantes</a></li>
                                <li><a href="/Paginas/PaginasAdmin/RegistroProovedores.aspx"><i class="fa fa-circle-o"></i>Alta de Fabricantes</a></li>
                            </ul>
                        </li>




                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Promociones de Regalo
                    </h1>
                </section>

                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <!-- Main content -->
                        <section class="content">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <div class="box-header">
                                            <h4>
                                                <asp:Label ID="lblError" runat="server" Text="Consulta de Promociones Existentes" CssClass="label label-danger"></asp:Label></h4>

                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped table-hover">

                                                <asp:GridView ID="Tabla1" runat="server" class="table table-bordered table-striped" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="idPromocion" HeaderStyle-HorizontalAlign="Center" HeaderText="ID" />
                                                        <asp:BoundField DataField="codigoDisa" HeaderStyle-HorizontalAlign="Center" HeaderText="Codigo DISA" />
                                                        <asp:BoundField DataField="nombre" HeaderStyle-HorizontalAlign="Center" HeaderText="Letras Iniciales" />
                                                        <asp:BoundField DataField="codigo_barrras" HeaderStyle-HorizontalAlign="Center" HeaderText="Codigo de Barras" />
                                                        <asp:BoundField DataField="LA" HeaderStyle-HorizontalAlign="Center" HeaderText="Lugar de Aplicacion" />
                                                        <asp:BoundField DataField="fecha_inicio" HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Inicio" />
                                                        <asp:BoundField DataField="fecha_fin" HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha Termino" />

                                                        <asp:CommandField DeleteText="" SelectText="Editar" ShowSelectButton="True" />
                                                    </Columns>
                                                </asp:GridView>

                                            </table>

                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </section>
                        <!-- /.content -->
                    </ContentTemplate>


                </asp:UpdatePanel>



            </div>
            <!-- /.content-wrapper -->


            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy;2016 <a href="">Hello World Technologies</a>.</strong> All rights
    reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <!-- /.control-sidebar-menu -->
                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->

                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
<!-- ./wrapper -->
    </form>
   
    <!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

</body>
</html>
