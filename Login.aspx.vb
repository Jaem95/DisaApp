﻿Public Class Login
    Inherits System.Web.UI.Page
    Dim AD As LoginAD = New LoginAD()
    Dim ADGeneral As ADGeneral = New ADGeneral()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetAllowResponseInBrowserHistory(False)
            Response.Cache.SetNoStore()
        End If

    End Sub


    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click




        Dim usuario As String = txtUsuario.Text
        Dim pwd As String = txtPassword.Text
        Dim status As Integer = 0
        Dim validacion As New List(Of String)
        Dim tipoUsuario As String = ""
        Dim informacionUsuario As New List(Of String)

        If usuario.Equals("") Or pwd.Equals("") Then
            lblError.Text = "No debes de dejar campos vacios!"
        Else
            'Consultamos que exista en  Empleados
            status = AD.ConsultarExistencia(usuario)
            If status = 1 Then
                'Consulto que la contraseña sea correcta
                validacion = AD.ValidarCredenciales(usuario, pwd)

                If validacion(0).Equals("TRUE") Then
                    If validacion(1).Equals("1") Then
                        lblError.Text = ("Hola Administrador!")
                        Session.Add("usuario", usuario)
                        Session.Add("pwd", pwd)

                        Session.Add("tipoUsuario", validacion(1).ToString())
                        Session.Add("ubicacion", validacion(2).ToString())

                        Response.Redirect("Paginas/Admin.aspx")
                    End If
                    If validacion(1).Equals("2") Then
                        lblError.Text = ("Hola Gerente!")
                        Session.Add("usuario", usuario)
                        Session.Add("pwd", pwd)

                        Session.Add("tipoUsuario", validacion(1).ToString())
                        Session.Add("ubicacion", validacion(2).ToString())

                        Response.Redirect("Paginas/Gerente.aspx")
                    End If
                    If validacion(1).Equals("3") Then
                        lblError.Text = ("Hola Bodeguero!")
                        Session.Add("usuario", usuario)
                        Session.Add("pwd", pwd)

                        Session.Add("tipoUsuario", validacion(1).ToString())
                        Session.Add("ubicacion", validacion(2).ToString())

                        Response.Redirect("Paginas/Bodeguero.aspx")
                    End If
                Else
                    lblError.Text = "Usuario y/o contraseña invalidos"
                End If
            Else
                'Muestro Error de Existencia
                lblError.Text = "Error! El usuario no existe!"
            End If

        End If

    End Sub
End Class